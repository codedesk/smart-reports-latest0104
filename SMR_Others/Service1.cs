﻿using Models;
using PortaUtility.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
namespace SMR_Others
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        int repeatTime = 7200;
        Timer timer = new Timer();


        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                //DebugLog("Service Started");
                log.Info("Service Started");
                AsliWork();

                timer.Elapsed += new ElapsedEventHandler(WorkFunction);
                timer.Interval = repeatTime * 1000; //number in milisecinds  
                timer.Enabled = true;
            }
            catch (Exception ex)
            {
                //DebugLog("Unable To Start Service - " + ex.Message.ToString());
                log.Info("Unable To Start Service - " + ex.Message.ToString());
            }
        }

        protected override void OnStop()
        {
            log.Info("Service Stopped");
            //DebugLog("Service Stopped");
        }



        public void WorkFunction(object source, ElapsedEventArgs e)
        {
            AsliWork();
        }

        public void AsliWork()
        {
            try
            {
                DataSet ds = new DataSet();

                string path = AppDomain.CurrentDomain.BaseDirectory;

                string filepath = path + "/ServiceSettign.xml";

                if (File.Exists(filepath))
                {
                    ds.ReadXml(filepath);
                }
                else
                {
                    log.Error("Service settigns not available.");

                    return;
                }


                if (ds != null && ds.Tables.Count > 0)
                {
                    Parallel.ForEach(ds.Tables[0].AsEnumerable(), drow =>
                    {
                        ApiUtility apiUtil = new ApiUtility();

                        ServiceSetting servicesetting = new ServiceSetting();

                        servicesetting.ConnString = drow["ConnectionString"].ToString();
                        servicesetting.LicenseKey = drow["LicenseKey"].ToString();

                        ServiceUtility serviceutil = new ServiceUtility();

                        //serviceutil.StartService(servicesetting);
                        apiUtil.CustomersGetSync(servicesetting).Wait();

                        apiUtil.GetAccountSync(null, servicesetting).Wait();

                        apiUtil.GetProductSync(null, servicesetting).Wait();

                        apiUtil.GetExtensionSync(null, servicesetting).Wait();

                        apiUtil.GetHuntGrupSync(null, servicesetting).Wait();

                    });

                }
            }
            catch (Exception ex)
            {
                log.Info("AsliWork " + ex.Message);
                //DebugLog(ex.Message.ToString());
            }
        }


    }
}
