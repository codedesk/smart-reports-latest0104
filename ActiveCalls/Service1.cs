﻿using Models;
using PortaUtility.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Timers;
using DAL;
namespace ActiveCalls
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        string logsConnectionString = "Data Source=213.229.116.7;Initial Catalog=SMS_Service_Logs;User ID=sa;password=@AccV12v.K2032@";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        int repeatTime = 30;
        Timer timer = new Timer();
     
        Timer RestartTimers = new Timer();

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {

                DataAccessLayer.InserServiceLogs(logsConnectionString, "Info", "Service Started", "");

                RestartTimers.Elapsed += new ElapsedEventHandler(checkTimers);
                RestartTimers.Interval = 600 * 1000; //number in milisecinds  
                RestartTimers.Enabled = true;

                timer.Elapsed += new ElapsedEventHandler(WorkFunction);
                timer.Interval = int.Parse(ConfigurationManager.AppSettings["ActiveCall_RepeatTime"]) * 1000; //number in milisecinds  
                timer.Enabled = true;


               
            }
            catch (Exception ex)
            {
                DataAccessLayer.InserServiceLogs(logsConnectionString, "Error", ex.Message, "");
                EventLog.WriteEntry("Error in service " + ex.Message);
                //DebugLog("Unable To Start Service - " + ex.Message.ToString());
                log.Info("Unable To Start Service - " + ex.Message.ToString());
            }
        }

        protected override void OnStop()
        {
            DataAccessLayer.InserServiceLogs(logsConnectionString, "Info", "Service stopped", "");
            log.Info("Service Stopped");
            //DebugLog("Service Stopped");
        }

      
       public void checkTimers(object source, ElapsedEventArgs e)
        {

            if(!timer.Enabled)
            {
                log.Info("Settings timer to true");
                DataAccessLayer.InserServiceLogs(logsConnectionString, "Info", "Settings timer to true", "");
                timer.Enabled = true;
            }
           
        }
        public void WorkFunction(object source, ElapsedEventArgs e)
        {
            AsliWork();
        }

        public void AsliWork()
        {
            try
            {
                DataAccessLayer.InserServiceLogs(logsConnectionString, "Info", "Inside Sync Active Calls.", "");
                DataSet ds = new DataSet();

                string path = AppDomain.CurrentDomain.BaseDirectory;

                string filepath = path + "/ServiceSettign.xml";

                if (File.Exists(filepath))
                {
                    ds.ReadXml(filepath);
                }
                else
                {
                    DataAccessLayer.InserServiceLogs(logsConnectionString, "Error", "Service settigns not available.", "");
                    log.Error("Service settigns not available.");

                    return;
                }


                if (ds != null && ds.Tables.Count > 0)
                {
                    Parallel.ForEach(ds.Tables[0].AsEnumerable(), drow =>
                    {
                        ApiUtility apiUtil = new ApiUtility();

                        ServiceSetting servicesetting = new ServiceSetting();

                        servicesetting.ConnString = drow["ConnectionString"].ToString();
                        servicesetting.LicenseKey = drow["LicenseKey"].ToString();

                        ServiceUtility serviceutil = new ServiceUtility();
                        try {
                            apiUtil.SyncActiveCalls(servicesetting).Wait();
                        }
                        catch(Exception ex)
                        {
                            DataAccessLayer.InserServiceLogs(logsConnectionString, "Error", "Settings timers to false , Error occured in sync active calls " + ex.Message.ToString(), "");
                            log.Error("Settings timers to false , Error occured in sync active calls " + ex.Message.ToString());
                            timer.Enabled = false;
                           
                        }

                    });

                }
            }
            catch (Exception ex)
            {
                DataAccessLayer.InserServiceLogs(logsConnectionString, "Error", "AsliWork " + ex.Message, "");
                log.Info("AsliWork " + ex.Message);
                //DebugLog(ex.Message.ToString());
            }
        }



        
    }
}
