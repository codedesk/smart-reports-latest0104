﻿using DAL;
using Models;
using PortaUtility;
using PortaUtility.Utilities;
using ReportingSystem;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestService
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {

            ServiceSetting servicesetting = new ServiceSetting();

            servicesetting.ConnString = "Data Source=213.229.116.7;Initial Catalog=smartreport;User ID=sa;password=@AccV12v.K2032@";
            ////servicesetting.LicenseKey = "f7d60373-8120-4685-a8cd-468fa4a5fb7b";
            ////a3a25547-aacd-1253-f5cd-166af2c113aa
            servicesetting.LicenseKey = "BD98ED49-F96D-465F-8160-94983510AFD1";
            ApiUtility apiUtil = new ApiUtility();
            apiUtil.GetHourlyCDRS_Old_From_Live(null, servicesetting).Wait();
            ////apiUtil.CustomersGetSync(servicesetting).Wait();
            //DataAccessLayer.InserServiceLogs("Data Source=78.129.239.99;Initial Catalog=SMS_Service_Logs;User ID=sa;password=!Qazwerty", "Info", "test", "");
            ////apiUtil.GetHourlyCDRS_Old_From_Live(null, servicesetting).Wait();
            ////Console.WriteLine("GetHourlyCDRS Done");
            StartService();
            Console.WriteLine("All Done");

            Console.ReadLine();
        }

        public static void StartService()
        {
            log.Info("Service Started");
            try
            {
                DataSet ds = new DataSet();

                string path = AppDomain.CurrentDomain.BaseDirectory;

                string filepath = path + "/ServiceSettign.xml";

                if (File.Exists(filepath))
                {
                    ds.ReadXml(filepath);
                }
                else
                {
                    log.Error("Service settigns not available.");

                    return;
                }


                if (ds != null && ds.Tables.Count > 0)
                {
                    Parallel.ForEach(ds.Tables[0].AsEnumerable(), drow =>
                    {
                        ApiUtility apiUtil = new ApiUtility();

                        ServiceSetting servicesetting = new ServiceSetting();

                        servicesetting.ConnString = drow["ConnectionString"].ToString();
                        servicesetting.LicenseKey = drow["LicenseKey"].ToString();

                        apiUtil.CustomersGetSync(servicesetting).Wait();

                        apiUtil.GetAccountSync(null, servicesetting).Wait();

                        apiUtil.GetProductSync(null, servicesetting).Wait();

                        apiUtil.GetExtensionSync(null, servicesetting).Wait();

                        apiUtil.GetHuntGrupSync(null, servicesetting).Wait();

                        //ServiceUtility serviceutil = new ServiceUtility();

                        //serviceutil.StartService(servicesetting);

                    });

                }

            }
            catch (System.Exception ex)
            {
                log.Error(ex.Message);
            }
        }

    }
}
