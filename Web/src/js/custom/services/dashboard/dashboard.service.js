﻿'use strict';
angular.module('naut').factory('dashboardService', ['$http', '$q', 'localStorageService', 'ngAuthSettings',
    function ($http, $q, localStorageService, ngAuthSettings) {

        var serviceBase = ngAuthSettings.apiServiceBaseUri;
        var dashboardServiceFactory = {};

        var _getDashboard = function (iCustomer) {

            return $http.get(serviceBase + 'api/Dashboard/GetDashboard?iCustomer=' + iCustomer).then(function (response) {
                return response;
            });

        };

        var _getWallboard = function (iCustomer) {

            return $http.get(serviceBase + 'api/Dashboard/GetWallboard?iCustomer=' + iCustomer).then(function (response) {
                return response;
            });

        };

        var _getActiveCalls = function (iCustomer) {

            return $http.get(serviceBase + 'api/Dashboard/GetActiveCalls?iCustomer=' + iCustomer).then(function (response) {
                return response;
            });

        };

        var _getLineChart = function (iCustomer, type) {

            return $http.get(serviceBase + 'api/Dashboard/Getlinechart?iCustomer=' + iCustomer + '&type='+ type).then(function (response) {
                return response;
            });

        };
        var _getLanguage = function (id) {

            return $http.get(serviceBase + 'api/Dashboard/GetLanguage?userID=' + id).then(function (response) {
                return response;
            });

        };
        

        var _updateLanguage = function (id, language) {

            return $http.get(serviceBase + 'api/Dashboard/UpdateLanguage?userID=' + id + '&language=' + language).then(function (response) {
                return response;
            });

        };

        dashboardServiceFactory.getDashboard = _getDashboard;
        dashboardServiceFactory.getWallboard = _getWallboard;
        dashboardServiceFactory.getActiveCalls = _getActiveCalls;
        dashboardServiceFactory.getLineChart = _getLineChart;
        dashboardServiceFactory.getLanguage = _getLanguage;
        dashboardServiceFactory.updateLanguage = _updateLanguage;
        

        return dashboardServiceFactory;
    }]);