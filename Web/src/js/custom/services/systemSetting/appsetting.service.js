﻿'use strict';
angular.module('naut').factory('appsettingService', ['$http', '$q', 'localStorageService', 'ngAuthSettings',
    function ($http, $q, localStorageService, ngAuthSettings) {

        var serviceBase = ngAuthSettings.apiServiceBaseUri;
        var partnerInfoServiceFactory = {};

        var _getAppSetting = function () {

            return $http.get(serviceBase + 'api/AppSetting/GetAppSetting').then(function (response) {
                return response;
            });

        };


  
        var _appSettingSubmit = function (model) {

            return $http.post(serviceBase + 'api/AppSetting/AppSettingSubmit/', model).then(function (response) {
                return response;
            });

        };


 
      
        partnerInfoServiceFactory.getappSetting = _getAppSetting;
        partnerInfoServiceFactory.appSettingSubmit = _appSettingSubmit;

        return partnerInfoServiceFactory;
    }]);