﻿'use strict';
angular.module('naut').controller('productController', ['$scope', '$location', '$modal', '$window', 'productService', 'localStorageService', 'ngAuthSettings', 'modalService', '$filter',

    function ($scope, $location, $modal, $window, productService, localStorageService, ngAuthSettings, modalService, $filter) {

        $scope.model = {
            selectedDDIList: [],
            selectedExtensionList: [],
            selectedIVRList: [],
            selectedVoiceMailList: [],
            iCustomer: 0,
        };


        var authData = localStorageService.get('authorizationData');
        if (authData) {
            $scope.model.iCustomer = authData.iCustomer;
        }


        productService.getProducts($scope.model.iCustomer).then(function (response) {
            debugger;
            $scope.productListDid = response.data.table0;
            $scope.productListExt = response.data.table1;

            $scope.productListIVR = response.data.table4;
            $scope.productListVM = response.data.table5;


            //$scope.ddlProductList = [];
            //$scope.extensionProductList = [];

            var ddl = [];
            for (var i = 0; i < response.data.table2.length; i++) {

                ddl.push(response.data.table2[i].id);
            }

            var extensions = [];
            for (var i = 0; i < response.data.table3.length; i++) {

                extensions.push(response.data.table3[i].id);
            }

            var ivr = [];
            for (var i = 0; i < response.data.table6.length; i++) {

                ivr.push(response.data.table6[i].id);
            }

            var vm = [];
            for (var i = 0; i < response.data.table7.length; i++) {

                vm.push(response.data.table7[i].id);
            }


          
            for (var i = 0; i < $scope.productListDid.length; i++) {

                if (ddl.indexOf($scope.productListDid[i].id) != -1) {
                    $scope.model.selectedDDIList.push($scope.productListDid[i]);
                }
                
            }

          
     

            for (var i = 0; i < $scope.productListExt.length; i++) {

                if (extensions.indexOf($scope.productListExt[i].id) != -1) {
                    $scope.model.selectedExtensionList.push($scope.productListExt[i]);
                }

            }


            for (var i = 0; i < $scope.productListIVR.length; i++) {

                if (ivr.indexOf($scope.productListIVR[i].id) != -1) {
                    $scope.model.selectedIVRList.push($scope.productListIVR[i]);
                }

            }

            for (var i = 0; i < $scope.productListVM.length; i++) {

                if (vm.indexOf($scope.productListVM[i].id) != -1) {
                    $scope.model.selectedVoiceMailList.push($scope.productListVM[i]);
                }

            }

          


        },
         function (err) {
             $.toaster({ title: 'Error', priority: 'danger', message: err.data });
         });


        $scope.productSubmit = function () {

            productService.productSubmit($scope.model).then(function (response) {
                $.toaster({ title: 'Message', priority: 'success', message: "Product save successfully." });
            },
         function (err) {
             $.toaster({ title: 'Error', priority: 'danger', message: err.data });
         });

        };





    }
]);
