﻿'use strict';
angular.module('naut').controller('appsettingController', ['$scope', '$location', '$modal', '$window', 'appsettingService', 'localStorageService', 'ngAuthSettings', 'modalService',

    function ($scope, $location, $modal, $window, appsettingService, localStorageService, ngAuthSettings, modalService) {

        $scope.model = {
            partnerName: '',
            Logo: '',
            licenseKey: '',
            customStyles: ''
        };


        appsettingService.getappSetting().then(function (response) {
            if (response.data != null)
            {
                $scope.model = response.data;
            }
        },
         function (err) {
              $.toaster({ title: 'Error', priority: 'danger', message: err.data });
         });


        $scope.appSettingSubmit = function (isValid) {
            debugger;
            if (!isValid) {
                return false;
            }

            appsettingService.appSettingSubmit($scope.model).then(function (response) {
                $.toaster({ title: 'Message', priority: 'success', message: "Partner Info saved successfully." });
            },
         function (err) {
              $.toaster({ title: 'Error', priority: 'danger', message: err.data });
         });

        };






    }
]);
