﻿'use strict';
angular.module('naut').controller('wallboardController', ['$scope', '$location', '$modal', '$window', 'dashboardService', 'localStorageService', 'ngAuthSettings', 'modalService', '$timeout', 'flotOptions', 'colors', '$rootScope', 'localize', 'backendHubProxy',

    function ($scope, $location, $modal, $window, dashboardService, localStorageService, ngAuthSettings, modalService, $timeout, flotOptions, colors, $rootScope, localize, backendHubProxy) {
        $scope.inCalls = [];
        $scope.outCalls = [];
        $scope.MCalls=[];
        var authData = localStorageService.get('authorizationData');
        if (authData) {
            dashboardService.getLanguage(authData.fkUser).then(function (response) {

                var language = response.data.table0[0];
                $scope.selectedLanguage = language.selectedLanuage;
                $scope.selectedLanguageName = "";
                if ($scope.selectedLanguage == "en-US") {
                    $scope.selectedLanguageName = "English";
                }
                if ($scope.selectedLanguage == "fr") {
                    $scope.selectedLanguageName = "French";
                }
                if ($scope.selectedLanguage == "de") {
                    $scope.selectedLanguageName = "German";
                }
                localize.setLanguage(language.selectedLanuage);
            }, function (err) {
                $.toaster({ title: 'Error', priority: 'danger', message: err.data });
            });
        }
        var ICustomer = $location.search().skey;
        $scope.cdate = $location.search().cdate;

        $scope.customerId = ICustomer;

        $scope.serviceBase = ngAuthSettings.apiServiceBaseUri;

        $scope.model = {
            totalActive: 0,
            inboundCalls: 0,
            answeredCalls: 0,
            talkTime: 0,
            totalMissed: 0,
            averageTalkTime: 0,
            outboundCalls: 0,
            totalIn: 0,
            totalOut: 0,
            totalInternal: 0,
            totalWaiting: 0
        };

                $scope.lineSeriesPoints = [true, true, true];
                $scope.lineSeriesLines = [true, true, true];

                $scope.chartLineFlotChartIn = angular.extend({}, flotOptions['line'], { yaxis: { max: 15 } });
                $scope.chartLineFlotChartOut = angular.extend({}, flotOptions['line'], { yaxis: { max: 30 } });


        $scope.HourlyIncomingData = ''

        $scope.HourlyOutgoingData = '';

        var myVar = setInterval(refreshWallBoard, 10000);

        function refreshWallBoard() {
            console.log("wallboard referesh started.");
            dashboardService.getWallboard(ICustomer).then(function (response) {
                
                $scope.model = response.data;
                $scope.CallsbyExtension = response.data.callsByExtention;
                debugger;
                $scope.chartLineFlotChartIn = angular.extend({}, flotOptions['line'], { yaxis: { max: response.data.maxIn } });
                $scope.chartLineFlotChartOut = angular.extend({}, flotOptions['line'], { yaxis: { max: response.data.maxOut } });

                $scope.HourlyIncomingData = response.data.hourlyInCalls;
                $scope.HourlyOutgoingData = response.data.hourlyOutCalls;
                $scope.inCalls = response.data.callsIn;
                $scope.outCalls = response.data.callsOut;
                $scope.waitingCalls = response.data.waitingCalls;
                $scope.MCalls = response.data.missedCalls;
                var d = new Date();
              
                $scope.updateTime = d.toLocaleTimeString();
               

                $('.legendLabel').hide();

                console.log("wallboard referesh completed.");
            });
        }

        refreshWallBoard();

       

    }
]);
