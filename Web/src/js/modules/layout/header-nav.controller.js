/**=========================================================
 * Module: HeaderNavController
 * Controls the header navigation
 =========================================================*/


'use strict';

angular
    .module('naut')
    .controller('HeaderNavController', ['$scope', '$rootScope', '$location', '$modal', '$window', 'authService', 'localStorageService', 'ngAuthSettings', '$remember', '$timeout', 'modalService', '$forget', 'dashboardService', 'localize',


function ($scope, $rootScope, $location, $modal, $window, authService, localStorageService, ngAuthSettings, $remember, $timeout, modalService, $forget, dashboardService, localize) {

    $scope.userTitle = "";
    $scope.headerMenuCollapsed = true;

    $scope.serviceBase = ngAuthSettings.apiServiceBaseUri;

    $scope.toggleHeaderMenu = function () {
        $scope.headerMenuCollapsed = !$scope.headerMenuCollapsed;
    };

    // Adjustment on route changes
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        $scope.headerMenuCollapsed = true;
    });

    $scope.logout = function () {

        authService.logOut($scope.loginData);
    };
   
    var authData = localStorageService.get('authorizationData');
    if (authData) {
        $scope.loadLanguege = function (value)
        {
            debugger;
            localize.setLanguage(value);
            dashboardService.updateLanguage(authData.fkUser, value).then(function (response) {
                localStorageService.set('defaultLanguage', value);
                $.toaster({ title: 'Info', priority: 'info', message: 'User language updated.' });
            }, function (err) {
                $.toaster({ title: 'Error', priority: 'danger', message: err.data });
            });
        }
        $scope.userTitle = authData.firstName + " " + authData.lastName

        $scope.customer = authData.customerName 
    }




}
    ]);
