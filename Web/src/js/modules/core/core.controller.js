/**=========================================================
 * Module: CoreController.js
 =========================================================*/


    'use strict';

    angular
        .module('naut')
        .controller('CoreController', ['$rootScope', 'authService', 'localStorageService', '$location', '$scope', 'localize', 'dashboardService',
    function ($rootScope, authService, localStorageService, $location, $scope, localize, dashboardService) {

        /* @ngInject */
        function CoreController($rootScope) {
            // Get title for each page
            $rootScope.pageTitle = function() {
                return $rootScope.app.name + ' - ' + $rootScope.app.description;
            };
            $scope.$on('$viewContentLoaded', function () {

               
                if ($location.path() != "/#/login") {

                    authService.userAuthenticate();
                }

                var myUrl = $location.path();
                if ($location.path() != "/login") {

                    var authData = localStorageService.get('authorizationData');
                    debugger;
                    if (authData) {
                        dashboardService.getLanguage(authData.fkUser).then(function (response) {

                            var language = response.data.table0[0];
                            $scope.selectedLanguage = language.selectedLanuage;
                            $scope.selectedLanguageName = "";
                            if ($scope.selectedLanguage == "en-US") {
                                $scope.selectedLanguageName = "English";
                            }
                            if ($scope.selectedLanguage == "fr") {
                                $scope.selectedLanguageName = "French";
                            }
                            if ($scope.selectedLanguage == "de") {
                                $scope.selectedLanguageName = "Dutch";
                            }
                            //localize.setLanguage(language.selectedLanguage);
                        })
                    }
                    //, function (err) {
                    //    $.toaster({ title: 'Error', priority: 'danger', message: err.data });
                    //});
                }
            })
            // Cancel events from templates
            // ----------------------------------- 
            $rootScope.cancel = function($event){
                $event.preventDefault();
                $event.stopPropagation();
            };
        }

    }])

