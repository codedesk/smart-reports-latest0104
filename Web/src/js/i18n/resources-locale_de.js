[
    {
        "key": "_DashboardTitle_",
        "value": "Dashboard ",
        "description": "Home page Dashboard Title text"
    },
    {
        "key": "_WallBoard_",
        "value": "Wandbrett",
        "description": "Wall Board"
    },
    {
        "key": "_In_",
        "value": "In",
        "description": "In"
    }
    ,

    {
        "key": "_TotalOutCalls_",
        "value": "Out",
        "description": "Out"
    },
    {
        "key": "_Answered_",
        "value": "Answered",
        "description": "Answered"
    }
    ,
    {
        "key": "_Missed_",
        "value": "Missed",
        "description": "Total Missed Calls"
    }
    ,
    {
        "key": "_ActiveCalls_",
        "value": "Active Calls",
        "description": "Active Calls"
    }
    , {
        "key": "_From_",
        "value": "From",
        "description": "From"
    }
    , {
        "key": "_To_",
        "value": "To",
        "description": "To"
    }
    , {
        "key": "_DurationMins_",
        "value": "Duration Mins",
        "description": "Duration Minutes"
    }
    , {
        "key": "_CallType_",
        "value": "Call Type",
        "description": "_CallType_"
    }

    , {
        "key": "_HourlyIncomingCallsDistribution_",
        "value": "Hourly Incoming Calls Distribution",
        "description": "Hourly Incoming Calls Distribution"
    }
    , {
        "key": "_Department_",
        "value": "Department",
        "description": "Department"
    }
    , {
        "key": "_TotalCalls_",
        "value": "Total Calls",
        "description": "Total Calls"
    }
    , {
        "key": "_TotalOut_",
        "value": "Total Out",
        "description": "Total Out"
    }

    , {
        "key": "_TotalIn_",
        "value": "Total In",
        "description": "Total In"
    }
    , {
        "key": "_TotalMissed_",
        "value": "Total Missed",
        "description": "Total Missed"
    }
    , {
        "key": "_TotalCallsMissed_",
        "value": "Total Calls Missed",
        "description": "Total Calls Missed"
    }
    , {
        "key": "_Missed_",
        "value": "Missed",
        "description": "Missed"
    }
    , {
        "key": "_Transferred_",
        "value": "Transferred",
        "description": "Transferred"
    }
    , {
        "key": "_AvgTalkTime_",
        "value": "Avg Talk Time",
        "description": "Avg Talk Time"
    }
    , {
        "key": "_TotalTalkTime_",
        "value": "Total Talk Time",
        "description": "Total Talk Time"
    }
      , {
          "key": "_SmartReports_",
          "value": "Smart Reports",
          "description": "Smart Reports"
      }
      , {
          "key": "_LOGINCREDENTIALS_",
          "value": "LOGIN CREDENTIALS",
          "description": "LOGIN CREDENTIALS"
      }
      , {
          "key": "_Rememberme_",
          "value": "Remember me",
          "description": "Remember me"
      }
      , {
          "key": "_Login_",
          "value": "Login",
          "description": "Login"
      }
      , {
          "key": "_Forgotyourpassword?_",
          "value": "Forgot your password?",
          "description": "Forgot your password?"
      }
      , {
          "key": "_CreateRole_",
          "value": "Create Role",
          "description": "Create Role"
      }
      , {
          "key": "_Name_",
          "value": "Name",
          "description": "Name"
      }
      , {
          "key": "_Roles_",
          "value": "Roles",
          "description": "Roles"
      }
      , {
          "key": "_Users_",
          "value": "Users",
          "description": "Users"
      }
       , {
           "key": "_Customers_",
           "value": "Customers",
           "description": "Customers"
       }
       , {
           "key": "_Register_",
           "value": "Register",
           "description": "Register"
       }
       , {
           "key": "_FirstName_",
           "value": "First Name",
           "description": "First Name"
       }
       , {
           "key": "_LastName_",
           "value": "Last Name",
           "description": "Last Name"
       }
       , {
           "key": "_Email_",
           "value": "Email",
           "description": "Email"
       }
       , {
           "key": "_Role_",
           "value": "Role",
           "description": "Role"
       }
        , {
            "key": "_Active_",
            "value": "Active",
            "description": "Active"
        }
         , {
             "key": "_Typepasswordtounlock_",
             "value": "Type password to unlock",
             "description": "Type password to unlock"
         }
         , {
             "key": "_RESETPASSWORD_",
             "value": "RESET PASSWORD",
             "description": "RESET PASSWORD"
         }
          , {
              "key": "_WewillsendyouVerifyCodetoresetyourpassword._",
              "value": "We will send you Verify Code to reset your password.",
              "description": "We will send you Verify Code to reset your password."
          }
          , {
              "key": "_Send_",
              "value": "Send",
              "description": "Send"
          }
           , {
               "key": "_Submit_",
               "value": "Submit",
               "description": "Submit"
           }
           , {
               "key": "_Pleaseentergivencodewhichweemailedyou._",
               "value": "Please enter given code which we emailed you.",
               "description": "Please enter given code which we emailed you."
           }
           , {
               "key": "_Pleaseenterpasswordandconfirmpassword._",
               "value": "Please enter password and confirmpassword.",
               "description": "Please enter password and confirmpassword."
           }
           , {
               "key": "_Password*_",
               "value": "Password*",
               "description": "Password*"
           }
           , {
               "key": "_ConfirmPassword*_",
               "value": "Confirm Password*",
               "description": "Confirm Password*"
           }
            , {
                "key": "_Cancel_",
                "value": "Cancel",
                "description": "Cancel"
            }
            , {
                "key": "_Password_",
                "value": "Password",
                "description": "Password"
            }
            , {
                "key": "_ConfirmPassword_",
                "value": "Confirm Password",
                "description": "Confirm Password"
            }
             , {
                 "key": "_Step1_",
                 "value": "Step 1",
                 "description": "Step 1"
             }
              , {
                  "key": "_Step2_",
                  "value": "Step 2",
                  "description": "Step 2"
              }
               , {
                   "key": "_Step3_",
                   "value": "Step 3",
                   "description": "Step 3"
               }
               , {
                   "key": "_CompanyInformation_",
                   "value": "Company Information",
                   "description": "Company Information"
               }
               , {
                   "key": "_Customer_",
                   "value": "Customer",
                   "description": "Customer"
               }
                , {
                    "key": "_Country_",
                    "value": "Country",
                    "description": "Country"
                }
                 , {
                     "key": "_TimeZone_",
                     "value": "Time Zone",
                     "description": "Time Zone"
                 }
                 , {
                     "key": "_Next_",
                     "value": "Next",
                     "description": "Next"
                 }
                 , {
                     "key": "_RoleandPermission_",
                     "value": "Role and Permission",
                     "description": "Role and Permission"
                 }
                  , {
                      "key": "_RoleName_",
                      "value": "Role Name",
                      "description": "Role Name"
                  }
                   , {
                       "key": "_SelectAll_",
                       "value": "Select All",
                       "description": "Select All"
                   }
                   , {
                       "key": "_Prev_",
                       "value": "Prev",
                       "description": "Prev"
                   }
                   , {
                       "key": "_Finish_",
                       "value": "Finish",
                       "description": "Finish"
                   }
                    , {
                        "key": "_Excellent!You'vecompletedallsteps._",
                        "value": "Excellent ! You've completed all steps.",
                        "description": "Excellent ! You've completed all steps."
                    }
                    , {
                        "key": "_Division_",
                        "value": "Division",
                        "description": "Division"
                    }
                    , {
                        "key": "_Extension_",
                        "value": "Extension",
                        "description": "Extension"
                    }
                     , {
                         "key": "_CostCentre_",
                         "value": "Cost Centre",
                         "description": "Cost Centre"
                     }
                     , {
                         "key": "_Username_",
                         "value": "User name",
                         "description": "User name"
                     }
                      , {
                          "key": "_Permission_",
                          "value": "Permission",
                          "description": "Permission"
                      }
                       , {
                           "key": "_Products_",
                           "value": "Products",
                           "description": "Products"
                       }
                        , {
                            "key": "_DIDProducts_",
                            "value": "DID Products",
                            "description": "DID Products"
                        }
                        , {
                            "key": "_ExtensionProducts_",
                            "value": "Extension Products",
                            "description": "Extension Products"
                        }
                        , {
                            "key": "_IVRProducts_",
                            "value": "IVR Products",
                            "description": "IVR Products"
                        }
                        , {
                            "key": "_VoiceMailProducts_",
                            "value": "Voice Mail Products",
                            "description": "Voice Mail Products"
                        }
                        , {
                            "key": "_Logo_",
                            "value": "Logo",
                            "description": "Logo"
                        }
                        , {
                            "key": "_LicenseKey_",
                            "value": "License Key",
                            "description": "License Key"
                        }
                        , {
                            "key": "_CustomCSS_",
                            "value": "Custom CSS",
                            "description": "Custom CSS"
                        }
                         , {
                             "key": "_SystemSettings_",
                             "value": "System Settings",
                             "description": "System Settings"
                         }
                         , {
                             "key": "_TargetAnswerTime_",
                             "value": "Target Answer Time",
                             "description": "Target Answer Time"
                         }
                         , {
                             "key": "_(Second)_",
                             "value": "(Second)",
                             "description": "(Second)"
                         }
                         , {
                             "key": "_MaximumCallDuration_",
                             "value": "Maximum Call Duration",
                             "description": "Maximum Call Duration"
                         }
                          , {
                              "key": "_Chart_",
                              "value": "Chart",
                              "description": "Chart"
                          }
                           , {
                               "key": "_CallsOffered_",
                               "value": "Calls Offered",
                               "description": "Calls Offered"
                           }
                           , {
                               "key": "_AnsweredinTarget_",
                               "value": "Answered in Target",
                               "description": "Answered in Target"
                           }
                            , {
                                "key": "_GradeofService_",
                                "value": "Grade of Service",
                                "description": "Grade of Service"
                            }
                            , {
                                "key": "_TotalCallsOffered_",
                                "value": "Total Calls Offered",
                                "description": "Total Calls Offered"
                            }
                            , {
                                "key": "_TotalCallsAnswered_",
                                "value": "Total Calls Answered",
                                "description": "Total Calls Answered"
                            }
                            , {
                                "key": "_TotalAnsweredinTarget_",
                                "value": "Total Answered in Target",
                                "description": "Total Answered in Target"
                            }
                            , {
                                "key": "_Filters_",
                                "value": "Filters",
                                "description": "Filters"
                            }
                            , {
                                "key": "_SelectDate/Time_",
                                "value": "Select Date/Time",
                                "description": "Select Date/Time"
                            }
                            , {
                                "key": "_Today_",
                                "value": "Today",
                                "description": "Today"
                            }
                            , {
                                "key": "_ThisWeek_",
                                "value": "This Week",
                                "description": "This Week"
                            }
                            , {
                                "key": "_ThisMonth_",
                                "value": "This Month",
                                "description": "This Month"
                            }
                            , {
                                "key": "_Yesterday_",
                                "value": "Yesterday",
                                "description": "Yesterday"
                            }
                            , {
                                "key": "_LastWeek_",
                                "value": "Last Week",
                                "description": "Last Week"
                            }
                            , {
                                "key": "_LastMonth_",
                                "value": "Last Month",
                                "description": "Last Month"
                            }
                            , {
                                "key": "_Last7Days_",
                                "value": "Last 7 Days",
                                "description": "Last 7 Days"
                            }
                             , {
                                 "key": "_StartDate_",
                                 "value": "Start Date",
                                 "description": "Start Date"
                             }
                             , {
                                 "key": "_EndDate_",
                                 "value": "End Date",
                                 "description": "End Date"
                             }
                             , {
                                 "key": "_DateRange_",
                                 "value": "Date Range",
                                 "description": "Date Range"
                             }
                             , {
                                 "key": "_TimeRange_",
                                 "value": "Time Range",
                                 "description": "Time Range"
                             }
                             , {
                                 "key": "_StartTime_",
                                 "value": "Start Time",
                                 "description": "Start Time"
                             }
                             , {
                                 "key": "_EndTime_",
                                 "value": "End Time",
                                 "description": "End Time"
                             }
                             , {
                                 "key": "_AllExtention_",
                                 "value": "All Extention",
                                 "description": "All Extention"
                             }
                             , {
                                 "key": "_SelectedExtensions_",
                                 "value": "Selected Extensions",
                                 "description": "Selected Extensions"
                             }
                             , {
                                 "key": "_AllDivisions_",
                                 "value": "All Divisions",
                                 "description": "All Divisions"
                             }
                             , {
                                 "key": "_AllDepartments_",
                                 "value": "All Departments",
                                 "description": "All Departments"
                             }
                              , {
                                  "key": "_AllCostCentres_",
                                  "value": "All Cost Centres",
                                  "description": "All Cost Centres"
                              }
                              , {
                                  "key": "_AllDDI's_",
                                  "value": "All DDI's",
                                  "description": "All DDI's"
                              }
                              , {
                                  "key": "_RestrictCampaigns_",
                                  "value": "Restrict Campaigns",
                                  "description": "Restrict Campaigns"
                              }
                              , {
                                  "key": "_RestrictDDI's_",
                                  "value": "Restrict DDI's",
                                  "description": "Restrict DDI's"
                              }
                              , {
                                  "key": "_IncomingCalls_",
                                  "value": "Incoming Calls",
                                  "description": "Incoming Calls"
                              }
                              , {
                                  "key": "_AnswerStatus_",
                                  "value": "Answer Status",
                                  "description": "Answer Status"
                              }
                              , {
                                  "key": "_Any_",
                                  "value": "Any",
                                  "description": "Any"
                              }
                              , {
                                  "key": "_Onlyanswered_",
                                  "value": "Only answered",
                                  "description": "Only answered"
                              }
                              , {
                                  "key": "_Onlyunanswered_",
                                  "value": "Only unanswered",
                                  "description": "Only unanswered"
                              }
                              , {
                                  "key": "_Routing_",
                                  "value": "Routing",
                                  "description": "Routing"
                              }
                              , {
                                  "key": "_Onlytransferred_",
                                  "value": "Only transferred",
                                  "description": "Only transferred"
                              }
                              , {
                                  "key": "_Onlynon-transferredcalls_",
                                  "value": "Only non-transferred calls",
                                  "description": "Only non-transferred calls"
                              }
                              , {
                                  "key": "_BouncedCalls_",
                                  "value": "Bounced Calls",
                                  "description": "Bounced Calls"
                              }
                              , {
                                  "key": "_IncludeBounced_",
                                  "value": "Include Bounced",
                                  "description": "Include Bounced"
                              }
                              , {
                                  "key": "_ExcludeBounced_",
                                  "value": "Exclude Bounced",
                                  "description": "Exclude Bounced"
                              }
                               , {
                                   "key": "_OutgoingCalls_",
                                   "value": "Outgoing Calls",
                                   "description": "Outgoing Calls"
                               }
                               , {
                                   "key": "_Local_",
                                   "value": "Local",
                                   "description": "Local"
                               }
                               , {
                                   "key": "_International_",
                                   "value": "International",
                                   "description": "International"
                               }
                               , {
                                   "key": "_Onlydirectcalls_",
                                   "value": "Only direct calls",
                                   "description": "Only direct calls"
                               }
                               , {
                                   "key": "_Onlytransferredcalls_",
                                   "value": "Only transferred calls",
                                   "description": "Only transferred calls"
                               }
                               , {
                                   "key": "_InternalCalls_",
                                   "value": "Internal Calls",
                                   "description": "Internal Calls"
                               }
                               , {
                                   "key": "_CallDurationRangeRestrictions_",
                                   "value": "Call Duration Range Restrictions",
                                   "description": "Call Duration Range Restrictions"
                               }
                               , {
                                   "key": "_Incoming_",
                                   "value": "Incoming",
                                   "description": "Incoming"
                               }
                               , {
                                   "key": "_Minimum_",
                                   "value": "Minimum",
                                   "description": "Minimum"
                               }
                               , {
                                   "key": "_Maximum_",
                                   "value": "Maximum",
                                   "description": "Maximum"
                               }
                                , {
                                    "key": "_Outgoing_",
                                    "value": "Outgoing",
                                    "description": "Outgoing"
                                }
                                , {
                                    "key": "_RingtimeRangeRestrictions_",
                                    "value": "Ring time Range Restrictions",
                                    "description": "Ring time Range Restrictions"
                                }

                                , {
                                    "key": "_IncomingAnswered_",
                                    "value": "Incoming Answered",
                                    "description": "Incoming Answered"
                                }
                                 , {
                                     "key": "_IncomingUnanswered_",
                                     "value": "Incoming Unanswered",
                                     "description": "Incoming Unanswered"
                                 }
                                 , {
                                     "key": "_Reset_",
                                     "value": "Reset",
                                     "description": "Reset"
                                 }
                                 , {
                                     "key": "_ApplyFilter_",
                                     "value": "Apply Filter",
                                     "description": "Apply Filter"
                                 }
                                 , {
                                     "key": "_ReportCatalogue_",
                                     "value": "Report Catalogue",
                                     "description": "Report Catalogue"
                                 }
                                 , {
                                     "key": "_Companies_",
                                     "value": "Companies",
                                     "description": "Companies"
                                 }
                                 , {
                                     "key": "_AppSettings_",
                                     "value": "App Settings",
                                     "description": "App Settings"
                                 }






]
