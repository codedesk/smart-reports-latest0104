﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Api.Models.Extension
{
    public class ExtensionModel
    {

        public int ExtensionId { get; set; }
        public string Id { get; set; }

        public string Name { get; set; }

        public int? CostCentreId { get; set; }

        public int? DepartmentId { get; set; }

        public int ?DivisionId { get; set; }

        public int ICustomer { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }




    }
}