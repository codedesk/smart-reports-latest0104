﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Api.Models.Product
{
    public class ProductModel
    {
        public List<ProductTypeModel> SelectedDDIList { get; set; }
        public List<ProductTypeModel> SelectedExtensionList { get; set; }

        public List<ProductTypeModel> selectedIVRList { get; set; }

        public List<ProductTypeModel> selectedVoiceMailList { get; set; }

        public int ICustomer { get; set; }
    }

    public class ProductTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}