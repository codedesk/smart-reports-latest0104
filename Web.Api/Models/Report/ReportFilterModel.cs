﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Api.Models.Report
{
    public class ReportFilterModel
    {






        public int PageSize { get; set; }

        public int Page { get; set; }


        public int ReportId { get; set; }

        public int customerId { get; set; }
        public string SelectedDateTime { get; set; }
        public string Allextention { get; set; }

        public string Extension { get; set; }
        public string ExtensionRange { get; set; }

        public string[] SelectedExtensions { get; set; }

        public bool TimeRange { get; set; }

        public string TimeRangeStartTime { get; set; }

        public string TimeRangeEndTime { get; set; }


        public string SelectedStartDate { get; set; }

        public string SelectedEndDate { get; set; }

        public int DivisionId { get; set; }
        public bool Division { get; set; }


        public int DepartmentId { get; set; }
        public bool Department { get; set; }


        public int CostCentreId { get; set; }
        public bool CostCentre { get; set; }

        public int? SiteId { get; set; }
        public string Site { get; set; }

        public int? AccountId { get; set; }
        public string Account { get; set; }

        public int? AccountGroupId { get; set; }
        public string AccountGroup { get; set; }

        public string DDICampaign { get; set; }
        public string SelectRestrictCampaigns { get; set; }

        public string RestrictDDI { get; set; }


        public string[] SelectedDDIs { get; set; }


        public bool IncomingCalls { get; set; }

        public string IncomingCallsAnsStatus { get; set; }

        public string Routing { get; set; }


        public string BouncedCalls { get; set; }


        public bool CheckOutgoingcalls { get; set; }

        public bool OutgoingcallsLocal { get; set; }


        public string OutgoingcallsNational { get; set; }

        public bool OutgoingcallsInternational { get; set; }


        public string OutgoingcallsOther { get; set; }


        public string RadioOutgoingcalls { get; set; }

        public bool CheckInternalcalls { get; set; }

        public string RadioInternalcalls { get; set; }

        public string CheckRestrict { get; set; }

        public string RadioRestrict { get; set; }


        public string RestrictStarts { get; set; }

        public string RestrictSelectFrom { get; set; }

        public string SelectFromDirectory { get; set; }

        public bool IncomingCallDurationRange { get; set; }

        public string Incomingminimum { get; set; }

        public string Incomingmuximum { get; set; }

        public bool OutgoingCallDurationRange { get; set; }

        public string Outgoingminimum { get; set; }

        public string Outgoingmuximum { get; set; }

        public bool IncomingAnsweredRingTime { get; set; }

        public string IncomingAnsweredRingTimeMinimum { get; set; }

        public string IncomingAnsweredRingTimeMaximum { get; set; }

        public bool IncomingUnansweredRingTime { get; set; }

        public string IncomingUnansweredRingTimeMinimum { get; set; }

        public string IncomingUnansweredRingTimeMaximum { get; set; }

        public bool IsLoadPage { get; set; }

        public string total { get; set; }
    }
}