﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Api.ExcelWork
{

    /// <summary>
    /// A helper class to keep track of the sharedstringtable string being created in a spreadsheet
    /// </summary>
    public class OpenXmlWriterHelper
    {
        /// <summary>
        /// contains the shared string as the key, and the index as the value.  index is 0 base
        /// </summary>
        private readonly Dictionary<string, string> _shareStringDictionary = new Dictionary<string, string>();
        private int _shareStringMaxIndex = 0;

        /// <summary>
        /// create the default excel formats.  These formats are required for the excel in order for it to render
        /// correctly.
        /// </summary>
        /// <returns></returns>
        private Stylesheet CreateDefaultStylesheet()
        {

            Stylesheet ss = new Stylesheet();

            Fonts fts = new Fonts();
            DocumentFormat.OpenXml.Spreadsheet.Font ft = new DocumentFormat.OpenXml.Spreadsheet.Font();
            FontName ftn = new FontName();
            ftn.Val = "Calibri";
            FontSize ftsz = new FontSize();
            ftsz.Val = 11;
            ft.FontName = ftn;
            ft.FontSize = ftsz;
            fts.Append(ft);
            fts.Count = (uint)fts.ChildElements.Count;

            Fills fills = new Fills();
            Fill fill;
            PatternFill patternFill;

            //default fills used by Excel, don't changes these

            fill = new Fill();
            patternFill = new PatternFill();
            patternFill.PatternType = PatternValues.None;
            fill.PatternFill = patternFill;
            fills.AppendChild(fill);

            fill = new Fill();
            patternFill = new PatternFill();
            patternFill.PatternType = PatternValues.Gray125;
            fill.PatternFill = patternFill;
            fills.AppendChild(fill);



            fills.Count = (uint)fills.ChildElements.Count;

            Borders borders = new Borders();
            Border border = new Border();
            border.LeftBorder = new LeftBorder();
            border.RightBorder = new RightBorder();
            border.TopBorder = new TopBorder();
            border.BottomBorder = new BottomBorder();
            border.DiagonalBorder = new DiagonalBorder();
            borders.Append(border);
            borders.Count = (uint)borders.ChildElements.Count;

            CellStyleFormats csfs = new CellStyleFormats();
            CellFormat cf = new CellFormat();
            cf.NumberFormatId = 0;
            cf.FontId = 0;
            cf.FillId = 0;
            cf.BorderId = 0;
            csfs.Append(cf);
            csfs.Count = (uint)csfs.ChildElements.Count;


            CellFormats cfs = new CellFormats();

            cf = new CellFormat();
            cf.NumberFormatId = 0;
            cf.FontId = 0;
            cf.FillId = 0;
            cf.BorderId = 0;
            cf.FormatId = 0;
            cfs.Append(cf);



            var nfs = new NumberingFormats();



            nfs.Count = (uint)nfs.ChildElements.Count;
            cfs.Count = (uint)cfs.ChildElements.Count;

            ss.Append(nfs);
            ss.Append(fts);
            ss.Append(fills);
            ss.Append(borders);
            ss.Append(csfs);
            ss.Append(cfs);

            CellStyles css = new CellStyles(
                new CellStyle()
                {
                    Name = "Normal",
                    FormatId = 0,
                    BuiltinId = 0,
                }
                );

            css.Count = (uint)css.ChildElements.Count;
            ss.Append(css);

            DifferentialFormats dfs = new DifferentialFormats();
            dfs.Count = 0;
            ss.Append(dfs);

            TableStyles tss = new TableStyles();
            tss.Count = 0;
            tss.DefaultTableStyle = "TableStyleMedium9";
            tss.DefaultPivotStyle = "PivotStyleLight16";
            ss.Append(tss);
            return ss;
        }
        public Stylesheet GenerateStyleSheet()
        {
            return new Stylesheet(
                new Fonts(
                    // Index 0 - The default font.
                    new Font(                                                              
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    // Index 1 - The bold font.
                    new Font(                                                              
                        new Bold(),
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }
                       


                        ),
                    // Index 2 - The Italic font.
                    new Font(                                                               
                        new Italic(),
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    // Index 3 - The Times Roman font. with 16 size
                    new Font(                                                               
                        new FontSize() { Val = 16 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Times New Roman" })
                ),
                // Index 0 - The default fill.
                new Fills(
                    new Fill(                                                          
                        new PatternFill() { PatternType = PatternValues.None }),

                 // Index 1 - The yellow fill. 
                 new Fill(                                                         
                new PatternFill(
                    new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "f0f68a" } }
                )
                { PatternType = PatternValues.Solid }),
                  // Index 2 - The yellow fill. 
                 new Fill(
                new PatternFill(
                    new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "b6d6b6" } }
                )
                { PatternType = PatternValues.Solid }),
                 // Index 3 - The yellow fill. 
                 new Fill(
                new PatternFill(
                    new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "ededea" } }
                )
                { PatternType = PatternValues.Solid }),
                  new Fill(
                new PatternFill(
                    new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "a2d7d8" } }
                )
                { PatternType = PatternValues.Solid })



        ),


                new Borders(
                    new Border(                                                         // Index 0 - The default border.
                        new LeftBorder(),
                        new RightBorder(),
                        new TopBorder(),
                        new BottomBorder(),
                        new DiagonalBorder()),
                    new Border(                                                         // Index 1 - Applies a Left, Right, Top, Bottom border to a cell
                        new LeftBorder(
                            new Color() { Rgb=new HexBinaryValue{ Value= "f7f2f2"} }
                        )
                        { Style = BorderStyleValues.Thin },
                        new RightBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new TopBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new BottomBorder(
                            new Color() { Auto = true }
                        )
                        { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                ),
              
                new CellFormats(
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 0 },                          // Index 0 - The default cell style.  If a cell does not have a style index applied it will use this style combination instead
                    new CellFormat() { FontId = 1, FillId = 4, BorderId = 1, ApplyFont = true },       // Index 1 - Bold 
                    new CellFormat() { FontId = 0, FillId = 4, BorderId = 1, ApplyFont = true },       // Index 2 - Italic
                    new CellFormat() { FontId = 1, FillId = 2, BorderId = 1, ApplyFont = true },       // Index 3 - Times Roman
                    new CellFormat() { FontId = 0, FillId = 2, BorderId = 1, ApplyFill = true },       // Index 4 - Yellow Fill
                    new CellFormat() { FontId = 1, FillId = 3, BorderId = 1, ApplyFill = true },       // Index 5 - Yellow Fill
                    new CellFormat() { FontId = 0, FillId = 3, BorderId = 1, ApplyFill = true } ,    // Index 6 - Yellow Fill
                    new CellFormat() { FontId = 1, FillId = 0, BorderId = 0, ApplyFont = true }, // Index 7 - Yellow Fill
                     new CellFormat() { FontId = 0, FillId = 3, BorderId = 1, ApplyFont = true } // Index 8 - Yellow Fill


                )
            ); // return
        }

        virtual public void SaveCustomStylesheet(WorkbookPart workbookPart)
        {

            //get a copy of the default excel style sheet then add additional styles to it
            //  var stylesheet =CreateDefaultStylesheet();

            //// ***************************** Fills *********************************
            //var fills = stylesheet.Fills;

            ////header fills background color
            //var fill = new Fill();
            //var patternFill = new PatternFill();
            //patternFill.PatternType = PatternValues.Solid;
            //patternFill.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("C8EEFF") };
            ////patternFill.BackgroundColor = new BackgroundColor() { Indexed = 64 };
            //fill.PatternFill = patternFill;
            //fills.AppendChild(fill);
            //fills.Count = (uint)fills.ChildElements.Count;

            //// *************************** numbering formats ***********************
            //var nfs = stylesheet.NumberingFormats;
            ////number less than 164 is reserved by excel for default formats
            //uint iExcelIndex = 165;
            //NumberingFormat nf;
            //nf = new NumberingFormat();
            //nf.NumberFormatId = iExcelIndex++;
            //nf.FormatCode = @"[$-409]m/d/yy\ h:mm\ AM/PM;@";
            //nfs.Append(nf);

            //nfs.Count = (uint)nfs.ChildElements.Count;

            ////************************** cell formats ***********************************
            //var cfs = stylesheet.CellFormats;//this should already contain a default StyleIndex of 0

            //var cf = new CellFormat();// Date time format is defined as StyleIndex = 1
            //cf.NumberFormatId = nf.NumberFormatId;
            //cf.FontId = 0;
            //cf.FillId = 0;
            //cf.BorderId = 0;
            //cf.FormatId = 0;
            //cf.ApplyNumberFormat = true;
            //cfs.Append(cf);

            //cf = new CellFormat();// Header format is defined as StyleINdex = 2
            //cf.NumberFormatId = 0;
            //cf.FontId = 0;
            //cf.FillId = 2;
            //cf.ApplyFill = true;
            //cf.BorderId = 0;
            //cf.FormatId = 0;
            //cfs.Append(cf);


            //cfs.Count = (uint)cfs.ChildElements.Count;

            var workbookStylesPart = workbookPart.AddNewPart<WorkbookStylesPart>();
           workbookStylesPart.Stylesheet = GenerateStyleSheet();
            workbookStylesPart.Stylesheet.Save();

        }


        /// <summary>
        /// write out the share string xml.  Call this after writing out all shared string values in sheet
        /// </summary>
        /// <param name="workbookPart"></param>
        public void CreateShareStringPart(WorkbookPart workbookPart)
        {
            if (_shareStringMaxIndex > 0)
            {
                var sharedStringPart = workbookPart.AddNewPart<SharedStringTablePart>();
                using (var writer = OpenXmlWriter.Create(sharedStringPart))
                {
                    writer.WriteStartElement(new SharedStringTable());
                    foreach (var item in _shareStringDictionary)
                    {
                        writer.WriteStartElement(new SharedStringItem());
                        writer.WriteElement(new Text(item.Key));
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }
            }

        }
        public CellValues getDataType(string value, string columnType)
        {
           

                CellValues myType = CellValues.String;
            bool hasbool = false;
            Decimal hasDecimal;
            DateTime hasDatetime;
            //var isbool = Boolean.TryParse(value, out hasbool);
            //if (isbool)
            //{
            //    myType = CellValues.Boolean;
            //    return myType;
            //}
           
           
            if (columnType.Trim().ToLower() == "decimal".Trim().ToLower() || columnType.Trim().ToLower() == "double".Trim().ToLower() || columnType.Trim().ToLower() == "int16".Trim().ToLower() || columnType.Trim().ToLower() == "int32".Trim().ToLower() || columnType.Trim().ToLower() == "int64".Trim().ToLower() || columnType.Trim().ToLower() == "single".Trim().ToLower() || columnType.Trim().ToLower() == "uint16".Trim().ToLower() || columnType.Trim().ToLower() == "uint32".Trim().ToLower() || columnType.Trim().ToLower() == "uint64".Trim().ToLower())
            {
               
                myType = CellValues.Number;
                return myType;
            }
            //var isDatetime = DateTime.TryParse(value, out hasDatetime);
            //if (isDatetime)
            //{
            //    myType = CellValues.Number;
            //    return myType;
            //}
            return myType;
        }


        /// <summary>
        /// CellValues = Boolean -> expects cellValue "True" or "False"
        /// CellValues = InlineString -> stores string within sheet
        /// CellValues = SharedString -> stores index within sheet. If this is called, please call CreateShareStringPart after creating all sheet data to create the shared string part
        /// CellValues = Date -> expects ((DateTime)value).ToOADate().ToString(CultureInfo.InvariantCulture) as cellValue 
        ///              and new OpenXmlAttribute[] { new OpenXmlAttribute("s", null, "1") }.ToList() as attributes so that the correct formatting can be applied
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="cellValue"></param>
        /// <param name="dataType"></param>
        /// <param name="attributes"></param>
        public void WriteCellValueSax(OpenXmlWriter writer, string cellValue, CellValues dataType, List<OpenXmlAttribute> attributes = null, uint styleIndex=0, string myDataType= "string")
        {
            if (cellValue.StartsWith("0"))
            {
                cellValue = cellValue.Substring(1);
            }
            switch (dataType)
            {
                case CellValues.InlineString:
                    {
                        dataType = getDataType(cellValue,myDataType);
                        if (attributes == null)
                        {
                            writer.WriteStartElement(new Cell() { DataType = dataType });
                        }
                        else
                        {
                            writer.WriteStartElement(new Cell() { DataType = dataType }, attributes);
                        }
                        writer.WriteElement(new CellValue(cellValue));

                        writer.WriteEndElement();



                       // if (attributes == null)
                       // {
                       //     attributes = new List<OpenXmlAttribute>();
                       // }
                       // var myType = getDataType(cellValue);
                       //attributes.Add(new OpenXmlAttribute("t", null, "inlineStr"));
                       // writer.WriteStartElement(new Cell() { DataType = myType }, attributes);
                       // writer.WriteElement(new CellValue(cellValue));
                       // //writer.WriteStartElement(new Cell(), attributes);
                       // //writer.WriteElement(new InlineString(new Text(cellValue)));
                       // writer.WriteEndElement();
                        break;
                    }
                case CellValues.SharedString:
                    {
                        //if (attributes == null)
                        //{
                        //    attributes = new List<OpenXmlAttribute>();
                        //}
                        //attributes.Add(new OpenXmlAttribute("t", null, "s"));//shared string type
                        //   var cell = new Cell();

                        //  cell.StyleIndex = _UInt32Value;
                        //  CellValue _CellValue = new CellValue(cellValue);
                        // cell.CellValue = _CellValue;
                        UInt32Value _UInt32Value = new UInt32Value(styleIndex);
                        writer.WriteStartElement(new Cell(new CellValue(cellValue)) { DataType = CellValues.String, StyleIndex = _UInt32Value });
                        //if (!_shareStringDictionary.ContainsKey(cellValue))
                        //{
                        //    _shareStringDictionary.Add(cellValue, cellValue);
                        //    _shareStringMaxIndex++;
                        //}

                        //writing the index as the cell value
                      //  writer.WriteElement(new CellValue(_shareStringDictionary[cellValue].ToString()));
                        writer.WriteElement(new CellValue(cellValue));


                        writer.WriteEndElement();//cell

                        break;
                    }
                case CellValues.Date:
                    {
                        if (attributes == null)
                        {
                            writer.WriteStartElement(new Cell() { DataType = CellValues.Number });
                        }
                        else
                        {
                            writer.WriteStartElement(new Cell() { DataType = CellValues.Number }, attributes);
                        }

                        writer.WriteElement(new CellValue(cellValue));

                        writer.WriteEndElement();

                        break;
                    }
                case CellValues.Boolean:
                    {
                        if (attributes == null)
                        {
                            attributes = new List<OpenXmlAttribute>();
                        }
                        attributes.Add(new OpenXmlAttribute("t", null, "b"));//boolean type
                        writer.WriteStartElement(new Cell(), attributes);
                        writer.WriteElement(new CellValue(cellValue == "True" ? "1" : "0"));
                        writer.WriteEndElement();
                        break;
                    }
                default:
                    {
                        if (attributes == null)
                        {
                            writer.WriteStartElement(new Cell() { DataType = dataType });
                        }
                        else
                        {
                            writer.WriteStartElement(new Cell() { DataType = dataType }, attributes);
                        }
                        writer.WriteElement(new CellValue(cellValue));

                        writer.WriteEndElement();


                        break;
                    }
            }

        }




    }
}
