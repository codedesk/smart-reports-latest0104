﻿using DataAccess.BLL;
using Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace Web.Api.Common
{
    public class CommonUtility
    {
        public static void LoginFailedLog(IUserLoginService userLoginService, string ip, string clientId, int? fkUser = null)
        {

            UserLogin tbluserLogin = new UserLogin
            {
                UserId = fkUser,
                IPAddress = ip,
                Successful = false,
                AppId = clientId,
                DateUtc = DateTime.UtcNow,
                IsLoggedIn = false
            };

            userLoginService.InsertUserLogin(tbluserLogin);
        }


        public static string GetHash(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();

            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);

            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);

            return Convert.ToBase64String(byteHash);
        }
    }
}