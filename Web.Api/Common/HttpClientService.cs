﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace Web.Api.Common
{
    public class HttpClientService
    {
        public static async Task<RootActiveCall> Get(string url)
        {
            RootActiveCall root = new RootActiveCall();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.activesoftswitch.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                // New code:
                HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    root = await response.Content.ReadAsAsync<RootActiveCall>();
                }
            }

            return root;
        }


        public static string HttpGet(string URI)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(URI + "?format=json");
            //req.Proxy = new System.Net.WebProxy(ProxyString, true); //true means no proxy
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }

    }
}