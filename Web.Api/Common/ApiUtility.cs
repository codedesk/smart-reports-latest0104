﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Web.Api.Common
{
    public class ApiUtility
    {
        private string BaseURL = System.Configuration.ConfigurationManager.AppSettings["BaseURL"].ToString();
        private string License = System.Configuration.ConfigurationManager.AppSettings["License"].ToString();

        public string HttpCustomerActiveCall(string URI,int iCustomer)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(BaseURL + "/" + URI + "/" + License + "/" + iCustomer+"?format=json");
            //req.Proxy = new System.Net.WebProxy(ProxyString, true); //true means no proxy
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }

        public async Task GetActiveCalls(int iCustomer)
        {
            try
            {
                RootActiveCall root = new RootActiveCall();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string urlCalls = "/GetActiveCallsCustomer/" + License + "/" + iCustomer + "?format=json";

                    HttpResponseMessage CustomerResponse = await client.GetAsync(urlCalls);
                    if (CustomerResponse.IsSuccessStatusCode)
                    {

                        RootActiveCall Customer = await CustomerResponse.Content.ReadAsAsync<RootActiveCall>();

                    }

                }
            }
            catch
            {


            }


        }
    }
}
