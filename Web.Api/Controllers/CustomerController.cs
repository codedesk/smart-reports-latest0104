﻿using Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web.Api.Controllers
{
    public class CustomerController : ApiController
    {
        #region Fields
        private readonly ICustomerService _customerService;

        #endregion

        #region Ctor
        public CustomerController()
        {
            _customerService = StructureMap.ObjectFactory.GetInstance<ICustomerService>();
        }
        #endregion

        #region Api Controllers



        /// <summary>
        /// Customer section start
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("GetCustomers")]

        public IHttpActionResult GetCustomers()
        {
            try
            {
                var roles = _customerService.GetCustomers().ToList();
                return Ok(roles);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }



        #endregion
    }
}
