﻿using Service.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Api.Models.Product;

namespace Web.Api.Controllers
{
    public class ProductController : ApiController
    {
        #region Fields
        private readonly ISpService _spService;


        #endregion

        #region Ctor
        public ProductController()
        {
            _spService = StructureMap.ObjectFactory.GetInstance<ISpService>();

        }
        #endregion

        #region Api Controllers



        /// <summary>
        /// Product section start
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("GetProducts")]

        public IHttpActionResult GetProducts(int icustomer)
        {
            try
            {

                DataTable dt = new DataTable();
                string proc = "prc_getProduects";

                SqlParameter[] param = {
                                             new SqlParameter("@iCustomer",icustomer),                                    
                                       };
                DataSet data = _spService.ExcuteSpAnonmious(proc, param, 8);

                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }



        [HttpPost]
        [Authorize]
        [ActionName("ProductSubmit")]

        public IHttpActionResult ProductSubmit(ProductModel model)
        {
            try
            {

                string query = "";

                query = string.Format("delete pt from product p inner join Product_ProductType pt on p.ID = pt.ProductId where ICustomer= {0};", model.ICustomer);

                foreach (var d in model.SelectedDDIList)
                {
                    query += string.Format("insert into Product_ProductType(ProductId,ProductTypeId) values({0},{1});", d.Id, 1);
                }


                foreach (var d in model.SelectedExtensionList)
                {
                    query += string.Format("insert into Product_ProductType(ProductId,ProductTypeId) values({0},{1});", d.Id, 2);
                }

                foreach (var d in model.selectedIVRList)
                {
                    query += string.Format("insert into Product_ProductType(ProductId,ProductTypeId) values({0},{1});", d.Id, 3);
                }

                foreach (var d in model.selectedVoiceMailList)
                {
                    query += string.Format("insert into Product_ProductType(ProductId,ProductTypeId) values({0},{1});", d.Id, 4);
                }

                SqlParameter[] param = {
                                       };


                int retVal = _spService.ExcuteSp(query, param);

                return Ok(true);

            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }






        #endregion
    }
}
