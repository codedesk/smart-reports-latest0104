﻿using DataAccess.BLL;
using Service.Contracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Api.Models.Extension;

namespace Web.Api.Controllers
{
    public class ExtensionController : ApiController
    {
        #region Fields
        private readonly IExtensionService _extensionService;
        private readonly IExt_Dept_Dev_CCService _ext_Dept_Dev_CCService;
        private readonly ICostCentreService _costCentreService;
        private readonly IDepartmentService _departmentService;
        private readonly IDivisionService _divisionService;
        private readonly ISpService _spService;

        #endregion

        #region Ctor
        public ExtensionController()
        {
            _extensionService = StructureMap.ObjectFactory.GetInstance<IExtensionService>();
            _ext_Dept_Dev_CCService = StructureMap.ObjectFactory.GetInstance<IExt_Dept_Dev_CCService>();
            _costCentreService = StructureMap.ObjectFactory.GetInstance<ICostCentreService>();
            _departmentService = StructureMap.ObjectFactory.GetInstance<IDepartmentService>();
            _divisionService = StructureMap.ObjectFactory.GetInstance<IDivisionService>();
            _spService = StructureMap.ObjectFactory.GetInstance<ISpService>();

        }
        #endregion

        #region Api Controllers


        /// <summary>
        /// Extension section start
        /// </summary>
        /// <returns></returns>

        [HttpPost]
        [Authorize]
        [ActionName("GetExtensions")]

        public IHttpActionResult GetExtensions(ExtensionModel model)
        {
            try
            {

                DataTable dt = new DataTable();
                string proc = "prc_getExtensions";
                string query = string.Empty;

                SqlParameter[] param = {
                                        new SqlParameter("@iCustomer",model.ICustomer), 
                                        new SqlParameter("@CurrentPage",model.Page == 1 ? 0 : (model.Page-1) * model.PageSize),
                                        new SqlParameter("@pageSize",model.PageSize),                                        
                                       };

                DataSet data = _spService.ExcuteSpAnonmious(proc, param, 2);

                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        [HttpGet]
        [Authorize]
        [ActionName("GetExtensionById")]

        public IHttpActionResult GetExtensionById(int id, int iCustomer, int clientId)
        {
            try
            {
                ExtensionModel model = new ExtensionModel();
                var extension = _extensionService.Getextensions(x => x.ExtensionID == id).FirstOrDefault();
                if (extension != null)
                {
                    model.ExtensionId = extension.ExtensionID;
                    model.Id = extension.id.ToString();
                    model.Name = extension.name;

                    var exDetail = _ext_Dept_Dev_CCService.GetExt_Dept_Dev_CCs(x => x.ExtensionId == id).FirstOrDefault();


                    if (exDetail != null)
                    {
                        model.DivisionId = exDetail.DivisionId ?? 0;
                        model.DepartmentId = exDetail.DepartmentId ?? 0;
                        model.CostCentreId = exDetail.CostCentreId ?? 0;
                    }

                }

                DataTable dt = new DataTable();

                SqlParameter[] param = {
                                        new SqlParameter("@iCustomer",iCustomer),                                        
                                        new SqlParameter("@clientId",clientId),                                        
                                       };

                DataSet data = _spService.ExcuteSpAnonmious("prc_report_get_filters", param, 4);

                return Ok(new ArrayList { model, data.Tables[1], data.Tables[2], data.Tables[3] });
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        [HttpPost]
        [Authorize]
        [ActionName("ExtensionSubmit")]

        public IHttpActionResult ExtensionSubmit(ExtensionModel model)
        {
            try
            {

                var extensions = _extensionService.Getextensions(x => x.ExtensionID == model.ExtensionId).FirstOrDefault();

                if (extensions != null)
                {

                    extensions.id = int.Parse(model.Id);
                    extensions.name = model.Name;

                    var exDetail = _ext_Dept_Dev_CCService.GetExt_Dept_Dev_CCs(x => x.ExtensionId == model.ExtensionId).FirstOrDefault();
                    _extensionService.Updateextension(extensions);

                    if (exDetail != null)
                    {

                        exDetail.DivisionId = model.DivisionId == 0 ? null : model.DivisionId;
                        exDetail.DepartmentId = model.DepartmentId == 0 ? null : model.DepartmentId;
                        exDetail.CostCentreId = model.CostCentreId == 0 ? null : model.CostCentreId;

                        _ext_Dept_Dev_CCService.UpdateExt_Dept_Dev_CC(exDetail);

                    }
                    else
                    {
                        Ext_Dept_Dev_CC ext = new Ext_Dept_Dev_CC();
                        ext.ExtensionId = model.ExtensionId;
                        ext.DivisionId = model.DivisionId != 0 ? model.DivisionId : null;
                        ext.DepartmentId = model.DepartmentId != 0 ? model.DepartmentId : null;
                        ext.CostCentreId = model.CostCentreId != 0 ? model.CostCentreId : null;
                        _ext_Dept_Dev_CCService.InsertExt_Dept_Dev_CC(ext);
                    }


                }

                return Ok(true);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }




        #endregion


    }
}
