﻿using DataAccess.BLL;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Service.Contracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using Web.Api.ExcelWork;
using Web.Api.Models.Report;

namespace Web.Api.Controllers
{
    public class ReportController : ApiController
    {
        #region Fields
        private readonly IReportCatalogueService _reportCatalogueService;
        private readonly IReportService _reportService;
        private readonly IExtensionService _extensionService;
        private readonly ISpService _spService;



        #endregion

        #region Ctor
        public ReportController()
        {
            _reportCatalogueService = StructureMap.ObjectFactory.GetInstance<IReportCatalogueService>();
            _reportService = StructureMap.ObjectFactory.GetInstance<IReportService>();
            _extensionService = StructureMap.ObjectFactory.GetInstance<IExtensionService>();
            _spService = StructureMap.ObjectFactory.GetInstance<ISpService>();

        }
        #endregion

        #region Api Controllers

        [HttpGet]
        [Authorize]
        [ActionName("GetReportCatalogues")]

        public IHttpActionResult GetReportCatalogues()
        {
            try
            {
                int[] temp = { 1003,1006 };

  
                var reportCatalogue = _reportCatalogueService.GetReportCatalogues(i=> i.IsActive == true).Select(s => new ReportCatalogue
                    {
                        Id = s.Id,
                        Title = s.Title,
                        Report = _reportService.GetReports(c => c.ReportCatalogueId == s.Id && c.IsActive == true).OrderBy(o => o.OrderNumber).ToList()
                        //x.ReportCatalogueId == s.Id
                    }).OrderBy(o => o.OrderNumber).ToList();


                

                return Ok(reportCatalogue);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }



        [HttpPost]
        [Authorize]
        [ActionName("GetReport")]
        public IHttpActionResult GetReport(ReportFilterModel model)
        {
            try
            {

                if (!model.IsLoadPage)
                {
                    if (model.SelectedDateTime.ToLower() == "today")
                    {
                        model.SelectedStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "thisweek")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "thismonth")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "yesterday")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                    }

                    else if (model.SelectedDateTime.ToLower() == "lastweek")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-14).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "lastmonth")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-60).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "last7days")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-8).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "daterange")
                    {
                        model.SelectedStartDate = Convert.ToDateTime(model.SelectedStartDate).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = Convert.ToDateTime(model.SelectedEndDate).ToString("yyyy-MM-dd");
                    }

                }
                else
                {
                    model.SelectedStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                    model.SelectedEndDate = DateTime.Now.ToString("yyyy-MM-dd");
                }


                if (model.TimeRange)
                {
                    model.TimeRangeStartTime = Convert.ToDateTime(model.TimeRangeStartTime).ToString("HH:mm:ss");
                    model.TimeRangeEndTime = Convert.ToDateTime(model.TimeRangeEndTime).ToString("HH:mm:ss");
                }
                else
                {
                    model.TimeRangeStartTime = "";
                    model.TimeRangeEndTime = "";
                }





                DataTable dt = new DataTable();
                string proc = "prc_ReportExecute";
                string query = string.Empty;
                var report = _reportService.GetReportById(model.ReportId);


                // string extensions = string.Empty;

                var extensions = model.Extension == "allextention" ? "-1" : model.Extension == "extensionrange" ? model.ExtensionRange
                    : string.Join(",", model.SelectedExtensions.Select(s => s.Split('-')[0].Trim()));



                model.DivisionId = !model.Division ? model.DivisionId : -1;
                model.DepartmentId = !model.Department ? model.DepartmentId : -1;
                model.CostCentreId = !model.CostCentre ? model.CostCentreId : -1;


                var ddi = model.DDICampaign == "restrictddi" ? string.Join(",", model.SelectedDDIs) : "";
                int restrictDDi = model.DDICampaign == "restrictcampaigns" ? int.Parse(model.SelectRestrictCampaigns) : -1;




                int calltypeid = -1;
                int? isMissedCall = null;
                int inComingTransfered = -1;
                int outGoingTransfered = -1;
                int bounced = -1;
                int location = -1;

                if (model.IncomingCalls && model.CheckOutgoingcalls)
                {

                    if (model.IncomingCallsAnsStatus == "onlyanswered")
                    {
                        isMissedCall = 0;
                    }
                    else if (model.IncomingCallsAnsStatus == "onlyunanswered")
                    {
                        isMissedCall = 1; ;
                    }


                    if (model.Routing == "onlynon-transferredcalls")
                    {
                        inComingTransfered = 0;
                    }
                    else if (model.Routing == "onlytransferred")
                    {
                        inComingTransfered = 1;
                    }

                    if (model.BouncedCalls == "excludebounced")
                    {
                        bounced = 0;
                    }
                    else if (model.BouncedCalls == "includebounced")
                    {
                        bounced = 1;
                    }





                    // out going

                    if (model.RadioOutgoingcalls == "onlydirectcalls")
                    {
                        outGoingTransfered = 0;
                    }
                    else if (model.RadioOutgoingcalls == "onlytransferredcalls")
                    {
                        outGoingTransfered = 1;
                    }


                    if (model.OutgoingcallsLocal && model.OutgoingcallsInternational)
                    {
                        location = -1;
                    }
                    else if (model.OutgoingcallsLocal)
                    {
                        location = 0;
                    }

                    else if (model.OutgoingcallsInternational)
                    {
                        location = 1;
                    }



                }
                else if (model.IncomingCalls)
                {

                    calltypeid = 1;
                    if (model.IncomingCallsAnsStatus == "onlyanswered")
                    {
                        isMissedCall = 0;
                    }
                    else if (model.IncomingCallsAnsStatus == "onlyunanswered")
                    {
                        isMissedCall = 1;
                    }

                    if (model.Routing == "onlynon-transferredcalls")
                    {
                        inComingTransfered = 0;
                    }
                    else if (model.Routing == "onlytransferred")
                    {
                        inComingTransfered = 1;
                    }


                    if (model.BouncedCalls == "excludebounced")
                    {
                        bounced = 0;
                    }
                    else if (model.BouncedCalls == "includebounced")
                    {
                        bounced = 1;
                    }






                }
                else if (model.CheckOutgoingcalls)
                {
                    calltypeid = 2;

                    if (model.RadioOutgoingcalls == "onlydirectcalls")
                    {
                        outGoingTransfered = 0;
                    }
                    else if (model.RadioOutgoingcalls == "onlytransferredcalls")
                    {
                        outGoingTransfered = 1;
                    }
                    if (model.OutgoingcallsLocal && model.OutgoingcallsInternational)
                    {
                        location = -1;
                    }
                    else if (model.OutgoingcallsLocal)
                    {
                        location = 0;
                    }

                    else if (model.OutgoingcallsInternational)
                    {
                        location = 1;
                    }





                }


                model.IncomingCallsAnsStatus = !model.IncomingCalls ? "" : model.IncomingCallsAnsStatus;

                model.Routing = !model.IncomingCalls ? "" : model.Routing;
                model.BouncedCalls = !model.IncomingCalls ? "" : model.BouncedCalls;


                model.RadioInternalcalls = !model.CheckInternalcalls ? "" : model.RadioInternalcalls;


                model.RadioOutgoingcalls = !model.CheckOutgoingcalls ? "" : model.RadioOutgoingcalls;

                int rCalltypeId = -1;
                int rMissedCall = -1;

                if (model.IncomingCallDurationRange && model.OutgoingCallDurationRange)
                {
                    rCalltypeId = 0;
                }
                else if (model.IncomingCallDurationRange)
                {
                    rCalltypeId = 1;
                }

                else if (model.OutgoingCallDurationRange)
                {
                    rCalltypeId = 2;
                }




                if (model.IncomingAnsweredRingTime && model.IncomingUnansweredRingTime)
                {
                    rMissedCall = 0;
                }
                else if (model.IncomingAnsweredRingTime)
                {
                    rMissedCall = 1;
                }

                else if (model.IncomingUnansweredRingTime)
                {
                    rMissedCall = 2;
                }


                string prcName = model.ReportId == 1012 || 
                    model.ReportId == 1016 ||
                     model.ReportId == 1008 || 
                    model.ReportId == 1 ? "prc_report_Ext" : "prc_report";

              

                query = string.Format(prcName + " {0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}'",
                 model.customerId,
                 model.SelectedStartDate,
                 model.SelectedEndDate,
                 model.TimeRangeStartTime,
                 model.TimeRangeEndTime,
                 extensions,
                 model.DivisionId,
                model.DepartmentId,
                model.CostCentreId,
                model.SiteId,
                model.AccountId,
                model.AccountGroupId,
                model.DDICampaign == "" ? "allddi" : model.DDICampaign,
                restrictDDi,
                ddi,
                model.RadioInternalcalls,
                rCalltypeId,
                rMissedCall,
                Convert.ToDateTime(model.Incomingminimum).TimeOfDay.TotalSeconds,
                Convert.ToDateTime(model.Incomingmuximum).TimeOfDay.TotalSeconds,
                Convert.ToDateTime(model.Outgoingminimum).TimeOfDay.TotalSeconds,
                Convert.ToDateTime(model.Outgoingmuximum).TimeOfDay.TotalSeconds,
                  Convert.ToDateTime(model.IncomingAnsweredRingTimeMinimum).TimeOfDay.TotalSeconds,
                    Convert.ToDateTime(model.IncomingAnsweredRingTimeMaximum).TimeOfDay.TotalSeconds,
                 Convert.ToDateTime(model.IncomingUnansweredRingTimeMinimum).TimeOfDay.TotalSeconds,
                    Convert.ToDateTime(model.IncomingUnansweredRingTimeMaximum).TimeOfDay.TotalSeconds,
                    model.Page == 1 ? 0 : (model.Page - 1) * model.PageSize,
                    model.PageSize,
                    "ASC",
                    calltypeid,
                    isMissedCall,
                    inComingTransfered,
                    outGoingTransfered,
                    location,
                    bounced,
                   model.ReportId
                 );


                SqlParameter[] param = {
                                        new SqlParameter("@CurrentPage","1"),
                                        new SqlParameter("@PageSize","-1"),
                                        new SqlParameter("@Query",query),
                                       };

                DataSet data = _spService.ExcuteSpAnonmious(proc, param, 3);
                //SqlParameter[] param = {
                //                        new SqlParameter("@iCustomer", model.customerId),
                //                        new SqlParameter("@selectedStartDate",model.SelectedStartDate),
                //                       new SqlParameter("@selectedEndDate", model.SelectedEndDate),
                //                       new SqlParameter("@startTimeRange",model.TimeRangeStartTime),
                //                       new SqlParameter("@endTimeRange",model.TimeRangeEndTime),
                //                       new SqlParameter("@extentions",extensions),
                //                       new SqlParameter("@divisionId",model.DivisionId),
                //                       new SqlParameter("@departmentId",model.DepartmentId),
                //                       new SqlParameter("@costcentreId",model.CostCentreId),
                //                       new SqlParameter("@site",model.SiteId),
                //                       new SqlParameter("@accooutId",model.AccountId),
                //                       new SqlParameter("@accooutGroupId",model.AccountGroupId),
                //                       new SqlParameter("@ddiType",model.DDICampaign == "" ? "allddi" : model.DDICampaign),
                //                       new SqlParameter("@restrictcampaigns",restrictDDi),
                //                       new SqlParameter("@ddi",ddi),
                //                       new SqlParameter("@internalCalls",model.RadioInternalcalls),
                //                       new SqlParameter("@RCallTypeID",rCalltypeId),
                //                       new SqlParameter("@RMissedCall",rMissedCall),
                //                       new SqlParameter("@callDurationRangeincomingMinimum",Convert.ToDateTime(model.Incomingminimum).TimeOfDay.TotalSeconds),
                //                       new SqlParameter("@callDurationRangeincomingMaximum",Convert.ToDateTime(model.Incomingmuximum).TimeOfDay.TotalSeconds),
                //                       new SqlParameter("@callDurationRangeOutgoingMinimum",Convert.ToDateTime(model.Outgoingminimum).TimeOfDay.TotalSeconds),
                //                       new SqlParameter("@callDurationRangeOutgoingMaximum",Convert.ToDateTime(model.Outgoingmuximum).TimeOfDay.TotalSeconds),
                //                       new SqlParameter("@IncomingAnsweredRingTimeMinimum",Convert.ToDateTime(model.IncomingAnsweredRingTimeMinimum).TimeOfDay.TotalSeconds),
                //                       new SqlParameter("@IncomingAnsweredRingTimeMaximum",Convert.ToDateTime(model.IncomingAnsweredRingTimeMaximum).TimeOfDay.TotalSeconds),
                //                       new SqlParameter("@incomingUnansweredRingTimeMinimum",Convert.ToDateTime(model.IncomingUnansweredRingTimeMinimum).TimeOfDay.TotalSeconds),
                //                       new SqlParameter("@incomingUnansweredRingTimeMaximum",Convert.ToDateTime(model.IncomingUnansweredRingTimeMaximum).TimeOfDay.TotalSeconds),
                //                       new SqlParameter("@CurrentPage",model.Page == 1 ? 0 : (model.Page - 1) * model.PageSize),
                //                       new SqlParameter("@PageSize",model.PageSize),
                //                       new SqlParameter("@Sorting","ASC"),
                //                       new SqlParameter("@CallTypeId",calltypeid),
                //                       new SqlParameter("@IsMissedCall",isMissedCall),
                //                       new SqlParameter("@InComingTransfered",inComingTransfered),
                //                       new SqlParameter("@OutGoingTransfered",outGoingTransfered),
                //                       new SqlParameter("@Location",location),
                //                       new SqlParameter("@Bounced",bounced),
                //                       new SqlParameter("@ReportId",model.ReportId)
                //                       };
                //DataSet data = _spService.ExcuteSpAnonmious(prcName, param, 3);
                //string ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                //DataSet data = new DataSet();
                //using (SqlConnection conn = new SqlConnection(ConnectionString))
                //{
                //    SqlCommand sqlComm = new SqlCommand(prcName, conn);
                //    sqlComm.Parameters.AddWithValue("@iCustomer", model.customerId);
                //    sqlComm.Parameters.AddWithValue("@selectedStartDate", model.SelectedStartDate);
                //    sqlComm.Parameters.AddWithValue("@selectedEndDate", model.SelectedEndDate);
                //    sqlComm.Parameters.AddWithValue("@startTimeRange", model.TimeRangeStartTime);
                //    sqlComm.Parameters.AddWithValue("@endTimeRange", model.TimeRangeEndTime);
                //    sqlComm.Parameters.AddWithValue("@extentions", extensions);
                //    sqlComm.Parameters.AddWithValue("@divisionId", model.DivisionId);
                //    sqlComm.Parameters.AddWithValue("@departmentId", model.DepartmentId);
                //    sqlComm.Parameters.AddWithValue("@costcentreId", model.CostCentreId);
                //    sqlComm.Parameters.AddWithValue("@site", model.SiteId);
                //    sqlComm.Parameters.AddWithValue("@accooutId", model.AccountId);
                //    sqlComm.Parameters.AddWithValue("@accooutGroupId", model.AccountGroupId);
                //    sqlComm.Parameters.AddWithValue("@ddiType", model.DDICampaign == "" ? "allddi" : model.DDICampaign);
                //    sqlComm.Parameters.AddWithValue("@restrictcampaigns", restrictDDi);
                //    sqlComm.Parameters.AddWithValue("@ddi", ddi);
                //    sqlComm.Parameters.AddWithValue("@internalCalls", model.RadioInternalcalls);
                //    sqlComm.Parameters.AddWithValue("@RCallTypeID", rCalltypeId);
                //    sqlComm.Parameters.AddWithValue("@RMissedCall", rMissedCall);
                //    sqlComm.Parameters.AddWithValue("@callDurationRangeincomingMinimum", Convert.ToDateTime(model.Incomingminimum).TimeOfDay.TotalSeconds);
                //    sqlComm.Parameters.AddWithValue("@callDurationRangeincomingMaximum", Convert.ToDateTime(model.Incomingmuximum).TimeOfDay.TotalSeconds);
                //    sqlComm.Parameters.AddWithValue("@callDurationRangeOutgoingMinimum", Convert.ToDateTime(model.Outgoingminimum).TimeOfDay.TotalSeconds);
                //    sqlComm.Parameters.AddWithValue("@callDurationRangeOutgoingMaximum", Convert.ToDateTime(model.Outgoingmuximum).TimeOfDay.TotalSeconds);
                //    sqlComm.Parameters.AddWithValue("@IncomingAnsweredRingTimeMinimum", Convert.ToDateTime(model.IncomingAnsweredRingTimeMinimum).TimeOfDay.TotalSeconds);
                //    sqlComm.Parameters.AddWithValue("@IncomingAnsweredRingTimeMaximum", Convert.ToDateTime(model.IncomingAnsweredRingTimeMaximum).TimeOfDay.TotalSeconds);
                //    sqlComm.Parameters.AddWithValue("@incomingUnansweredRingTimeMinimum", Convert.ToDateTime(model.IncomingUnansweredRingTimeMinimum).TimeOfDay.TotalSeconds);
                //    sqlComm.Parameters.AddWithValue("@incomingUnansweredRingTimeMaximum", Convert.ToDateTime(model.IncomingUnansweredRingTimeMaximum).TimeOfDay.TotalSeconds);
                //    sqlComm.Parameters.AddWithValue("@CurrentPage", model.Page == 1 ? 0 : (model.Page - 1) * model.PageSize);
                //    sqlComm.Parameters.AddWithValue("@PageSize", model.PageSize);
                //    sqlComm.Parameters.AddWithValue("@Sorting", "ASC");
                //    sqlComm.Parameters.AddWithValue("@CallTypeId", calltypeid);
                //    sqlComm.Parameters.AddWithValue("@IsMissedCall", isMissedCall);
                //    sqlComm.Parameters.AddWithValue("@InComingTransfered", inComingTransfered);
                //    sqlComm.Parameters.AddWithValue("@OutGoingTransfered", outGoingTransfered);
                //    sqlComm.Parameters.AddWithValue("@Location", location);
                //    sqlComm.Parameters.AddWithValue("@Bounced", bounced);
                //    sqlComm.Parameters.AddWithValue("@ReportId", model.ReportId);

                //    sqlComm.CommandType = CommandType.StoredProcedure;

                //    SqlDataAdapter da = new SqlDataAdapter();
                //    da.SelectCommand = sqlComm;

                //    da.Fill(data);
                //}
                return Ok(new ArrayList { data, report.Title });
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }



        [HttpPost]       
        [ActionName("ExportReportToExcel")]
        public object ExportReportToExcel(ReportFilterModel model)
        {
            try
            {

                if (!model.IsLoadPage)
                {
                    if (model.SelectedDateTime.ToLower() == "today")
                    {
                        model.SelectedStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "thisweek")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "thismonth")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "yesterday")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                    }

                    else if (model.SelectedDateTime.ToLower() == "lastweek")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-14).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "lastmonth")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-60).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "last7days")
                    {
                        model.SelectedStartDate = DateTime.Now.AddDays(-8).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                    }
                    else if (model.SelectedDateTime.ToLower() == "daterange")
                    {
                        model.SelectedStartDate = Convert.ToDateTime(model.SelectedStartDate).ToString("yyyy-MM-dd");
                        model.SelectedEndDate = Convert.ToDateTime(model.SelectedEndDate).ToString("yyyy-MM-dd");
                    }

                }
                else
                {
                    model.SelectedStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                    model.SelectedEndDate = DateTime.Now.ToString("yyyy-MM-dd");
                }


                if (model.TimeRange)
                {
                    model.TimeRangeStartTime = Convert.ToDateTime(model.TimeRangeStartTime).ToString("HH:mm:ss");
                    model.TimeRangeEndTime = Convert.ToDateTime(model.TimeRangeEndTime).ToString("HH:mm:ss");
                }
                else
                {
                    model.TimeRangeStartTime = "";
                    model.TimeRangeEndTime = "";
                }





                DataTable dt = new DataTable();
                string proc = "prc_ReportExecute";
                string query = string.Empty;
                var report = _reportService.GetReportById(model.ReportId);


                // string extensions = string.Empty;

                var extensions = model.Extension == "allextention" ? "-1" : model.Extension == "extensionrange" ? model.ExtensionRange
                    : string.Join(",", model.SelectedExtensions.Select(s => s.Split('-')[0].Trim()));



                model.DivisionId = !model.Division ? model.DivisionId : -1;
                model.DepartmentId = !model.Department ? model.DepartmentId : -1;
                model.CostCentreId = !model.CostCentre ? model.CostCentreId : -1;


                var ddi = model.DDICampaign == "restrictddi" ? string.Join(",", model.SelectedDDIs) : "";
                int restrictDDi = model.DDICampaign == "restrictcampaigns" ? int.Parse(model.SelectRestrictCampaigns) : -1;




                int calltypeid = -1;
                int? isMissedCall = null;
                int inComingTransfered = -1;
                int outGoingTransfered = -1;
                int bounced = -1;
                int location = -1;

                if (model.IncomingCalls && model.CheckOutgoingcalls)
                {

                    if (model.IncomingCallsAnsStatus == "onlyanswered")
                    {
                        isMissedCall = 0;
                    }
                    else if (model.IncomingCallsAnsStatus == "onlyunanswered")
                    {
                        isMissedCall = 1; ;
                    }


                    if (model.Routing == "onlynon-transferredcalls")
                    {
                        inComingTransfered = 0;
                    }
                    else if (model.Routing == "onlytransferred")
                    {
                        inComingTransfered = 1;
                    }

                    if (model.BouncedCalls == "excludebounced")
                    {
                        bounced = 0;
                    }
                    else if (model.BouncedCalls == "includebounced")
                    {
                        bounced = 1;
                    }





                    // out going

                    if (model.RadioOutgoingcalls == "onlydirectcalls")
                    {
                        outGoingTransfered = 0;
                    }
                    else if (model.RadioOutgoingcalls == "onlytransferredcalls")
                    {
                        outGoingTransfered = 1;
                    }


                    if (model.OutgoingcallsLocal && model.OutgoingcallsInternational)
                    {
                        location = -1;
                    }
                    else if (model.OutgoingcallsLocal)
                    {
                        location = 0;
                    }

                    else if (model.OutgoingcallsInternational)
                    {
                        location = 1;
                    }



                }
                else if (model.IncomingCalls)
                {

                    calltypeid = 1;
                    if (model.IncomingCallsAnsStatus == "onlyanswered")
                    {
                        isMissedCall = 0;
                    }
                    else if (model.IncomingCallsAnsStatus == "onlyunanswered")
                    {
                        isMissedCall = 1;
                    }

                    if (model.Routing == "onlynon-transferredcalls")
                    {
                        inComingTransfered = 0;
                    }
                    else if (model.Routing == "onlytransferred")
                    {
                        inComingTransfered = 1;
                    }


                    if (model.BouncedCalls == "excludebounced")
                    {
                        bounced = 0;
                    }
                    else if (model.BouncedCalls == "includebounced")
                    {
                        bounced = 1;
                    }






                }
                else if (model.CheckOutgoingcalls)
                {
                    calltypeid = 2;

                    if (model.RadioOutgoingcalls == "onlydirectcalls")
                    {
                        outGoingTransfered = 0;
                    }
                    else if (model.RadioOutgoingcalls == "onlytransferredcalls")
                    {
                        outGoingTransfered = 1;
                    }
                    if (model.OutgoingcallsLocal && model.OutgoingcallsInternational)
                    {
                        location = -1;
                    }
                    else if (model.OutgoingcallsLocal)
                    {
                        location = 0;
                    }

                    else if (model.OutgoingcallsInternational)
                    {
                        location = 1;
                    }





                }


                model.IncomingCallsAnsStatus = !model.IncomingCalls ? "" : model.IncomingCallsAnsStatus;

                model.Routing = !model.IncomingCalls ? "" : model.Routing;
                model.BouncedCalls = !model.IncomingCalls ? "" : model.BouncedCalls;


                model.RadioInternalcalls = !model.CheckInternalcalls ? "" : model.RadioInternalcalls;


                model.RadioOutgoingcalls = !model.CheckOutgoingcalls ? "" : model.RadioOutgoingcalls;

                int rCalltypeId = -1;
                int rMissedCall = -1;

                if (model.IncomingCallDurationRange && model.OutgoingCallDurationRange)
                {
                    rCalltypeId = 0;
                }
                else if (model.IncomingCallDurationRange)
                {
                    rCalltypeId = 1;
                }

                else if (model.OutgoingCallDurationRange)
                {
                    rCalltypeId = 2;
                }




                if (model.IncomingAnsweredRingTime && model.IncomingUnansweredRingTime)
                {
                    rMissedCall = 0;
                }
                else if (model.IncomingAnsweredRingTime)
                {
                    rMissedCall = 1;
                }

                else if (model.IncomingUnansweredRingTime)
                {
                    rMissedCall = 2;
                }

                SqlParameter[] getreport = {
                                        new SqlParameter("@reportId", model.ReportId), 
                                                                             
                                       };
                DataSet getReportName = _spService.ExcuteSpAnonmious("prc_getReportName", getreport, 1);
               
                string prcName = model.ReportId == 1012 ||
                    model.ReportId == 1016 ||
                     model.ReportId == 1008 ||
                    model.ReportId == 1 ? "prc_report_Ext" : "prc_report";



                query = string.Format(prcName + " {0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}'",
                 model.customerId,
                 model.SelectedStartDate,
                 model.SelectedEndDate,
                 model.TimeRangeStartTime,
                 model.TimeRangeEndTime,
                 extensions,
                 model.DivisionId,
                model.DepartmentId,
                model.CostCentreId,
                model.SiteId,
                model.AccountId,
                model.AccountGroupId,
                model.DDICampaign == "" ? "allddi" : model.DDICampaign,
                restrictDDi,
                ddi,
                model.RadioInternalcalls,
                rCalltypeId,
                rMissedCall,
                Convert.ToDateTime(model.Incomingminimum).TimeOfDay.TotalSeconds,
                Convert.ToDateTime(model.Incomingmuximum).TimeOfDay.TotalSeconds,
                Convert.ToDateTime(model.Outgoingminimum).TimeOfDay.TotalSeconds,
                Convert.ToDateTime(model.Outgoingmuximum).TimeOfDay.TotalSeconds,
                  Convert.ToDateTime(model.IncomingAnsweredRingTimeMinimum).TimeOfDay.TotalSeconds,
                    Convert.ToDateTime(model.IncomingAnsweredRingTimeMaximum).TimeOfDay.TotalSeconds,
                 Convert.ToDateTime(model.IncomingUnansweredRingTimeMinimum).TimeOfDay.TotalSeconds,
                    Convert.ToDateTime(model.IncomingUnansweredRingTimeMaximum).TimeOfDay.TotalSeconds,
                    model.Page == 1 ? 0 : (model.Page - 1) * model.PageSize,
                    model.total, //model.PageSize,
                    "ASC",
                    calltypeid,
                    isMissedCall,
                    inComingTransfered,
                    outGoingTransfered,
                    location,
                    bounced,
                   model.ReportId
                 );

               
                SqlParameter[] param = {
                                        new SqlParameter("@CurrentPage","1"),
                                        new SqlParameter("@PageSize","-1"),
                                        new SqlParameter("@Query",query),                                        
                                       };
                string Title = getReportName.Tables[0].Rows[0]["Title"].ToString();
                DataSet data = _spService.ExcuteSpAnonmious(proc, param, 3);
                foreach(var v in data.Tables[2].Rows)
                {
                    var dddd = v;
                }
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                MemoryStream ms = createExcel(data, Title); //generatedDocument;            
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(ms.ToArray());
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = Title + ".xlsx";
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                if (File.Exists(HttpContext.Current.Server.MapPath("~/ExcelSheets/") + Title + ".xlsx"))
                {
                    File.Delete(HttpContext.Current.Server.MapPath("~/ExcelSheets/") + Title + ".xlsx");
                }
                File.WriteAllBytes(HttpContext.Current.Server.MapPath("~/ExcelSheets/") + Title + ".xlsx", ms.ToArray());
                string virtualUrl = "https://" + HttpContext.Current.Request.Url.Authority.ToString() + "/ExcelSheets/" + Title + ".xlsx";
                return virtualUrl;
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        [HttpGet]
        [Authorize]
        [ActionName("GetFiltersData")]

        public IHttpActionResult GetFiltersData(int iCustomer, int clientId)
        {
            try
            {
                DataTable dt = new DataTable();

                SqlParameter[] param = {
                                        new SqlParameter("@iCustomer",iCustomer),                                        
                                        new SqlParameter("@clientId",clientId),                                        
                                       };

                DataSet data = _spService.ExcuteSpAnonmious("prc_report_get_filters", param, 6);

                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        [HttpGet]
        [Authorize]
        [ActionName("GetALLExtensions")]
        public IHttpActionResult GetALLExtensions()
        {
            try
            {
                var extnsions = _extensionService.Getextensions().Select(s => new { extension = s.id + " - " + s.name }).Distinct().ToList();
                return Ok(extnsions);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        #endregion

        #region Functions
        public MemoryStream createExcel(DataTable data, string reportTitle)
        {



            MemoryStream mystream = new MemoryStream();
            using (var spreadSheet = SpreadsheetDocument.Create(mystream, SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = spreadSheet.AddWorkbookPart();

                var openXmlExportHelper = new OpenXmlWriterHelper();
                openXmlExportHelper.SaveCustomStylesheet(workbookPart);


                var workbook = workbookPart.Workbook = new Workbook();
                var sheets = workbook.AppendChild<Sheets>(new Sheets());


                //WorkbookStylesPart stylesPart = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                //stylesPart.Stylesheet = GenerateStyleSheet();
                //stylesPart.Stylesheet.Save();
                // create worksheet 1
                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                var sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };
                sheets.Append(sheet);

                using (var writer = OpenXmlWriter.Create(worksheetPart))
                {

                    writer.WriteStartElement(new Worksheet());
                    writer.WriteStartElement(new SheetData());
                    //Heading
                    writer.WriteStartElement(new Row());
                    var attributes1 = new OpenXmlAttribute[] { new OpenXmlAttribute("s", null, "2") }.ToList();
                    openXmlExportHelper.WriteCellValueSax(writer, reportTitle, CellValues.SharedString, attributes1, 7);
                    writer.WriteEndElement(); //end of Row tag
                    writer.WriteStartElement(new Row());

                    openXmlExportHelper.WriteCellValueSax(writer, "", CellValues.SharedString, attributes1, 7);
                    writer.WriteEndElement(); //end of Row tag
                    // Parent Header
                    writer.WriteStartElement(new Row());
                    for (int i = 0; i < data.Columns.Count; i++)
                    {

                        var attributes = new OpenXmlAttribute[] { new OpenXmlAttribute("s", null, "2") }.ToList();
                        openXmlExportHelper.WriteCellValueSax(writer, data.Columns[i].ColumnName.ToUpper(), CellValues.SharedString, attributes, 1);

                    }
                    writer.WriteEndElement(); //end of Row tag


                    //Parent Row

                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        writer.WriteStartElement(new Row());
                        for (int j = 0; j < data.Columns.Count; j++)
                        {

                            openXmlExportHelper.WriteCellValueSax(writer, Convert.ToString(data.Rows[i][j]), CellValues.InlineString, null, 0);

                        }

                        writer.WriteEndElement(); //end of Row tag
                    }

                    writer.WriteEndElement(); //end of SheetData
                    writer.WriteEndElement(); //end of worksheet
                    writer.Close();
                }


                //create the share string part using sax like approach too
                openXmlExportHelper.CreateShareStringPart(workbookPart);

            }
            return mystream;
            //      HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            //MemoryStream ms = mystream; //generatedDocument;

            //      result = Request.CreateResponse(HttpStatusCode.OK);
            //      result.Content = new ByteArrayContent(ms.ToArray());
            //      result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            //      result.Content.Headers.ContentDisposition.FileName = reportTitle + ".xlsx";
            //      result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

        }
        public MemoryStream createExcel(DataSet dsData, string reportTitle)
        {



            MemoryStream mystream = new MemoryStream();
            using (var spreadSheet = SpreadsheetDocument.Create(mystream, SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = spreadSheet.AddWorkbookPart();

                var openXmlExportHelper = new OpenXmlWriterHelper();
                openXmlExportHelper.SaveCustomStylesheet(workbookPart);


                var workbook = workbookPart.Workbook = new Workbook();
                var sheets = workbook.AppendChild<Sheets>(new Sheets());


                //WorkbookStylesPart stylesPart = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                //stylesPart.Stylesheet = GenerateStyleSheet();
                //stylesPart.Stylesheet.Save();
                // create worksheet 1
                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                var sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Sheet1" };
                sheets.Append(sheet);

                using (var writer = OpenXmlWriter.Create(worksheetPart))
                {

                    writer.WriteStartElement(new Worksheet());
                    writer.WriteStartElement(new SheetData());
                    //Heading
                    writer.WriteStartElement(new Row());
                    var attributes1 = new OpenXmlAttribute[] { new OpenXmlAttribute("s", null, "2") }.ToList();
                    openXmlExportHelper.WriteCellValueSax(writer, reportTitle, CellValues.SharedString, attributes1, 7);
                    writer.WriteEndElement(); //end of Row tag
                    writer.WriteStartElement(new Row());

                    openXmlExportHelper.WriteCellValueSax(writer, "", CellValues.SharedString, attributes1, 7);
                    writer.WriteEndElement(); //end of Row tag
                    // Parent Header
                    int count = 0;
                    foreach (DataTable data in dsData.Tables)
                    {
                        if (count < 2)
                        {

                            writer.WriteStartElement(new Row());
                            for (int i = 0; i < data.Columns.Count; i++)
                            {

                                var attributes = new OpenXmlAttribute[] { new OpenXmlAttribute("s", null, "2") }.ToList();
                               
                                    openXmlExportHelper.WriteCellValueSax(writer, data.Columns[i].ColumnName.ToUpper(), CellValues.SharedString, attributes, 1);

                                

                            }
                            writer.WriteEndElement(); //end of Row tag


                            //Parent Row

                            for (int i = 0; i < data.Rows.Count; i++)
                            {
                                writer.WriteStartElement(new Row());
                                for (int j = 0; j < data.Columns.Count; j++)
                                {
                                    if (count < 1)
                                    {
                                        openXmlExportHelper.WriteCellValueSax(writer, Convert.ToString(data.Rows[i][j]), CellValues.InlineString, null, 0);
                                    }
                                    else
                                    {
                                        openXmlExportHelper.WriteCellValueSax(writer, Convert.ToString(data.Rows[i][j]), CellValues.SharedString, null, 8);

                                    }
                                }

                                writer.WriteEndElement(); //end of Row tag
                            }
                        }
                        count++;
                    }

                    writer.WriteEndElement(); //end of SheetData
                    writer.WriteEndElement(); //end of worksheet
                    
                    writer.Close();
                }


                //create the share string part using sax like approach too
                openXmlExportHelper.CreateShareStringPart(workbookPart);
               
            }
            return mystream;
            //      HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            //MemoryStream ms = mystream; //generatedDocument;

            //      result = Request.CreateResponse(HttpStatusCode.OK);
            //      result.Content = new ByteArrayContent(ms.ToArray());
            //      result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            //      result.Content.Headers.ContentDisposition.FileName = reportTitle + ".xlsx";
            //      result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

        }
        #endregion
    }
}

