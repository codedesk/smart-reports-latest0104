﻿using DataAccess.BLL;
using Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web.Api.Controllers
{
    public class DivisionController : ApiController
    {
         #region Fields
        private readonly IDivisionService _divisionService;

        #endregion

        #region Ctor
        public DivisionController()
        {
            _divisionService = StructureMap.ObjectFactory.GetInstance<IDivisionService>();
        }
        #endregion

        #region Api Controllers



        /// <summary>
        /// Division section start
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("GetDivisions")]

        public IHttpActionResult GetDivisions(int? clientId)
        {
            try
            {
                var data = _divisionService.GetDivisions(x => x.ClientId == clientId).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        [HttpGet]
        [Authorize]
        [ActionName("GetDivisionById")]

        public IHttpActionResult GetDivisionById(int id, int? clientId)
        {
            try
            {
                var data = _divisionService.GetDivisions(x => x.Id == id && x.ClientId == clientId).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        [HttpPost]
        [Authorize]
        [ActionName("DivisionSubmit")]

        public IHttpActionResult DivisionSubmit(Division model)
        {
            try
            {

                var data = _divisionService.GetDivisions(x => x.DivisionName.Trim() == model.DivisionName.Trim() && x.ClientId == model.ClientId).ToList();


                if (model.Id <= 0)
                {
                    if (data.Any())
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "Division name already exist."));
                    }
                    _divisionService.InsertDivision(model);
                }
                else
                {
                    if (data.Count() > 1)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "Division name already exist."));
                    }


                    var role = _divisionService.GetDivisionById(model.Id);

                    if (role != null)
                    {
                        role.DivisionName = model.DivisionName;
                        _divisionService.UpdateDivision(role);
                    }
                    else
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "Division Id does not exist, please try again!"));
                    }


                }

                return Ok(true);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }




        [HttpGet]
        [Authorize]
        [ActionName("DeleteDivision")]
        public IHttpActionResult DeleteDivision(int id, int? clientId)
        {
            try
            {
                var roles = _divisionService.GetDivisions(x=> x.Id == id && x.ClientId == clientId).FirstOrDefault();

                if (roles == null)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "Division does not exist."));
                }

                _divisionService.DeleteDivision(roles);
                var roleList = _divisionService.GetDivisions(x => x.ClientId == clientId).ToList();
                return Ok(roleList);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        #endregion

    }
}
