﻿using DataAccess.BLL;
using Microsoft.Web.Administration;
using Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web.Api.Controllers
{
    public class SystemSettingController : ApiController
    {

        #region Fields
        private readonly ISystemSettingService _systemSettingervice;

        #endregion

        #region Ctor
        public SystemSettingController()
        {
            _systemSettingervice = StructureMap.ObjectFactory.GetInstance<ISystemSettingService>();
        }
        #endregion

        #region Api Controllers



        /// <summary>
        /// SystemSetting section start
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("GetSystemSettings")]

        public IHttpActionResult GetSystemSettings(int iCustomer)
        {
            try
            {
                var systemSettings = _systemSettingervice.GetSystemSettings(x => x.ICustomer == iCustomer).FirstOrDefault();
                return Ok(systemSettings);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        [HttpGet]
        [Authorize]
        [ActionName("GetSystemSettingById")]

        public IHttpActionResult GetSystemSettingById(int id)
        {
            try
            {
                var role = _systemSettingervice.GetSystemSettings(x => x.Id == id).FirstOrDefault();
                return Ok(role);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        [HttpPost]
        [Authorize]
        [ActionName("SystemSettingSubmit")]

        public IHttpActionResult SystemSettingSubmit(SystemSetting model)
        {
            try
            {
                if (model != null)
                {
                    var exisData = _systemSettingervice.GetSystemSettings(x => x.ICustomer == model.ICustomer).FirstOrDefault();

                    if (exisData != null)
                    {
                        exisData.LocalCode = model.LocalCode;
                        exisData.AnswerTime = model.AnswerTime;
                        exisData.MaxCallDuration = model.MaxCallDuration;
                        _systemSettingervice.UpdateSystemSetting(exisData);
                    }
                    else
                    {
                        _systemSettingervice.InsertSystemSetting(model);
                    }


                }

               
                return Ok(true);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }





        #endregion

        void ChangeSiteBinding(string siteName, string oldBindingValue, string newBindingValue, string protocol)
        {
            using (ServerManager manager = new ServerManager())
            {
                // Find the site by name from IIS
                Microsoft.Web.Administration.Site site = manager.Sites.Where(q => q.Name == siteName).FirstOrDefault();
                if (site == null)
                {
                    throw new Exception("The specified site name does not exist in IIS!");
                }
                else
                {
                    bool found = false;

                    // Loop through all the site's bindings
                    foreach (var binding in site.Bindings)
                    {
                        // If the binding protocol matches the passed protocol, continue

                        if (binding.Protocol.Equals(protocol))
                        {
                            // If the current binding value matches the old binding value passed, continue
                            if (binding.BindingInformation.Equals(oldBindingValue))
                            {
                                // Set this to indicate the binding was found
                                found = true;
                                // Set the binding value to the new passed value
                                binding.BindingInformation = newBindingValue;
                                break;
                            }
                        }
                    }

                    if (found)
                    {
                        // If the binding was found and changed, commit the changes.
                        manager.CommitChanges();
                    }
                    else
                    {
                        // If the binding was not found, throw an exception indicating as much.
                        throw new Exception("The specified old binding does not exist on this site!");
                    }
                }
            }
        }
    }
}
