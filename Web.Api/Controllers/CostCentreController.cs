﻿using DataAccess.BLL;
using Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web.Api.Controllers
{
    public class CostCentreController : ApiController
    {
        #region Fields
        private readonly ICostCentreService _costCentreService;

        #endregion

        #region Ctor
        public CostCentreController()
        {
            _costCentreService = StructureMap.ObjectFactory.GetInstance<ICostCentreService>();
        }
        #endregion

        #region Api Controllers



        /// <summary>
        /// CostCentre section start
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("GetCostCentres")]

        public IHttpActionResult GetCostCentres(int? clientId)
        {
            try
            {
                var data = _costCentreService.GetCostCentres(x => x.ClientId == clientId).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        [HttpGet]
        [Authorize]
        [ActionName("GetCostCentreById")]

        public IHttpActionResult GetCostCentreById(int id, int? clientId)
        {
            try
            {
                var data = _costCentreService.GetCostCentres(x => x.Id == id && x.ClientId == clientId).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        [HttpPost]
        [Authorize]
        [ActionName("CostCentreSubmit")]

        public IHttpActionResult CostCentreSubmit(CostCentre model)
        {
            try
            {

                var costcentres = _costCentreService.GetCostCentres(x => x.CostCentreName.Trim() == model.CostCentreName.Trim() && x.ClientId == model.ClientId).ToList();

                if (model.Id <= 0)
                {
                    if (costcentres.Any())
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "CostCentre name already exist."));
                    }
                    _costCentreService.InsertCostCentre(model);
                }
                else
                {
                    if (costcentres.Count() > 1)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "CostCentre name already exist."));
                    }


                    var costcentre = _costCentreService.GetCostCentreById(model.Id);

                    if (costcentre != null)
                    {
                        costcentre.CostCentreName = model.CostCentreName;
                        _costCentreService.UpdateCostCentre(costcentre);
                    }
                    else
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "CostCentre Id does not exist, please try again!"));
                    }


                }

                return Ok(true);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }




        [HttpGet]
        [Authorize]
        [ActionName("DeleteCostCentre")]
        public IHttpActionResult DeleteCostCentre(int id, int? clientId)
        {
            try
            {
                var costCentres = _costCentreService.GetCostCentres(x => x.Id == id && x.ClientId == clientId).FirstOrDefault();

                if (costCentres == null)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "CostCentre does not exist."));
                }

                _costCentreService.DeleteCostCentre(costCentres);
                var roleList = _costCentreService.GetCostCentres(x => x.ClientId == clientId).ToList();
                return Ok(roleList);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        #endregion
    }
}
