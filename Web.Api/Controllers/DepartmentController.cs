﻿using DataAccess.BLL;
using Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web.Api.Controllers
{
    public class DepartmentController : ApiController
    {


        #region Fields
        private readonly IDepartmentService _departmentService;

        #endregion

        #region Ctor
        public DepartmentController()
        {
            _departmentService = StructureMap.ObjectFactory.GetInstance<IDepartmentService>();
        }
        #endregion

        #region Api Controllers



        /// <summary>
        /// Department section start
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Authorize]
        [ActionName("GetDepartments")]

        public IHttpActionResult GetDepartments(int? clientId)
        {
            try
            {
                var data = _departmentService.GetDepartments(x => x.ClientId == clientId).ToList();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        [HttpGet]
        [Authorize]
        [ActionName("GetDepartmentById")]

        public IHttpActionResult GetDepartmentById(int id, int? clientId)
        {
            try
            {
                var data = _departmentService.GetDepartments(x => x.Id == id && x.ClientId == clientId).FirstOrDefault();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        [HttpPost]
        [Authorize]
        [ActionName("DepartmentSubmit")]

        public IHttpActionResult DepartmentSubmit(Department model)
        {
            try
            {

                var data = _departmentService.GetDepartments(x => x.DepartmentName.Trim() == model.DepartmentName.Trim() && x.ClientId == model.ClientId).ToList();


                if (model.Id <= 0)
                {
                    if (data.Any())
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "Department name already exist."));
                    }
                    _departmentService.InsertDepartment(model);
                }
                else
                {
                    if (data.Count() > 1)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "Department name already exist."));
                    }


                    var depart = _departmentService.GetDepartmentById(model.Id);

                    if (depart != null)
                    {
                        depart.DepartmentName = model.DepartmentName;
                        _departmentService.UpdateDepartment(depart);
                    }
                    else
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "Department Id does not exist, please try again!"));
                    }


                }

                return Ok(true);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }




        [HttpGet]
        [Authorize]
        [ActionName("DeleteDepartment")]
        public IHttpActionResult DeleteDepartment(int id, int? clientId)
        {
            try
            {
                var roles = _departmentService.GetDepartments(x => x.Id == id && x.ClientId == clientId).FirstOrDefault();

                if (roles == null)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, "Department does not exist."));
                }

                _departmentService.DeleteDepartment(roles);
                var roleList = _departmentService.GetDepartments(x => x.ClientId == clientId).ToList();
                return Ok(roleList);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        #endregion
    }
}
