﻿using Models;
using PortaUtility;
using Service.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Web.Api.Common;

namespace Web.Api.Controllers
{
    public class DashboardController : ApiController
    {

        #region Fields
        private readonly ISpService _spService;

        #endregion

        #region Ctor
        public DashboardController()
        {
            _spService = StructureMap.ObjectFactory.GetInstance<ISpService>();

        }
        #endregion

        #region Api Controllers

        [HttpGet]
        [Authorize]
        [ActionName("GetDashboard")]

        public IHttpActionResult GetDashboard(int iCustomer)
        {
            try
            {

                SqlParameter[] param = {
                                        new SqlParameter("@iCustomer",iCustomer),                                        
                                       };

                DataSet data = _spService.ExcuteSpAnonmious("prc_dashboard", param, 3);


                DashBoardData dashboard = new DashBoardData();
                dashboard.WidgetData = new WidgetData();

                string custoemrid = StringCipher.Encrypt(iCustomer.ToString());
                string encodedCustomerId = HttpUtility.UrlEncode(custoemrid);

                dashboard.WidgetData.ICustomer = encodedCustomerId;
                dashboard.WidgetData.TotalCalls = (int)data.Tables[0].Rows[0]["TotalCalls"];
                dashboard.WidgetData.TotalOut = (int)data.Tables[0].Rows[0]["TotalOut"];
                dashboard.WidgetData.TotalIn = (int)data.Tables[0].Rows[0]["TotalIn"];
                dashboard.WidgetData.Internal = (int)data.Tables[0].Rows[0]["Internal"];
                dashboard.WidgetData.External = (int)data.Tables[0].Rows[0]["External"];
                dashboard.WidgetData.TotalAnswered = (int)data.Tables[0].Rows[0]["TotalAnswered"];
                dashboard.WidgetData.TotalMissed = (int)data.Tables[0].Rows[0]["TotalMissed"];
                dashboard.WidgetData.talkTime = Convert.ToString( data.Tables[0].Rows[0]["talkTime"]);

                dashboard.DeptWiseCalls = new List<DepartWiseCall>();

                foreach (DataRow row in data.Tables[1].Rows)
                {
                    dashboard.DeptWiseCalls.Add(new DepartWiseCall
                    {
                        Department = Convert.ToString(row["Department"]),
                        totalCalls = Convert.ToInt32(row["totalCalls"]),
                        TotalOut = Convert.ToInt32(row["TotalOut"]),
                        TotalIn = Convert.ToInt32(row["TotalIn"]),
                        Missed = Convert.ToInt32(row["Missed"]),
                        Transferred = Convert.ToInt32(row["Transferred"]),
                        AvgTalkTime = Convert.ToString(row["AvgTalkTime"]),
                        TotalTalkTime = Convert.ToString(row["TotalTalkTime"])
                    });
                }

                dashboard.PeriodWiseCalls = new List<PeriodtWiseCall>();

                foreach (DataRow row in data.Tables[2].Rows)
                {
                    dashboard.PeriodWiseCalls.Add(new PeriodtWiseCall
                    {
                        Period = Convert.ToString(row["Period"]),
                        In = Convert.ToInt32(row["In"]),
                    });
                }

                return Ok(dashboard);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }



        [HttpGet]
        [AllowAnonymous]
        [ActionName("GetWallboard")]

        public IHttpActionResult GetWallboard(string iCustomer)
        {
            try
            {
                string DecodedId = HttpUtility.UrlDecode(iCustomer);
                string customer = StringCipher.Decrypt(DecodedId);

                SqlParameter[] param = {
                                        new SqlParameter("@iCustomer", customer),                                        
                                       };

                DataSet data = _spService.ExcuteSpAnonmious("prc_wallboard", param, 4);

                WallBoardData wallboard = new WallBoardData();
                if (data != null && data.Tables != null && data.Tables.Count > 0)
                {
                    foreach (DataRow row in data.Tables[0].Rows)
                    {
                        wallboard.inboundCalls = Convert.ToString(row["inboundCalls"]);
                        wallboard.answeredCalls = Convert.ToString(row["answeredCalls"]);
                        wallboard.talkTime = Convert.ToString(row["talkTime"]);
                        wallboard.totalMissed = Convert.ToString(row["totalMissed"]);
                        wallboard.averageTalkTime = Convert.ToString(row["averageTalkTime"]);
                        wallboard.outboundCalls = Convert.ToString(row["outboundCalls"]);
                        wallboard.maxIn = Convert.ToString(row["maxIn"]);
                        wallboard.maxOut = Convert.ToString(row["maxOut"]);
                    }

                }

                List<LstActiveCall> lst = ActiveCalls(Convert.ToInt32(customer));

                wallboard.totalActive = lst.Count().ToString();
                wallboard.totalIn = lst.Where(i => i.CallType == "INCOMING").Count().ToString();
                wallboard.totalOut = lst.Where(i => i.CallType == "OUTGOING").Count().ToString();
                wallboard.totalWaiting = lst.Where(i => i.CallType == "WAITING").Count().ToString();

                wallboard.HourlyInCalls = GetIncoming(data.Tables[1]);
                wallboard.HourlyOutCalls = GetOutGoing(data.Tables[1]);
                wallboard.callsByExtention = getCallByExtension(data.Tables[3]);
                return Ok(wallboard);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }



        [HttpGet]
        [Authorize]
        [ActionName("GetActiveCalls")]

        public IHttpActionResult GetActiveCalls(int iCustomer)
        {
            try
            {

                //foreach (var item in data.lstActiveCalls)
                //{
                //    LstActiveCall obj = new LstActiveCall();
                //    obj.CLI = item.CLI;
                //    obj.CLD = item.CLD;

                //    SqlParameter[] param = {
                //                        new SqlParameter("@iAccount",item.IAccount),                                        
                //                       };

                //    var accountName = _spService.ExcuteSpAnonmious("prc_getExtensionName", param, 1).Tables[0];
                //    obj.Name = accountName.Rows.Count > 0 ? accountName.AsEnumerable().FirstOrDefault().Field<string>("name") : "";
                //    obj.DurationMins = item.DurationMins;
                //    obj.CallType = item.CallType;
                //    list.Add(obj);
                //}

                var lst = ActiveCalls(iCustomer);
                var filteredcalls = lst.Where(i => i.CallType != "WAITING").ToList();

                return Ok(filteredcalls);

            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }



        [HttpGet]
        [Authorize]
        [ActionName("GetLanguage")]
        public IHttpActionResult GetLanguage(int userID)
        {
            try
            {

                SqlParameter[] param = {
                                        new SqlParameter("@userID",userID),                                        
                                       };

                DataSet data = _spService.ExcuteSpAnonmious("prc_getLanguage", param, 1);

                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        [HttpGet]
        [Authorize]
        [ActionName("UpdateLanguage")]
        public IHttpActionResult UpdateLanguage(int userID, string language)
        {
            try
            {

                SqlParameter[] param = {
                                        new SqlParameter("@userID",userID),
                                        new SqlParameter("@language", language)
                                       };

                DataSet data = _spService.ExcuteSpAnonmious("prc_updateLanguage", param, 1);

                return Ok(data);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }
        private List<LstActiveCall> ActiveCalls(int iCustomer)
        {
            List<LstActiveCall> list = new List<LstActiveCall>();
            try
            {

                //var activeCallsJson = new ApiUtility().HttpCustomerActiveCall("GetActiveCallsCustomer", iCustomer);
                //var json = new JavaScriptSerializer();
                //RootActiveCall data = json.Deserialize<RootActiveCall>(activeCallsJson);
               
                CDRUtilities cdrutil = new CDRUtilities();
                
               string myConnection = System.Configuration.ConfigurationManager.ConnectionStrings["Default"].ToString();
                var data = cdrutil.GetActiveCalls(iCustomer, myConnection);
                list = cdrutil.ProcessActiveCalls(iCustomer, data, myConnection);
            }
            catch
            {
                throw;
            }

            return list;
        }

        public List<CallsbyExtension> getCallByExtension(DataTable dt)
        {
            List<CallsbyExtension> listCallsbyExtensions = new List<CallsbyExtension>();
            foreach (DataRow row in dt.Rows)
            {
                CallsbyExtension callsbyExtension = new CallsbyExtension();
                callsbyExtension.AvgTalkTime = Convert.ToString(row["AvgTalkTime"]);
                callsbyExtension.In = Convert.ToString(row["In"]);
                callsbyExtension.Out = Convert.ToString(row["Out"]);
                callsbyExtension.ExtensionName = Convert.ToString(row["ExtensionName"]);

                callsbyExtension.ExtensionNumber = Convert.ToString(row["ExtensionNumber"]);
                callsbyExtension.Internal = Convert.ToString(row["Internal"]);
                callsbyExtension.Missed = Convert.ToString(row["Missed"]);
                callsbyExtension.TotalCalls = Convert.ToString(row["TotalCalls"]);
                callsbyExtension.totalminutes = Convert.ToString(row["totalminutes"]);
                callsbyExtension.TotalTalkTime = Convert.ToString(row["TotalTalkTime"]);
                listCallsbyExtensions.Add(callsbyExtension);
            }
            return listCallsbyExtensions;
        }
        private List<LineChart> GetIncoming(DataTable DT)
        {
            List<LineChart> HourlyInCalls = new List<LineChart>();

            var objLst = new LineChart();

            objLst.color = "#1d93d9";
            objLst.label = "Hourly Calls";

            objLst.data = new List<List<string>>();

            foreach (DataRow row in DT.Rows)
            {
                var LstObj = new List<string>();

                LstObj.Add(Convert.ToString(row["Period"]));
                LstObj.Add(Convert.ToString(row["In"]));

                objLst.data.Add(LstObj);
            }

            HourlyInCalls.Add(objLst);

            return HourlyInCalls;

        }

       
        private List<LineChart> GetOutGoing(DataTable DT)
        {
            List<LineChart> HourlyOutCalls = new List<LineChart>();

            var objLst = new LineChart();

            objLst.color = "#1d93d9";
            objLst.label = "Hourly Calls";

            objLst.data = new List<List<string>>();

            foreach (DataRow row in DT.Rows)
            {
                var LstObj = new List<string>();

                LstObj.Add(Convert.ToString(row["Period"]));
                LstObj.Add(Convert.ToString(row["Out"]));

                objLst.data.Add(LstObj);
            }

            HourlyOutCalls.Add(objLst);

            return HourlyOutCalls;

        }


        #endregion
    }

    public class DashBoardData
    {
        public WidgetData WidgetData { get; set; }
        public List<DepartWiseCall> DeptWiseCalls { get; set; }
        public List<PeriodtWiseCall> PeriodWiseCalls { get; set; }
    }

    public class WidgetData
    {
        public string ICustomer { get; set; }
        public int TotalCalls { get; set; }
        public int TotalOut { get; set; }
        public int TotalIn { get; set; }
        public int Internal { get; set; }
        public int External { get; set; }
        public int TotalAnswered { get; set; }
        public int TotalMissed { get; set; }
        public string talkTime { get; set; }

    }

    public class DepartWiseCall
    {
        public string Department { get; set; }
        public int totalCalls { get; set; }
        public int TotalOut { get; set; }
        public int TotalIn { get; set; }
        public int Missed { get; set; }
        public int Transferred { get; set; }
        public string AvgTalkTime { get; set; }
        public string TotalTalkTime { get; set; }
    }

    public class PeriodtWiseCall
    {
        public string Period { get; set; }
        public int In { get; set; }
    }

    public class WallBoardData
    {
        public int MyProperty { get; set; }

        public string inboundCalls { get; set; }
        public string outboundCalls { get; set; }

        public string answeredCalls { get; set; }
        public string totalMissed { get; set; }

        public string talkTime { get; set; }
        public string averageTalkTime { get; set; }


        public string totalActive { get; set; }
        public string totalIn { get; set; }
        public string totalOut { get; set; }

        public string  totalWaiting { get; set; }

        public string maxIn { get; set; }
        public string maxOut { get; set; }

        public List<LineChart> HourlyInCalls = new List<LineChart>();
        public List<LineChart> HourlyOutCalls = new List<LineChart>();
        public List<CallsbyExtension> callsByExtention = new List<CallsbyExtension>();

    }


    public class CallsbyExtension
    {
        public string ExtensionNumber { get; set; }
        public string ExtensionName { get; set; }

        public string TotalCalls { get; set; }
        public string Out { get; set; }
        public string In { get; set; }
        public string Missed { get; set; }

        public string Internal { get; set; }

        public string TotalTalkTime { get; set; }
        public string AvgTalkTime { get; set; }

        public string totalminutes { get; set; }

    }
    public class LineChart
    {
        public string label { get; set; }
        public string color { get; set; }
        public List<List<string>> data { get; set; }
    }
}
