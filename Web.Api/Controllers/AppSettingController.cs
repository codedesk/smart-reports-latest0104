﻿using DataAccess.BLL;
using Microsoft.Web.Administration;
using Service.Contracts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web.Api.Controllers
{
    public class AppSettingController : ApiController
    {

        #region Fields
        public IAppSettingService _appsettingervice { get; set; }

        #endregion

        #region Ctor
        public AppSettingController()
        {
            _appsettingervice = StructureMap.ObjectFactory.GetInstance<IAppSettingService>();
        }
        #endregion

        #region Api Controllers



        /// <summary>
        /// SystemSetting section start
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [AllowAnonymous]
        [ActionName("GetAppSetting")]

        public IHttpActionResult GetAppSetting()
        {
            try
            {
                var systemSettings = _appsettingervice.GetAppSetting();

                AppSetting appsetting = new AppSetting();

                if(systemSettings != null){
                   appsetting= systemSettings.FirstOrDefault();
                }

                return Ok(appsetting);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }


        [HttpPost]
        [Authorize]
        [ActionName("AppSettingSubmit")]

        public IHttpActionResult AppSettingSubmit(AppSetting model)
        {
            try
            {
                if (model != null)
                {



                    var exisData = _appsettingervice.GetAppSetting().FirstOrDefault();

                    if (exisData != null)
                    {
                        exisData.LicenseKey = model.LicenseKey;
                        exisData.Logo = model.Logo;
                        exisData.CustomStyles = model.CustomStyles;
                        _appsettingervice.UpdateAppSetting(exisData);
                    }
                    else
                    {
                        _appsettingervice.InsertAppSetting(model);
                    }

                    LoadImage(exisData.Logo);
                }

               
                return Ok(true);
            }
            catch (Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }

        public void LoadImage(string logo)
        {
            if (!string.IsNullOrEmpty(logo))
            {
                string base64string = logo.Replace("data:image/png;base64,", "");
                byte[] bytes = Convert.FromBase64String(base64string);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }

                string physicalPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Assets/Images/CompanyLogo");

                image.Save(physicalPath + "/Logo.png", System.Drawing.Imaging.ImageFormat.Png);

            }
        }

        [HttpGet]
        [Authorize]
        [ActionName("CheckSession")]

        public IHttpActionResult CheckSession()
        {
            try
            {
                return Ok(true);
            }
            catch (Exception ex)
            {
                return Ok(false);
            }
        }

     
        #endregion



     
    }
}
