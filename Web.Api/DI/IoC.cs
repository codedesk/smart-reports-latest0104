﻿using Service.Contracts;
using Service.Services;
using StructureMap;
using StructureMap.Graph;
using System.Web.ApplicationServices;

namespace Web.Api.DI
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
            {
                x.Scan(scan =>
                {
                    scan.WithDefaultConventions();
                    scan.TheCallingAssembly();
                    scan.Assembly("DataAccess");
                    scan.Assembly("Service");

                });

                x.For<IAppService>().Use<AppService>();
                x.For<ISpService>().Use<SpService>();
                x.For<IRoleService>().Use<Service.Services.RoleService>();
                x.For<IUserService>().Use<UserService>();
                x.For<IPermissionService>().Use<PermissionService>();
                x.For<IRolePermissionService>().Use<RolePermissionService>();
                x.For<IUserLoginService>().Use<UserLoginService>();
                x.For<IReportService>().Use<ReportService>();
                x.For<IReportCatalogueService>().Use<ReportCatalogueService>();
                x.For<IExtensionService>().Use<ExtensionService>();
                x.For<IDivisionService>().Use<DivisionService>();
                x.For<IDepartmentService>().Use<DepartmentService>();
                x.For<ICostCentreService>().Use<CostCentreService>();
                x.For<IExt_Dept_Dev_CCService>().Use<Ext_Dept_Dev_CCService>();
                x.For<ICustomerService>().Use<CustomerService>();
                x.For<ISystemSettingService>().Use<SystemSettingService>();
                x.For<IActionCategoryService>().Use<ActionCategoryService>();
                x.For<IActionService>().Use<ActionService>();

                x.For<ICountryService>().Use<CountryService>();
                x.For<ICompanyService>().Use<CompanyService>();
            });
            return ObjectFactory.Container;
        }
    }
}


