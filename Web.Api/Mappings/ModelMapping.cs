﻿using System;
using System.Web.Mvc;
using AutoMapper;
using DataAccess.BLL;
using Web.Api.Models.User;




namespace TimeBoxe.Web.Mappings
{
    public class ModelMapping : IAutoMap
    {
        public void Initialize()
        {
            Mapper.CreateMap<UserModel, User>();
            Mapper.CreateMap<User, UserModel>()
          .ForMember(b => b.ConfirmPassword, o => o.MapFrom(m => m.Password));

        }
    }
}