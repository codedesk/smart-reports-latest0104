﻿using DAL;
using Models;
using PortaUtility.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
namespace HourlCDR
{
    public partial class Service1 : ServiceBase
    {
        string logsConnectionString = "Data Source=213.229.116.7;Initial Catalog=SMS_Service_Logs;User ID=sa;password=@AccV12v.K2032@";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        bool inProgress = false;
        Timer timer = new Timer();
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try {
                DataAccessLayer.InserCDRServiceLogs(logsConnectionString, "Info", "Hourly CDR Service Started", "");
                //StartFetching();
                timer.Elapsed += new ElapsedEventHandler(ManageHourlyCDR);
            timer.Interval = int.Parse(ConfigurationManager.AppSettings["HourlyCdr_RepeatTime"]) * 1000; //number in milisecinds  
            timer.Enabled = true;
            }catch(Exception ex)
            {
                DataAccessLayer.InserCDRServiceLogs(logsConnectionString, "Error", "Hourly CDR Service Error occured on start " + ex.Message, "");
            }
        }

        public void ManageHourlyCDR(object source, ElapsedEventArgs e)
        {
            try
            {
                StartFetching();
            }catch(Exception ex)
            {
                DataAccessLayer.InserCDRServiceLogs(logsConnectionString, "Error", "Hourly CDR Service Error occured on ManageHourlyCDR " + ex.Message, "");
            }
        }


        public void StartFetching()
        {
            try
            {
                DataAccessLayer.InserCDRServiceLogs(logsConnectionString, "Info", "Inside start fetching function, Inprogress status is " + inProgress, "");
                if (!inProgress)
                {
                    inProgress = true;
                    DataAccessLayer.InserCDRServiceLogs(logsConnectionString, "Info", "Inside hourly cdr service", "");
                    DataSet ds = new DataSet();

                    string path = AppDomain.CurrentDomain.BaseDirectory;

                    string filepath = path + "/ServiceSettign.xml";

                    if (File.Exists(filepath))
                    {
                        ds.ReadXml(filepath);
                    }
                    else
                    {
                        log.Error("Service settigns not available.");

                        return;
                    }


                    if (ds != null && ds.Tables.Count > 0)
                    {
                        Parallel.ForEach(ds.Tables[0].AsEnumerable(), drow =>
                        {
                            ApiUtility apiUtil = new ApiUtility();

                            ServiceSetting servicesetting = new ServiceSetting();

                            servicesetting.ConnString = drow["ConnectionString"].ToString();
                            servicesetting.LicenseKey = drow["LicenseKey"].ToString();

                            ServiceUtility serviceutil = new ServiceUtility();

                            //serviceutil.StartService(servicesetting);
                            apiUtil.GetHourlyCDRS_Old_From_Live(null, servicesetting).Wait();


                        });

                    }
                    DataAccessLayer.InserCDRServiceLogs(logsConnectionString, "Info", "Hourly CDR Service Completed", "");
                    inProgress = false;
                }

            }
            catch (Exception ex)
            {
                inProgress = false;
                DataAccessLayer.InserCDRServiceLogs(logsConnectionString, "Error", "Error occured in syncing hourly cdr services " + ex.Message, "");
                throw ex;
            }
        }

        protected override void OnStop()
        {
        }
    }
}
