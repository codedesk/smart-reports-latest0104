﻿
namespace HourlCDR
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HourlyCdrProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.HourlCDRInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // HourlyCdrProcessInstaller
            // 
            this.HourlyCdrProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.HourlyCdrProcessInstaller.Password = null;
            this.HourlyCdrProcessInstaller.Username = null;
            // 
            // HourlCDRInstaller
            // 
            this.HourlCDRInstaller.ServiceName = "HourlCDR";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.HourlyCdrProcessInstaller,
            this.HourlCDRInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller HourlyCdrProcessInstaller;
        private System.ServiceProcess.ServiceInstaller HourlCDRInstaller;
    }
}