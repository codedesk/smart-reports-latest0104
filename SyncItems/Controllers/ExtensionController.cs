﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SyncItems.Repositories;

namespace SyncItems.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExtensionController : ControllerBase
    {
        private readonly ExtensionRepo _extensionRepo;
        public ExtensionController(ExtensionRepo extensionRepo)
        {
            _extensionRepo=extensionRepo;
        }
        public IActionResult GetExtensions()
        {
            _extensionRepo.GetExtensions(null);
            return Ok();
        }
    }
}
