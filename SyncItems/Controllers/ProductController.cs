﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SyncItems.Repositories;

namespace SyncItems.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductRepo _productRepo;
        public ProductController(ProductRepo productRepo)
        {
            _productRepo=productRepo;
        }
        public IActionResult GetProducts()
        {
            _productRepo.GetProducts(null);
            return Ok();
        }
    }
}
