﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SyncItems.Common;
using SyncItems.Repositories;

namespace SyncItems.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AccountRepo _accountRepo;
        public AccountController(AccountRepo accountRepo)
        {
            _accountRepo=accountRepo;
        }
        public IActionResult GetAccounts()
        {
            _accountRepo.GetAccount(null);
            return Ok();
        }
    }
}
