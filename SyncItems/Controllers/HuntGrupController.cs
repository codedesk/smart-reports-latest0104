﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SyncItems.Repositories;

namespace SyncItems.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HuntGrupController : ControllerBase
    {
        private readonly HuntGrupRepo _huntGrupRepo;
        public HuntGrupController(HuntGrupRepo huntGrupRepo)
        {
            _huntGrupRepo=huntGrupRepo;
        }
        public IActionResult GetHuntGrup()
        {
            _huntGrupRepo.GetHuntGrup(null);
            return Ok();
        }
    }
}
