﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SyncItems.Common
{
    public class ServiceSettings
    {
        public string LicenseKey { get; set; }
        public string BaseUrl { get; set; }
        public string ConnString { get; set; }
        public static ServiceSettings GetValudeFromConfig()
        {
            ServiceSettings serviceSettings = new ServiceSettings();
            var configurationBuilder = new ConfigurationBuilder();
            string path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            serviceSettings.ConnString = configurationBuilder.Build().GetSection("ConnectionStrings:DefaultConnection").Value;
            serviceSettings.BaseUrl = configurationBuilder.Build().GetSection("APISettings:baseURL").Value;
            serviceSettings.LicenseKey = configurationBuilder.Build().GetSection("APISettings:LicenseKey").Value;
            return serviceSettings;
        }
        
        public static string ObjectToXMLGeneric<T>(T filter)
        {

            string xml = null;
            using (StringWriter sw = new StringWriter())
            {
                var xnameSpace = new XmlSerializerNamespaces();
                xnameSpace.Add("", "");
                XmlSerializer xs = new XmlSerializer(typeof(T));
                var settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.OmitXmlDeclaration = true;
                using (var writer = XmlWriter.Create(sw, settings))
                {
                    xs.Serialize(writer, filter, xnameSpace);
                }
                try
                {
                    xml = sw.ToString();

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return xml;
        }
    }
}
