﻿using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SyncItems.Common;
using SyncItems.Models;

namespace SyncItems.Repositories
{
    public class ExtensionRepo
    {
        public void GetExtensions(int? ICustomer)
        {
            CustomerRepo customerRepo = new CustomerRepo();
            ServiceSettings ss = ServiceSettings.GetValudeFromConfig();

            List<CustomersShortInfo> SysCustomers = new List<CustomersShortInfo>();
            SysCustomers.Add(new CustomersShortInfo { ICustomer = ICustomer ?? 0 });

            if (ICustomer == null)
            {
                SysCustomers = customerRepo.GetCustomer(ss.ConnString);
            }

            List<ExtensionLst> result = new List<ExtensionLst>();
            int loopCount = 0;
            foreach (var customer in SysCustomers)
            {
                var options = new RestClientOptions(ss.BaseUrl)
                {
                    MaxTimeout = -1,
                };
                var client = new RestClient(options);
                var request = new RestRequest("/GetCustomerExtensions/"+ss.LicenseKey+"/"+customer.ICustomer+"?format=json", Method.Get);
                RestResponse extnResponse = client.Execute(request);
                Console.WriteLine(extnResponse.Content);
                if (extnResponse.StatusCode == HttpStatusCode.OK)
                {
                    Extension extension = JsonConvert.DeserializeObject<Extension>(extnResponse.Content);
                    if (extension != null && extension.ExtensionLst.Count>0)
                    {
                        result = extension.ExtensionLst.ToList();
                        int jobstatus = 0;
                        InsertExtension(result, customer.ICustomer, ref jobstatus, ss.ConnString);
                    }
                    if (loopCount != 0 && loopCount % 5 == 0)
                    {
                        System.Threading.Thread.Sleep(5000);
                    }
                    loopCount++;
                }
            }

            
        }
        public string InsertExtension(List<ExtensionLst> lst, int ICustomer, ref int jobStatus, string ConnString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "InsertExtension";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ServiceSettings.ObjectToXMLGeneric<List<ExtensionLst>>(lst));
                    cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;

            }
            jobStatus = 1;
            return "Successfully Insterted";


        }
    }
}
