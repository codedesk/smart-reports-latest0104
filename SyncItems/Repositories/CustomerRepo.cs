﻿using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SyncItems.Common;
using SyncItems.Models;

namespace SyncItems.Repositories
{
    public class CustomerRepo
    {
        public void GetCustomers()
        {
            ServiceSettings ss = ServiceSettings.GetValudeFromConfig();
            var options = new RestClientOptions(ss.BaseUrl)
            {
                MaxTimeout = -1,
            };
            var client = new RestClient(options);
            var request = new RestRequest("/getcustomersshortinfowithnolimit/"+ss.LicenseKey+"/true?format=json", Method.Get);
            RestResponse custResponse = client.Execute(request);
            Console.WriteLine(custResponse.Content);

            List<CustomersShortInfo> result = new List<CustomersShortInfo>();
            if (custResponse.StatusCode == HttpStatusCode.OK)
            {
                Customer customer = JsonConvert.DeserializeObject<Customer>(custResponse.Content);
                if (customer != null && customer.CustomersShortInfo != null)
                {
                    result = customer.CustomersShortInfo.ToList();

                    int jobstatus = 0;
                    InsertCustomer(result, ss.ConnString, ref jobstatus);

                }
            }


        }
        public string InsertCustomer(List<CustomersShortInfo> lst, string connString, ref int jobStatus)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "InsertCustomer";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ServiceSettings.ObjectToXMLGeneric<List<CustomersShortInfo>>(lst));
                    cmd.Connection = new SqlConnection(connString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;

            }
            jobStatus = 1;
            return "Successfully Insterted";


        }
        public List<CustomersShortInfo> GetCustomer(string ConnString)
        {
            List<CustomersShortInfo> LstCus = new List<CustomersShortInfo>();
            DataTable DT = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("GetCustomer", ConnString);
            DA.SelectCommand.CommandType = CommandType.StoredProcedure;
            DA.Fill(DT);

            foreach (DataRow r in DT.Rows)
            {
                LstCus.Add(new CustomersShortInfo { ICustomer = Convert.ToInt32(r["ICustomer"].ToString()) });
            }

            return LstCus;

        }
    }
}
