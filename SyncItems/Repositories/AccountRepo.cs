﻿using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SyncItems.Common;
using SyncItems.Models;

namespace SyncItems.Repositories
{
    public class AccountRepo
    {
        public void GetAccount(int? ICustomer)
        {
            CustomerRepo customerRepo = new CustomerRepo();
            ServiceSettings ss = ServiceSettings.GetValudeFromConfig();

            List<CustomersShortInfo> SysCustomers = new List<CustomersShortInfo>();
            SysCustomers.Add(new CustomersShortInfo { ICustomer = ICustomer ?? 0 });
            if (ICustomer == null) { SysCustomers = customerRepo.GetCustomer(ss.ConnString); }

            string JobStatusMsg = "";
            int jobStatus = 1;
            int loopCount = 0;
            foreach (var customer in SysCustomers)
            {
                try
                {
                    var options = new RestClientOptions(ss.BaseUrl)
                    {
                        MaxTimeout = -1,
                    };
                    var client = new RestClient(options);
                    var request = new RestRequest("/GetAccountByICustomer/"+ss.LicenseKey+"/"+customer.ICustomer+"?format=json", Method.Get);
                    RestResponse accountResponse = client.Execute(request);

                    if (accountResponse.StatusCode == HttpStatusCode.OK)
                    {
                        CustomerAccounts customerAccounts = JsonConvert.DeserializeObject<CustomerAccounts>(accountResponse.Content);
                        if (customerAccounts != null && customerAccounts.Accounts.Count>0)
                        {
                            JobStatusMsg = InsertAccounts(customerAccounts.Accounts, customer.ICustomer, ref jobStatus, ss.ConnString);
                        }
                    }
                }
                catch (Exception ex)
                {
                    jobStatus = 0;
                }
                if (loopCount != 0 && loopCount % 5 == 0)
                {
                    System.Threading.Thread.Sleep(5000);
                }
                loopCount++;
            }

        }

        public string InsertAccounts(List<Account> lst, int ICustomer, ref int jobStatus, string ConnString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    //var x = cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<Account>>(lst));
                    cmd.CommandText = "InsertAccount";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ServiceSettings.ObjectToXMLGeneric<List<Account>>(lst));
                    cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                throw ex;
            }
            jobStatus = 1;
            return "Successfully Insterted";


        }
    }
}
