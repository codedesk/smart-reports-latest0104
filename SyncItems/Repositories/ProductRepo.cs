﻿using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SyncItems.Common;
using SyncItems.Models;

namespace SyncItems.Repositories
{
    public class ProductRepo
    {
        public void GetProducts(int? ICustomer)
        {
            CustomerRepo customerRepo = new CustomerRepo();
            ServiceSettings ss = ServiceSettings.GetValudeFromConfig();

            List<CustomersShortInfo> SysCustomers = new List<CustomersShortInfo>();
            SysCustomers.Add(new CustomersShortInfo { ICustomer = ICustomer ?? 0 });

            if (ICustomer == null)
            {
                SysCustomers = customerRepo.GetCustomer(ss.ConnString);
            }
            List<HGInfoLst> result = new List<HGInfoLst>();
            int loopCount = 0;
            foreach (var customer in SysCustomers)
            {
                var options = new RestClientOptions(ss.BaseUrl)
                {
                    MaxTimeout = -1,
                };
                var client = new RestClient(options);
                var request = new RestRequest("/GetCustomerProducts/"+ss.LicenseKey+"/"+customer.ICustomer+"?format=json", Method.Get);
                RestResponse prodResponse = client.Execute(request);
                if (prodResponse.StatusCode == HttpStatusCode.OK)
                {
                    Products products = JsonConvert.DeserializeObject<Products>(prodResponse.Content);
                    if (products != null && products.PortaProducts.Count>0)
                    {
                        int jobstatus = 0;
                        InsertProducts(products.PortaProducts, customer.ICustomer, ref jobstatus, ss.ConnString);
                    }
                    if (loopCount != 0 && loopCount % 5 == 0)
                    {
                        System.Threading.Thread.Sleep(5000);
                    }
                    loopCount++;
                }

            }
                
        }
        public string InsertProducts(List<Product> lst, int ICustomer, ref int jobStatus, string ConnString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "InsertProduct";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData",ServiceSettings.ObjectToXMLGeneric<List<Product>>(lst));
                    cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;
            }
            jobStatus = 1;
            return "Successfully Insterted";


        }
    }
}
