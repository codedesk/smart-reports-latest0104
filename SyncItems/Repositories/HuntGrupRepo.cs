﻿using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SyncItems.Common;
using SyncItems.Models;

namespace SyncItems.Repositories
{
    public class HuntGrupRepo
    {
        public void GetHuntGrup(int? ICustomer)
        {
            CustomerRepo customerRepo = new CustomerRepo();
            ServiceSettings ss = ServiceSettings.GetValudeFromConfig();

            List<CustomersShortInfo> SysCustomers = new List<CustomersShortInfo>();
            SysCustomers.Add(new CustomersShortInfo { ICustomer = ICustomer ?? 0 });

            if (ICustomer == null)
            {
                SysCustomers = customerRepo.GetCustomer(ss.ConnString);
            }
            List<HGInfoLst> result = new List<HGInfoLst>();
            int loopCount = 0;
            foreach (var customer in SysCustomers)
            {
                var options = new RestClientOptions(ss.BaseUrl)
                {
                    MaxTimeout = -1,
                };
                var client = new RestClient(options);
                var request = new RestRequest("/GetCustomerHuntGroups/"+ss.LicenseKey+"/"+customer.ICustomer+"?format=json", Method.Get);
                RestResponse HGResponse = client.Execute(request);

                if (HGResponse.StatusCode == HttpStatusCode.OK)
                {
                    HuntGroup huntGroup= JsonConvert.DeserializeObject<HuntGroup>(HGResponse.Content);
                    if (huntGroup != null && huntGroup.HGInfoLst.Count>0)
                    {
                        result = huntGroup.HGInfoLst.ToList();
                        int jobstatus = 0;
                        InsertHuntGroup(result, customer.ICustomer, ref jobstatus, ss.ConnString);
                    }
                    if (loopCount != 0 && loopCount % 5 == 0)
                    {
                        System.Threading.Thread.Sleep(5000);
                    }
                    loopCount++;
                }
            }

        }

        public string InsertHuntGroup(List<HGInfoLst> lst, int ICustomer, ref int jobStatus, string ConnString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "InsertHuntGroup";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ServiceSettings.ObjectToXMLGeneric<List<HGInfoLst>>(lst));
                    cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;

            }
            jobStatus = 1;
            return "Successfully Insterted";


        }
    }
}
