﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncItems.Models
{
    public class AccountModel
    {
    }
    public class CustomerAccounts
    {
        public List<Account> Accounts { get; set; }
    }
    public class Account
    {
        public string Id { get; set; }
        public int IAccount { get; set; }
        public int ICustomer { get; set; }
        public string BillingModel { get; set; }
        public string ISO4217 { get; set; }
        public double Balance { get; set; }
        public string UILogin { get; set; }
        public string UIPassword { get; set; }
        public string VoipPassword { get; set; }
        public bool Blocked { get; set; }
        public bool UMEnabled { get; set; }
        public string Email { get; set; }
        public string Phone1 { get; set; }
        public string Address { get; set; }
        public string CompanyName { get; set; }
        public string BillStatus { get; set; }
        public string FollowMeEnabled { get; set; }
        public int IRoutingPlan { get; set; }
        public int IProduct { get; set; }
        public string Note { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Country { get; set; }
        public string Phone2 { get; set; }
        public int IEnv { get; set; }
        public string Salutation { get; set; }
        public int ISubscriber { get; set; }

    }
}
