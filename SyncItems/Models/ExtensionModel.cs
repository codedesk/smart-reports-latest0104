﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SyncItems.Models
{
    public class ExtensionModel
    {
    }
    public class ExtensionLst
    {
        public int ExtensionID { get; set; }
        public int i_c_ext { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public int i_account { get; set; }
        public string account_id { get; set; }
    }
    public class Extension
    {
        public List<object> lstportaActiveCalls { get; set; }
        public List<ExtensionLst> ExtensionLst { get; set; }
        public bool ExtensionDeleted { get; set; }
    }
}
