﻿
namespace CustomerItemsSync
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customeritemsserviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.customeritemsserviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // customeritemsserviceProcessInstaller1
            // 
            this.customeritemsserviceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.customeritemsserviceProcessInstaller1.Password = null;
            this.customeritemsserviceProcessInstaller1.Username = null;
            // 
            // customeritemsserviceInstaller1
            // 
            this.customeritemsserviceInstaller1.ServiceName = "customeritemssync";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.customeritemsserviceProcessInstaller1,
            this.customeritemsserviceInstaller1});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller customeritemsserviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller customeritemsserviceInstaller1;
    }
}