﻿using Models;
using PortaUtility.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Timers;
using DAL;

namespace CustomerItemsSync
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        string logsConnectionString = "Data Source=213.229.116.7;Initial Catalog=SMS_Service_Logs;User ID=sa;password=@AccV12v.K2032@";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        int repeatTime = 30;
      
        Timer timer3 = new Timer();
        Timer RestartTimers = new Timer();

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {

                DataAccessLayer.InserServiceLogs(logsConnectionString, "Info", "Service Started", "");
                timer3.Elapsed += new ElapsedEventHandler(WorkFunction3);
                timer3.Interval = int.Parse(ConfigurationManager.AppSettings["Others_RepeatTime"]) * 1000; //number in milisecinds  
                timer3.Enabled = true;
                EventLog.WriteEntry("Smartreports Service Finished for syncing customer items");
            }
            catch (Exception ex)
            {
                DataAccessLayer.InserServiceLogs(logsConnectionString, "Error", ex.Message, "");
                EventLog.WriteEntry("Error in service " + ex.Message);
                //DebugLog("Unable To Start Service - " + ex.Message.ToString());
                log.Info("Unable To Start Service - " + ex.Message.ToString());
            }
        }

        protected override void OnStop()
        {
            DataAccessLayer.InserServiceLogs(logsConnectionString, "Info", "Service stopped", "");
            log.Info("Service Stopped for syncing customer items ");
            //DebugLog("Service Stopped");
        }


        public void WorkFunction3(object source, ElapsedEventArgs e)
        {
            try
            {
                DataAccessLayer.InserServiceLogs(logsConnectionString, "Info", "Inside Sync Other items.", "");
                DataSet ds = new DataSet();

                string path = AppDomain.CurrentDomain.BaseDirectory;

                string filepath = path + "/ServiceSettign.xml";

                if (File.Exists(filepath))
                {
                    ds.ReadXml(filepath);
                }
                else
                {
                    log.Error("Service settigns not available.");

                    return;
                }


                if (ds != null && ds.Tables.Count > 0)
                {
                    Parallel.ForEach(ds.Tables[0].AsEnumerable(), drow =>
                    {
                        ApiUtility apiUtil = new ApiUtility();

                        ServiceSetting servicesetting = new ServiceSetting();

                        servicesetting.ConnString = drow["ConnectionString"].ToString();
                        servicesetting.LicenseKey = drow["LicenseKey"].ToString();

                        ServiceUtility serviceutil = new ServiceUtility();

                        //serviceutil.StartService(servicesetting);
                        apiUtil.CustomersGetSync(servicesetting).Wait();

                        apiUtil.GetAccountSync(null, servicesetting).Wait();

                        apiUtil.GetProductSync(null, servicesetting).Wait();

                        apiUtil.GetExtensionSync(null, servicesetting).Wait();

                        apiUtil.GetHuntGrupSync(null, servicesetting).Wait();

                    });

                }
            }
            catch (Exception ex)
            {
                DataAccessLayer.InserServiceLogs(logsConnectionString, "Error", "Error occured in syncing other services " + ex.Message, "");

                //DebugLog(ex.Message.ToString());
            }
        }


    }
}
