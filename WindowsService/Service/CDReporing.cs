﻿using Models;
using PortaUtility.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;


namespace ReportingSystem
{
    public partial class CDReporing : ServiceBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public CDReporing()
        {
            InitializeComponent();
        }
        
       protected override void OnStart(string[] args)
        {
            try
            {
                EventLog.WriteEntry("Smartreports Service Starting");
                log.Info("Smartreports Service Starting");
                StartService();
                log.Info("Smartreports Service Started");
                EventLog.WriteEntry("Smartreports Service Started");

            }
            catch (Exception ex)
            {
                try
                {
                    log.Error("Smartreports Service Error " + ex.Message);
                    EventLog.WriteEntry("Smartreports Service Error " + ex.Message);

                }
                catch (Exception)
                {
                }
               
            }
        
        }

       protected override void OnStop()
        {
            try
            {
                EventLog.WriteEntry("Stopping");
                log.Info("Stopping");

                log.Info("Stopped");
                EventLog.WriteEntry("Service Stopped");
            }
            catch (System.Exception)
            {
                
            }
           

        }


       public void StartService()
       {
           log.Info("Service Started");
           try
           {

               //ServiceSetting service_setting = new ServiceSetting();

               //service_setting.ConnString = "Data Source=CodeDEsk;Initial Catalog=SmartReport;User ID=sa;password=!Qazwerty";
               //service_setting.LicenseKey = "f7d60373-8120-4685-a8cd-468fa4a5fb7b";

               //ServiceUtility serviceutility = new ServiceUtility();

               //serviceutility.StartService(service_setting);


               DataSet ds = new DataSet();

               string path = AppDomain.CurrentDomain.BaseDirectory;

               string filepath = path + "/ServiceSettign.xml";

               if (File.Exists(filepath))
               {
                   ds.ReadXml(filepath);
               }
               else
               {
                   log.Error("Service settigns not available.");
                   return;
               }


               if (ds != null && ds.Tables.Count > 0)
               {
                   Parallel.ForEach(ds.Tables[0].AsEnumerable(), drow =>
                   {
                       ApiUtility apiUtil = new ApiUtility();

                       ServiceSetting servicesetting = new ServiceSetting();

                       servicesetting.ConnString = drow["ConnectionString"].ToString();
                       servicesetting.LicenseKey = drow["LicenseKey"].ToString();

                       ServiceUtility serviceutil = new ServiceUtility();

                       serviceutil.StartService(servicesetting);

                   });

               }

           }
           catch (System.Exception ex)
           {
               log.Error(ex.Message);
           }
       }




    }

  
}
