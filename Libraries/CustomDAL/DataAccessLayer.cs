﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DAL
{
    public class DataAccessLayer
    {


        string myConnection = System.Configuration.ConfigurationManager.ConnectionStrings["Default"].ToString();


        public List<TimerSettingModel> GetTimerSettings(string sqlconn)
        {
            DataTable DT = new DataTable();
            List<TimerSettingModel> lst = new List<TimerSettingModel>();
            try
            {
                var query = "select * from TimerSetting";
                using (SqlDataAdapter da = new SqlDataAdapter(query, sqlconn))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        foreach (DataRow dr in DT.Rows)
                        {
                            lst.Add(new TimerSettingModel
                            {
                                Id = Convert.ToInt32(dr["Id"]),
                                TimerName = dr["TimerName"].ToString(),
                                DueTime = Convert.ToInt32(dr["DueTime"].ToString()),
                                Period = Convert.ToInt32(dr["Period"].ToString()),
                                IsActive = Convert.ToBoolean(dr["IsActive"].ToString()),
                            });

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public TimerSettingModel GetTimerSetting(int id, string sqlConn)
        {
            DataTable DT = new DataTable();
            List<TimerSettingModel> lst = new List<TimerSettingModel>();
            try
            {
                var query = string.Format("select top 1 * from TimerSetting where id = {0}", id);
                using (SqlDataAdapter da = new SqlDataAdapter(query, sqlConn))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        foreach (DataRow dr in DT.Rows)
                        {
                            lst.Add(new TimerSettingModel
                            {
                                Id = Convert.ToInt32(dr["Id"]),
                                TimerName = dr["TimerName"].ToString(),
                                DueTime = Convert.ToInt32(dr["DueTime"].ToString()),
                                Period = Convert.ToInt32(dr["Period"].ToString()),
                                IsActive = Convert.ToBoolean(dr["IsActive"].ToString()),
                            });
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst.FirstOrDefault();
        }

        public void InserActiveCalls(string sqlConn, ActiveCallReseller activeCallReseller)
        {
            string qry = "Delete from  ActiveCallsSync;  ";
            if (activeCallReseller.lstActiveCalls.Count > 0) {
                
                
                foreach(var item in activeCallReseller.lstActiveCalls) {
                    qry = qry + "Insert into ActiveCallsSync (IEnv,CLI, CLD,AccountID, Connect_time, Disconnect_time, ICustomer, IAccount, noofcalls, DurationMins, CallType, VendorId, IConnection, IDialRuleCld) " +
                   "Values (" + item.IEnv + ", '" + item.CLI + "', '" + item.CLD + "','" + item.AccountID + "','" + item.Connect_time + "','" + item.Disconnect_time + "'," + item.ICustomer + "," + item.IAccount + "," + item.noofcalls + ",'" + item.DurationMins + "','" + item.CallType + "'," + "0" + "," + "0" + "," + "0);  ";
                }

            
          
            }

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = qry;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = new SqlConnection(sqlConn);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();


            }
        }

        public static void InserServiceLogs(string sqlConn, string Type, string Message, string License)
        {

            string qry = "INSERT INTO [dbo].[Logs]" +
                        "([Type]" +
                        ",[Message]" +
                        ",[ServerDateTime]" +
                        ",[Date]" +
                        ",[LicenseKey])" +
                        "VALUES" +
                        "('" + Type + "'," +
                        "'" + Message + "'," +
                        "'" + DateTime.Now + "'," +
                        "GetDate()," +
                         "'" + License + "');";
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = qry;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = new SqlConnection(sqlConn);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
        }


        public static void InserCDRServiceLogs(string sqlConn, string Type, string Message, string License)
        {
            try { 
            string qry = "INSERT INTO [dbo].[CDRLogs]" +
                        "([Type]" +
                        ",[Message]" +
                        ",[ServerDateTime]" +
                        ",[Date]" +
                        ",[LicenseKey])" +
                        "VALUES" +
                        "('" + Type + "'," +
                        "'" + Message + "'," +
                        "'" + DateTime.Now + "'," +
                        "GetDate()," +
                         "'" + License + "');";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = qry;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = new SqlConnection(sqlConn);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();


            }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public string InsertCDR(List<Lstaccountcdr> lst, ref int jobStatus, String ConnString)
        // public string InsertCDR(List<hourlycdr> lst, int ICustomer, ref int jobStatus, String ConnString)
        {
            try
            {
                var xml = ObjectToXMLGeneric<List<Lstaccountcdr>>(lst);
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandText = "InsertCdr";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<Lstaccountcdr>>(lst));
                    //cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<hourlycdr>>(lst));
                    //cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;
            }
            jobStatus = 1;
            return "Successfully Insterted";


        }



        public string InsertCDR_Old(List<hourlycdr> lst, int ICustomer, ref int jobStatus, String ConnString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandText = "InsertCdr_old";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<hourlycdr>>(lst));
                    cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;
            }
            jobStatus = 1;
            return "Successfully Insterted";


        }
        public List<hourlycdr> GetCDR(int ICustomer, string ConnString, string Datetime)
        {
            List<hourlycdr> lstCDR = new List<hourlycdr>();


            DataTable DT = new DataTable();
            try
            {
                string query = "select * from hourlycdr where ICustomer = " + ICustomer + " AND Connect_time >='" + Datetime + "'  AND IsProcessed = 0";
                //string query = "select * from hourlycdr where ICustomer = "+ ICustomer +" AND Connect_time >= DateAdd(Month, -1, GETDATE()) AND IsProcessed = 0";

                using (SqlDataAdapter da = new SqlDataAdapter(query, ConnString))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        lstCDR = (List<hourlycdr>)DT.ToList<hourlycdr>();

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstCDR;
        }
        public List<hourlycdr> GetCDR_Old_Live(int ICustomer, string ConnString)
        {
            List<hourlycdr> lstCDR = new List<hourlycdr>();
            DataTable DT = new DataTable();
            try
            {
                string query = string.Concat("select * from hourlycdr where ICustomer = ", ICustomer, " AND Connect_time >= DateAdd(Month, -1, GETDATE()) AND IsProcessed = 0");
                using (SqlDataAdapter da = new SqlDataAdapter(query, ConnString))
                {
                    da.Fill(DT);
                    if (DT != null)
                    {
                        lstCDR = (List<hourlycdr>)DT.ToList<hourlycdr>();
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return lstCDR;
        }
        public List<hourlycdr> GetCDR_Old(int ICustomer, string ConnString)
        {
            List<hourlycdr> lstCDR = new List<hourlycdr>();


            DataTable DT = new DataTable();
            try
            {
                string query = "select * from hourlycdr where ICustomer = " + ICustomer + " AND Connect_time >= DateAdd(Month, -1, GETDATE()) AND IsProcessed = 0";

                using (SqlDataAdapter da = new SqlDataAdapter(query, ConnString))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        lstCDR = (List<hourlycdr>)DT.ToList<hourlycdr>();

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstCDR;
        }
        public DateTime getjobstartdate(string ConnString)
        {
            DateTime cronejobDatetime = new DateTime();

            DataTable DT = new DataTable();
            try
            {
                string query = "select * from cronjob_command";
                //string query = "select * from hourlycdr where ICustomer = "+ ICustomer +" AND Connect_time >= DateAdd(Month, -1, GETDATE()) AND IsProcessed = 0";

                SqlConnection conn = new SqlConnection(ConnString);
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                DT.Load(reader);
                foreach (DataRow row in DT.Rows)
                {
                    string start = row["CroneJobEndTime"].ToString();
                    cronejobDatetime = Convert.ToDateTime(row["CroneJobEndTime"].ToString());
                     
                   
                }
                reader.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return cronejobDatetime;
        }

        public SystemSetting GetSystemSetting(int ICustomer, String ConnString)
        {
            SystemSetting systemsettings = new SystemSetting();


            DataTable DT = new DataTable();
            try
            {
                string query = "select Top 1 Id, ICustomer, AnswerTime, LocalCode, MaxCallDuration from SystemSetting WHERE ICustomer = " + ICustomer;

                using (SqlDataAdapter da = new SqlDataAdapter(query, ConnString))
                {
                    da.Fill(DT);

                    if (DT != null && DT.Rows.Count > 0)
                    {
                        systemsettings.AnswerTime = (int)DT.Rows[0]["AnswerTime"];
                        systemsettings.LocalCode = (int)DT.Rows[0]["LocalCode"];
                        systemsettings.MaxCallDuration = (int)DT.Rows[0]["MaxCallDuration"];

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return systemsettings;
        }

        public List<Account> GetAccount(int ICustomer, String ConnString)
        {
            List<Account> lstAccount = new List<Account>();


            DataTable DT = new DataTable();
            try
            {
                string query = "select * from account where ICustomer =" + ICustomer;

                using (SqlDataAdapter da = new SqlDataAdapter(query, ConnString))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        lstAccount = (List<Account>)DT.ToList<Account>();

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstAccount;
        }

        public List<Product> GetProduct(int ICustomer, String ConnString)
        {
            List<Product> lstProduct = new List<Product>();


            DataTable DT = new DataTable();
            try
            {
                string query = "select * from product where ICustomer =" + ICustomer;

                using (SqlDataAdapter da = new SqlDataAdapter(query, ConnString))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        lstProduct = (List<Product>)DT.ToList<Product>();

                    }
                }
            }
            catch (Exception ex)
            {
               
            }

            return lstProduct;
        }
        public List<DefinedProduct> GetProductByType(int ICustomer, int TypeId, String ConnString)
        {
            List<DefinedProduct> lstDefinedProduct = new List<DefinedProduct>();


            DataTable DT = new DataTable();
            try
            {
                string query = "select IProduct from Product_ProductType pt inner join product p on p.ID = pt.ProductId where p.ICustomer = " + ICustomer + " and pt.ProductTypeId = " + TypeId;

                using (SqlDataAdapter da = new SqlDataAdapter(query, ConnString))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        lstDefinedProduct = (List<DefinedProduct>)DT.ToList<DefinedProduct>();

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstDefinedProduct;
        }

        public List<LstActiveCall> GetActiveCallsDb(int ICustomer, string ConnString)
        {
            List<LstActiveCall> lstActiveCalls = new List<LstActiveCall>();
            DataTable DT = new DataTable();
            try
            {
                string query = "select * from ActiveCallsSync where ICustomer =" + ICustomer;
                //string query = "select * from ActiveCallsSync ";

                using (SqlDataAdapter da = new SqlDataAdapter(query, ConnString))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        lstActiveCalls = (List<LstActiveCall>)DT.ToList<LstActiveCall>();

                    }
                }
            }
            catch (Exception ex)
            {

            }

            return lstActiveCalls;
        }
        public List<ExtensionLst> GetExtensions(int ICustomer, String ConnString)
        {
            List<ExtensionLst> lstExtension = new List<ExtensionLst>();


            DataTable DT = new DataTable();
            try
            {
                string query = "select * from extension where ICustomer =" + ICustomer;

                using (SqlDataAdapter da = new SqlDataAdapter(query, ConnString))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        lstExtension = (List<ExtensionLst>)DT.ToList<ExtensionLst>();

                    }
                }
            }
            catch (Exception ex)
            {

            }

            return lstExtension;
        }
        public string InsertCall(Call call, string ConnString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "prc_InsCall";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    string xml = ObjectToXMLGeneric<Call>(call);
                    cmd.Parameters.AddWithValue("@XMLData", xml);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Successfully Inserted";


        }

        public string InsertCustomer(List<CustomersShortInfo> lst, string connString, ref int jobStatus)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "InsertCustomer";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<CustomersShortInfo>>(lst));
                    cmd.Connection = new SqlConnection(connString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;

            }
            jobStatus = 1;
            return "Successfully Insterted";


        }

        public string InsertExtension(List<ExtensionLst> lst, int ICustomer, ref int jobStatus, string ConnString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "InsertExtension";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<ExtensionLst>>(lst));
                    cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;

            }
            jobStatus = 1;
            return "Successfully Insterted";


        }

        public string InsertHuntGroup(List<HGInfoLst> lst, int ICustomer, ref int jobStatus, string ConnString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "InsertHuntGroup";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<HGInfoLst>>(lst));
                    cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;

            }
            jobStatus = 1;
            return "Successfully Insterted";


        }
        public List<CustomersShortInfo> GetCustomer(string ConnString)
        {
            List<CustomersShortInfo> LstCus = new List<CustomersShortInfo>();
            DataTable DT = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("GetCustomer", ConnString);
            DA.SelectCommand.CommandType = CommandType.StoredProcedure;
            DA.Fill(DT);

            foreach (DataRow r in DT.Rows)
            {
                LstCus.Add(new CustomersShortInfo { ICustomer = Convert.ToInt32(r["ICustomer"].ToString()) });
            }

            return LstCus;

        }

        public List<Product> GetProductTariff()
        {
            List<Product> listProduct = new List<Product>();
            DataTable DT = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("[prc_getProduct]", myConnection);
            DA.SelectCommand.CommandType = CommandType.StoredProcedure;
            DA.Fill(DT);

            foreach (DataRow r in DT.Rows)
            {
                string iTariff = r["ITariff"].ToString();
                
                    listProduct.Add(new Product { ITariff = Convert.ToInt32(r["ITariff"].ToString()) });
                
            }

            return listProduct;

        }

        public List<Country> GetCountry()
        {
            List<Country> listCountry = new List<Country>();
            DataTable DT = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("[prc_getCountry]", myConnection);
            DA.SelectCommand.CommandType = CommandType.StoredProcedure;
            DA.Fill(DT);

            foreach (DataRow r in DT.Rows)
            {
                listCountry.Add(new Country
                {

                    Id = Convert.ToInt32(r["Id"].ToString()),
                    Title = r["Title"].ToString(),
                    Code = r["Title"].ToString(),
                    Prefix = Convert.ToInt32(r["Prefix"].ToString()),

                });
            }

            return listCountry;

        }
       

        public string InsertHotel(Models.HotelAccount.Hotel hotel, int ICustomer, ref int jobStatus)
        {
            string Name = hotel.Customer.PersonalInformation.Name;
            string Address = hotel.Customer.CustomerAddress.Address1 + hotel.Customer.CustomerAddress.Address2 + hotel.Customer.CustomerAddress.Address3;
            string Phone1 = hotel.Customer.CustomerContact.Phone1;
            string Phone2 = hotel.Customer.CustomerContact.Phone2;
            string Currency = hotel.Customer.CustomerStatus.ISO4217;
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "InsertHotel";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@Address", Address);
                cmd.Parameters.AddWithValue("@Phone1", Phone1);
                cmd.Parameters.AddWithValue("@Phone2", Phone2);
                cmd.Parameters.AddWithValue("@Currency", Currency);
                cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                cmd.Connection = new SqlConnection(myConnection);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
            return ("OK");
        }
        public string InsertAccounts(List<Account> lst, int ICustomer, ref int jobStatus, string ConnString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    //var x = cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<Account>>(lst));
                    cmd.CommandText = "InsertAccount";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<Account>>(lst));
                    cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                throw ex;
            }
            jobStatus = 1;
            return "Successfully Insterted";


        }

        public string InsertProducts(List<Product> lst, int ICustomer, ref int jobStatus, string ConnString)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "InsertProduct";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<Product>>(lst));
                    cmd.Parameters.AddWithValue("@ICustomer", ICustomer);
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;
            }
            jobStatus = 1;
            return "Successfully Insterted";


        }


        public string InsertTariffRate(List<TariffRate> lst, int iTariff, int countryId, ref int jobStatus)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "InsertTariffRate";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@XMLData", ObjectToXMLGeneric<List<TariffRate>>(lst));
                    cmd.Parameters.AddWithValue("@iTariff", iTariff);
                    cmd.Parameters.AddWithValue("@countryId", countryId);
                    cmd.Connection = new SqlConnection(myConnection);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                jobStatus = 0;
                return ex.Message;
            }
            jobStatus = 1;
            return "Successfully Insterted";


        }
        public string UpdatejobLastRunTime( DateTime LastRun, string ConnString)
        {
            try 
            { 
             string qry = "update cronjob_command set CroneJobEndTime='" + LastRun + "'";
             using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = qry;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Successfully Insterted";


        }

        public string UpdateCustomerjob(int ICustomer, int status, int Records, bool Insert, DateTime LastRun, DateTime NextRun, string ConnString)
        {
            try
            {
                string lastRun = LastRun.ToString("yyyy-MM-dd HH:mm:ss");
                string nextRun = NextRun.ToString("yyyy-MM-dd HH:mm:ss");
                string qry = "Insert into customerjob (ICustomer, LastRunTime, NextRunTime, RecordsFetched, Status, ReportTo) " +
                            "Values (" + ICustomer + ", '" + lastRun + "', '" + nextRun + "'," + Records + "," + status + ",'farrukh.khan@code-desk.com')";
                if (!Insert)
                {
                    qry = "Update customerjob Set LastRunTime = '" + lastRun + "', NextRunTime = '" + nextRun + "', RecordsFetched = " + Records + ", Status = " + status + " WHERE ICustomer = " + ICustomer;
                }



                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = qry;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Successfully Insterted";


        }

        public string InsertCustomerjobLog(int ICustomer, string description, string ConnString)
        {
            try
            {
                string qry = "Insert into customerjoblog (ICustomer,JobTime, Description) " +
                            "Values (" + ICustomer + ", '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', '" + description + "' )";


                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = qry;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = new SqlConnection(ConnString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();


                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Successfully Insterted";


        }
     
        public static String ObjectToXMLGeneric<T>(T filter)
        {

            string xml = null;
            using (StringWriter sw = new StringWriter())
            {
                var xnameSpace = new XmlSerializerNamespaces();
                xnameSpace.Add("", "");
                XmlSerializer xs = new XmlSerializer(typeof(T));
                var settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.OmitXmlDeclaration = true;
                using (var writer = XmlWriter.Create(sw, settings))
                {
                    xs.Serialize(writer, filter, xnameSpace);
                }
                try
                {
                    xml = sw.ToString();

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return xml;
        }

        public List<CustomerJob> GetCustomerJob(string ConnString)
        {
            DataTable DT = new DataTable();
            List<CustomerJob> lst = new List<CustomerJob>();
            try
            {
                var query = "select * from CustomerJob";
                using (SqlDataAdapter da = new SqlDataAdapter(query, ConnString))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        foreach (DataRow dr in DT.Rows)
                        {
                            lst.Add(new CustomerJob
                            {
                                Id = Convert.ToInt32(dr["Id"]),
                                ICustomer = Convert.ToInt32(dr["ICustomer"]),
                                LastRunTime = Convert.ToDateTime(dr["LastRunTime"]),
                                NextRunTime = Convert.ToDateTime(dr["NextRunTime"]),
                                ReportTo = dr["ReportTo"].ToString(),
                                Status = Convert.ToInt32(dr["Status"]),
                            });

                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return lst;
        }

        public void UpdateWakeupAlarm(int Id) 
        {
            var qry = "update [WakeUpAlarm] SET [Status] = 'InActive' WHERE Id = " + Id;

            using (SqlConnection conn = new SqlConnection(myConnection)) 
            {
                SqlCommand cmd = new SqlCommand(qry, conn);

                cmd.CommandType = CommandType.Text;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public List<WakeupAlarmModel> GetWakeupAlarms()
        {
            DataTable DT = new DataTable();
            List<WakeupAlarmModel> lst = new List<WakeupAlarmModel>();
            try
            {
                var query = "select a.id as Extension, a.ICustomer as ICustomer, a.IAccount as IAccount  , w.* from [WakeUpAlarm] w inner join account a on w.account_id = a.account_id inner join Reservation r on w.GuestId = r.[GuestID] where w.Status = 'Active' AND  [date] BETWEEN  DATEADD(minute,-30,GETDATE()) AND  DATEADD(minute,5,GETDATE()) and r.InOut = 'In'";
                using (SqlDataAdapter da = new SqlDataAdapter(query, myConnection))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        foreach (DataRow dr in DT.Rows)
                        {
                            lst.Add(new WakeupAlarmModel
                            {
                                Id = Convert.ToInt32(dr["Id"]),
                                Date = Convert.ToDateTime(dr["Date"]),
                                Status = dr["Status"].ToString(),
                                Description = dr["Description"].ToString(),
                                account_id = Convert.ToInt32(dr["account_id"].ToString()),
                                Extension = dr["Extension"].ToString(),
                                IAccount = dr["IAccount"].ToString(),
                                ICustomer = dr["ICustomer"].ToString(),
                            });

                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return lst;
        }

        public List<WakeUpAlarmPhone> AlarmCredentials(string ICustomer, string IAccount)
        {
            DataTable DT = new DataTable();
            List<WakeUpAlarmPhone> lst = new List<WakeUpAlarmPhone>();
            try
            {
                var qry = "select Row_Number() over(order by Id desc) as Id, Id as Number, UILogin as LoginID, VoipPassword as PasswordId from account where IProduct in (select  IProduct from Product where name like 'WA' + '%'   )and ICustomer = '" + ICustomer + "'";
                using (SqlDataAdapter da = new SqlDataAdapter(qry, myConnection))
                {
                    da.Fill(DT);

                    if (DT != null)
                    {
                        foreach (DataRow dr in DT.Rows)
                        {
                            lst.Add(new WakeUpAlarmPhone
                            {
                                Id = Convert.ToInt32(dr["Id"]),
                                Number = dr["Number"].ToString(),
                                UserName = dr["LoginId"].ToString(),
                                Password = dr["PasswordId"].ToString(),
                            });

                        }

                    }
                }
            }
            catch (Exception ex)
            { 
                
            }
            return lst;
            
        }

       



    }

    public static class DataTableExtensions
    {
        public static IList<T> ToList<T>(this DataTable table) where T : new()
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            IList<T> result = new List<T>();

            foreach (var row in table.Rows)
            {
                var item = CreateItemFromRow<T>((DataRow)row, properties);
                result.Add(item);
            }

            return result;
        }

        private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
        {
            T item = new T();
            try
            {
               
                foreach (var property in properties)
                {
                    if (row.Table.Columns.Contains(property.Name) && row[property.Name] != DBNull.Value)
                    {
                        if (property.Name == "IAccount" || property.Name == "Used_Quantity" || property.Name == "i_account")
                        {
                            property.SetValue(item, Convert.ToInt32(row[property.Name]), null);
                        }
                        else if (property.Name == "Balance")
                        {
                            property.SetValue(item, Convert.ToDouble(row[property.Name]), null);
                        }
                        else if (property.Name == "Blocked" || property.Name == "UMEnabled")
                        {
                            property.SetValue(item, Convert.ToBoolean(row[property.Name]), null);
                        }
                        else if (property.Name == "id")
                        {
                            property.SetValue(item, Convert.ToString(row[property.Name]), null);
                        }
                        else if (property.Name == "MaintanenceFee")
                        {
                            property.SetValue(item, Convert.ToInt32(row[property.Name]), null);
                        }
                        else
                        {
                            property.SetValue(item, row[property.Name], null);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
           
            return item;
        }
    }
}
