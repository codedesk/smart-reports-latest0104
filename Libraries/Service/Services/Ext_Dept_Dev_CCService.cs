using System;
using System.Collections.Generic;
using DataAccess.BLL;
using DataAccess.DAL;
using DataAccess.RepositoryContracts;
using Service.Contracts;
using System.Data.SqlClient;
using System.Collections;


namespace Service.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class Ext_Dept_Dev_CCService : IExt_Dept_Dev_CCService
    {
        private readonly IExt_Dept_Dev_CCRepository _roleRepository;
        private readonly IUnitofWork _unitOfWork;

        public Ext_Dept_Dev_CCService(IExt_Dept_Dev_CCRepository appsRepository, IUnitofWork unitOfWork)
        {
            _roleRepository = appsRepository;
            _unitOfWork = unitOfWork;
        }
        #region IExt_Dept_Dev_CCtService Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Ext_Dept_Dev_CC> GetExt_Dept_Dev_CCs()
        {
            return _roleRepository.GetAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Ext_Dept_Dev_CC> GetExt_Dept_Dev_CCs(Func<Ext_Dept_Dev_CC, bool> where)
        {
            if (@where == null) throw new ArgumentException("where");
            return _roleRepository.GetMany(where);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Ext_Dept_Dev_CC GetExt_Dept_Dev_CCById(int id)
        {
            return _roleRepository.GetById(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objExt_Dept_Dev_CC"></param>
        public void InsertExt_Dept_Dev_CC(Ext_Dept_Dev_CC objExt_Dept_Dev_CC)
        {
            _roleRepository.Add(objExt_Dept_Dev_CC);
            _unitOfWork.Commit();
        }


        public void InsertBulkExt_Dept_Dev_CC(IEnumerable<Ext_Dept_Dev_CC> objExt_Dept_Dev_CC)
        {
            foreach (var item in objExt_Dept_Dev_CC)
            {
                _roleRepository.Add(item);
            }
            _unitOfWork.Commit();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="objExt_Dept_Dev_CC"></param>
        public void UpdateExt_Dept_Dev_CC(Ext_Dept_Dev_CC objExt_Dept_Dev_CC)
        {
            _roleRepository.Update(objExt_Dept_Dev_CC);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objExt_Dept_Dev_CC"></param>
        public void DeleteExt_Dept_Dev_CC(Ext_Dept_Dev_CC objExt_Dept_Dev_CC)
        {
            _roleRepository.Delete(objExt_Dept_Dev_CC);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<Ext_Dept_Dev_CC> ExcuteSpType(string query, SqlParameter[] Parameters)
        {
            return _roleRepository.ExcuteSpType(query, Parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable ExcuteSp(string query, SqlParameter[] Parameters)
        {
            return _roleRepository.ExcuteSp(query, Parameters);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<Ext_Dept_Dev_CC> GetBySqlEntity(string query)
        {
            return _roleRepository.GetBySqlEntity(query);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable GetBySql(string query)
        {
            return _roleRepository.GetBySql(query);
        }

        #endregion

    }
}
