using System;
using System.Collections.Generic;
using DataAccess.BLL;
using DataAccess.DAL;
using DataAccess.RepositoryContracts;
using Service.Contracts;
using System.Data.SqlClient;
using System.Collections;


namespace Service.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IUnitofWork _unitOfWork;

        public CustomerService(ICustomerRepository appsRepository, IUnitofWork unitOfWork)
        {
            _customerRepository = appsRepository;
            _unitOfWork = unitOfWork;
        }
        #region ICustomertService Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<customer> GetCustomers()
        {
            return _customerRepository.GetAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<customer> GetCustomers(Func<customer, bool> where)
        {
            if (@where == null) throw new ArgumentException("where");
            return _customerRepository.GetMany(where);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public customer GetCustomerById(int id)
        {
            return _customerRepository.GetById(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objCustomer"></param>
        public void InsertCustomer(customer objCustomer)
        {
            _customerRepository.Add(objCustomer);
            _unitOfWork.Commit();
        }


        public void InsertBulkCustomer(IEnumerable<customer> objCustomer)
        {
            foreach (var item in objCustomer)
            {
                _customerRepository.Add(item);
            }
            _unitOfWork.Commit();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="objCustomer"></param>
        public void UpdateCustomer(customer objCustomer)
        {
            _customerRepository.Update(objCustomer);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objCustomer"></param>
        public void DeleteCustomer(customer objCustomer)
        {
            _customerRepository.Delete(objCustomer);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<customer> ExcuteSpType(string query, SqlParameter[] Parameters)
        {
            return _customerRepository.ExcuteSpType(query, Parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable ExcuteSp(string query, SqlParameter[] Parameters)
        {
            return _customerRepository.ExcuteSp(query, Parameters);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<customer> GetBySqlEntity(string query)
        {
            return _customerRepository.GetBySqlEntity(query);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable GetBySql(string query)
        {
            return _customerRepository.GetBySql(query);
        }

        #endregion

    }
}
