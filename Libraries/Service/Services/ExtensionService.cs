using System;
using System.Collections.Generic;
using DataAccess.BLL;
using DataAccess.DAL;
using DataAccess.RepositoryContracts;
using Service.Contracts;
using System.Data.SqlClient;
using System.Collections;


namespace Service.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class ExtensionService : IExtensionService
    {
        private readonly IExtensionRepository _extensionRepository;
        private readonly IUnitofWork _unitOfWork;

        public ExtensionService(IExtensionRepository appsRepository, IUnitofWork unitOfWork)
        {
            _extensionRepository = appsRepository;
            _unitOfWork = unitOfWork;
        }
        #region IextensiontService Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<extension> Getextensions()
        {
            return _extensionRepository.GetAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<extension> Getextensions(Func<extension, bool> where)
        {
            if (@where == null) throw new ArgumentException("where");
            return _extensionRepository.GetMany(where);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public extension GetextensionById(int id)
        {
            return _extensionRepository.GetById(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objextension"></param>
        public void Insertextension(extension objextension)
        {
            _extensionRepository.Add(objextension);
            _unitOfWork.Commit();
        }


        public void InsertBulkextension(IEnumerable<extension> objextension)
        {
            foreach (var item in objextension)
            {
                _extensionRepository.Add(item);
            }
            _unitOfWork.Commit();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="objextension"></param>
        public void Updateextension(extension objextension)
        {
            _extensionRepository.Update(objextension);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objextension"></param>
        public void Deleteextension(extension objextension)
        {
            _extensionRepository.Delete(objextension);
            _unitOfWork.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<extension> ExcuteSpType(string query, SqlParameter[] Parameters)
        {
            return _extensionRepository.ExcuteSpType(query, Parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable ExcuteSp(string query, SqlParameter[] Parameters)
        {
            return _extensionRepository.ExcuteSp(query, Parameters);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<extension> GetBySqlEntity(string query)
        {
            return _extensionRepository.GetBySqlEntity(query);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable GetBySql(string query)
        {
            return _extensionRepository.GetBySql(query);
        }

        #endregion

    }
}
