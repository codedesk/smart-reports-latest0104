using System;
using System.Collections.Generic;
using DataAccess.BLL;
using System.Collections;
using System.Data.SqlClient;

namespace Service.Contracts
{
    public interface ICustomerService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<customer> GetCustomers();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<customer> GetCustomers(Func<customer, bool> where);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        customer GetCustomerById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objcustomer"></param>
        void InsertCustomer(customer objcustomer);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objcustomer"></param>
        void InsertBulkCustomer(IEnumerable<customer> objcustomer);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objcustomer"></param>
        void UpdateCustomer(customer objcustomer);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objcustomer"></param>
        void DeleteCustomer(customer objcustomer);




        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<customer> ExcuteSpType(string query, SqlParameter[] Parameters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable ExcuteSp(string query, SqlParameter[] Parameters);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<customer> GetBySqlEntity(string query);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable GetBySql(string query);
        

        
    }
}
