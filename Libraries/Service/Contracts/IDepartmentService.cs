using System;
using System.Collections.Generic;
using DataAccess.BLL;
using System.Collections;
using System.Data.SqlClient;

namespace Service.Contracts
{
    public interface IDepartmentService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Department> GetDepartments();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Department> GetDepartments(Func<Department, bool> where);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Department GetDepartmentById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDepartment"></param>
        void InsertDepartment(Department objDepartment);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDepartment"></param>
        void InsertBulkDepartment(IEnumerable<Department> objDepartment);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDepartment"></param>
        void UpdateDepartment(Department objDepartment);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDepartment"></param>
        void DeleteDepartment(Department objDepartment);




        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<Department> ExcuteSpType(string query, SqlParameter[] Parameters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable ExcuteSp(string query, SqlParameter[] Parameters);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<Department> GetBySqlEntity(string query);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable GetBySql(string query);
        

        
    }
}
