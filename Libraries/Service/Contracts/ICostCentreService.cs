using System;
using System.Collections.Generic;
using DataAccess.BLL;
using System.Collections;
using System.Data.SqlClient;

namespace Service.Contracts
{
    public interface ICostCentreService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<CostCentre> GetCostCentres();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<CostCentre> GetCostCentres(Func<CostCentre, bool> where);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        CostCentre GetCostCentreById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objCostCentre"></param>
        void InsertCostCentre(CostCentre objCostCentre);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objCostCentre"></param>
        void InsertBulkCostCentre(IEnumerable<CostCentre> objCostCentre);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objCostCentre"></param>
        void UpdateCostCentre(CostCentre objCostCentre);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objCostCentre"></param>
        void DeleteCostCentre(CostCentre objCostCentre);




        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<CostCentre> ExcuteSpType(string query, SqlParameter[] Parameters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable ExcuteSp(string query, SqlParameter[] Parameters);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<CostCentre> GetBySqlEntity(string query);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable GetBySql(string query);
        

        
    }
}
