using System;
using System.Collections.Generic;
using DataAccess.BLL;
using System.Collections;
using System.Data.SqlClient;

namespace Service.Contracts
{
    public interface IExt_Dept_Dev_CCService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Ext_Dept_Dev_CC> GetExt_Dept_Dev_CCs();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Ext_Dept_Dev_CC> GetExt_Dept_Dev_CCs(Func<Ext_Dept_Dev_CC, bool> where);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Ext_Dept_Dev_CC GetExt_Dept_Dev_CCById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objExt_Dept_Dev_CC"></param>
        void InsertExt_Dept_Dev_CC(Ext_Dept_Dev_CC objExt_Dept_Dev_CC);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objExt_Dept_Dev_CC"></param>
        void InsertBulkExt_Dept_Dev_CC(IEnumerable<Ext_Dept_Dev_CC> objExt_Dept_Dev_CC);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objExt_Dept_Dev_CC"></param>
        void UpdateExt_Dept_Dev_CC(Ext_Dept_Dev_CC objExt_Dept_Dev_CC);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objExt_Dept_Dev_CC"></param>
        void DeleteExt_Dept_Dev_CC(Ext_Dept_Dev_CC objExt_Dept_Dev_CC);




        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<Ext_Dept_Dev_CC> ExcuteSpType(string query, SqlParameter[] Parameters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable ExcuteSp(string query, SqlParameter[] Parameters);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<Ext_Dept_Dev_CC> GetBySqlEntity(string query);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable GetBySql(string query);
        

        
    }
}
