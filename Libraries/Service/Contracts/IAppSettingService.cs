using System;
using System.Collections.Generic;
using DataAccess.BLL;
using System.Collections;
using System.Data.SqlClient;

namespace Service.Contracts
{
    public interface IAppSettingService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<AppSetting> GetAppSetting();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<AppSetting> GetAppSetting(Func<AppSetting, bool> where);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        AppSetting GetAppSettingById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objAppSetting"></param>
        void InsertAppSetting(AppSetting objAppSetting);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objAppSetting"></param>
        void InsertBulkAppSetting(IEnumerable<AppSetting> objAppSetting);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objAppSetting"></param>
        void UpdateAppSetting(AppSetting objAppSetting);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objAppSetting"></param>
        void DeleteAppSetting(AppSetting objAppSetting);




        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<AppSetting> ExcuteSpType(string query, SqlParameter[] Parameters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable ExcuteSp(string query, SqlParameter[] Parameters);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<AppSetting> GetBySqlEntity(string query);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable GetBySql(string query);
        

        
    }
}
