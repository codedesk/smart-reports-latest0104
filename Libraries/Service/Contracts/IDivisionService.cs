using System;
using System.Collections.Generic;
using DataAccess.BLL;
using System.Collections;
using System.Data.SqlClient;

namespace Service.Contracts
{
    public interface IDivisionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Division> GetDivisions();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Division> GetDivisions(Func<Division, bool> where);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Division GetDivisionById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDivision"></param>
        void InsertDivision(Division objDivision);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDivision"></param>
        void InsertBulkDivision(IEnumerable<Division> objDivision);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDivision"></param>
        void UpdateDivision(Division objDivision);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDivision"></param>
        void DeleteDivision(Division objDivision);




        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<Division> ExcuteSpType(string query, SqlParameter[] Parameters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable ExcuteSp(string query, SqlParameter[] Parameters);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<Division> GetBySqlEntity(string query);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable GetBySql(string query);
        

        
    }
}
