using System;
using System.Collections.Generic;
using DataAccess.BLL;
using System.Collections;
using System.Data.SqlClient;

namespace Service.Contracts
{
    public interface IExtensionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<extension> Getextensions();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<extension> Getextensions(Func<extension, bool> where);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        extension GetextensionById(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objextension"></param>
        void Insertextension(extension objextension);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objextension"></param>
        void InsertBulkextension(IEnumerable<extension> objextension);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objextension"></param>
        void Updateextension(extension objextension);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objextension"></param>
        void Deleteextension(extension objextension);




        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<extension> ExcuteSpType(string query, SqlParameter[] Parameters);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable ExcuteSp(string query, SqlParameter[] Parameters);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<extension> GetBySqlEntity(string query);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable GetBySql(string query);
        

        
    }
}
