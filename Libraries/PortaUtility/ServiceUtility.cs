﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PortaUtility.Utilities
{
   public class ServiceUtility
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Timer _serviceTimer;
        private TimerCallback _servicetimerDelegate;
        private bool _processingJobCdr;

        private bool _InProgress = false;

        public void StartService(ServiceSetting servicesetting)
        {
            ApiUtility apiUtil = new ApiUtility();

            List<TimerSettingModel> timerList = apiUtil.GetTimerSettings(servicesetting.ConnString);

            foreach (var item in timerList)
            {

                if (item.Id == 1)
                {
                    _servicetimerDelegate = new TimerCallback(GetDataFromAPI);
                    _serviceTimer = new Timer(_servicetimerDelegate, servicesetting, item.DueTime, item.Period);
                    _processingJobCdr = item.IsActive;
                }
            }

        }

        public void GetDataFromAPI(object servicesetting)
        {
            ServiceSetting _ServiceSetting = (ServiceSetting)servicesetting;

            if (!_InProgress)
            {
                log.Info("Get data from porta api Started");
                ApiUtility apiUtil = new ApiUtility();
                try
                {
                    TimerSettingModel timer = apiUtil.GetTimerSetting(1, _ServiceSetting);
                    _processingJobCdr = timer.IsActive;
                }
                catch
                {
                    _processingJobCdr = false;
                }

                if (!_processingJobCdr)
                    return;

                try
                {
                    _InProgress = true;
                    apiUtil.GetHourlyCDRS(null, _ServiceSetting).Wait();

                    apiUtil.CustomersGetSync(_ServiceSetting).Wait();

                    apiUtil.GetAccountSync(null, _ServiceSetting).Wait();

                    apiUtil.GetProductSync(null, _ServiceSetting).Wait();

                    apiUtil.GetExtensionSync(null, _ServiceSetting).Wait();

                    apiUtil.GetHuntGrupSync(null, _ServiceSetting).Wait();

                   

                    _InProgress = false;
                }
                catch (System.Exception ex)
                {
                    log.Error("Get data from porta api " + ex.Message);
                }
                finally
                {

                }

                log.Info("Get data from porta api Completed");
            }
            else
            {
                log.Info("In Progress");
            }
        }
    }
}
