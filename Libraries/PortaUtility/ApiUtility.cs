﻿using DAL;
using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;

using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TimeZoneConverter;

namespace PortaUtility.Utilities
{
    public class ApiUtility
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string LogConnString = "Data Source=213.229.116.7;Initial Catalog=SMS_Service_Logs;User ID=sa;password=@AccV12v.K2032@";
        private string BaseURL = System.Configuration.ConfigurationManager.AppSettings["BaseURL"].ToString();
        private string License = System.Configuration.ConfigurationManager.AppSettings["License"].ToString();


        private DataAccessLayer objDal = new DataAccessLayer();
        private CDRUtilities objCDRUtil = new CDRUtilities();
        private CDRUtilities_Old objCDRUtil_old = new CDRUtilities_Old();
        public string HttpCustomerActiveCall(string URI, int iCustomer, string LicenseKey)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(BaseURL + "/" + URI + "/" + LicenseKey + "/" + iCustomer + "?format=json");
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }

        public async Task CustomersGetSync(ServiceSetting servicesetting)
        {
            log.Info("Get Customer Started");
            DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Customer Started", servicesetting.LicenseKey);
            List<CustomersShortInfo> result = new List<CustomersShortInfo>();
            try
            {
                using (var client = new HttpClient())
                {
                    PrepareClient(client);

                    string url_Account = "getcustomersshortinfowithnolimit/" + servicesetting.LicenseKey + "/true?format=json";

                    HttpResponseMessage AccountResponse = await client.GetAsync(url_Account);
                    Customer customer = null;
                    if (AccountResponse.IsSuccessStatusCode)
                    {
                        customer = await AccountResponse.Content.ReadAsAsync<Customer>();
                    }

                    if (customer != null && customer.CustomersShortInfo != null)
                    {
                        result = customer.CustomersShortInfo.ToList();

                        int jobstatus = 0;
                        objDal.InsertCustomer(result, servicesetting.ConnString, ref jobstatus);

                    }

                }
            }
            catch (Exception ex)
            {
                log.Error("Get Customer " + ex.Message);
                DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get Customer " + ex.Message, servicesetting.LicenseKey);
            }
            log.Info("Get Customer Completed");
            DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Customer Completed", servicesetting.LicenseKey);
        }

        public async Task SyncActiveCalls(ServiceSetting servicesetting)
        {
            try
            {
                DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Active Calls Started", servicesetting.LicenseKey);
                ActiveCallReseller activeCallReseller = new ActiveCallReseller();
                using (var client = new HttpClient())
                {
                    PrepareClient(client);
                    string URL = "GetActiveCallsReseller/" + servicesetting.LicenseKey + "?format=json";
                    //string URL = "getAllAccountsCDRS/BD98ED49-F96D-465F-8160-94983510AFD1/2021-03-10 00:00:00/2021-03-10 23:59:59/0/0/100000/0";
                    var EncUrl = URL.Replace(" ", "%20");
                    // HTTP GET
                    HttpResponseMessage response = await client.GetAsync(EncUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        activeCallReseller = await response.Content.ReadAsAsync<ActiveCallReseller>();
                        objDal.InserActiveCalls(servicesetting.ConnString, activeCallReseller);
                        DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Active Calls Completed", servicesetting.LicenseKey);
                    }
                    else
                    {
                        Exception ex = new Exception("API CALL FAILED");
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get Active Failed " + ex.Message, servicesetting.LicenseKey);
                throw ex;

            }
        }

        public async Task GetHourlyCDRS(int? ICustomer, ServiceSetting servicesetting)
        {
            try
            {
                log.Info("Get CDR Started with update code");

                DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Hourly CDR Started at " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"), servicesetting.LicenseKey);
                log.Info("CAME IN FUNCTION AT " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"));
                List<CustomersShortInfo> SysCustomers = new List<CustomersShortInfo>();
                SysCustomers.Add(new CustomersShortInfo { ICustomer = ICustomer ?? 0 });
                if (ICustomer == null) { SysCustomers = objDal.GetCustomer(servicesetting.ConnString); }
                AllAccountCdrs CDR = new AllAccountCdrs();
                List<CustomerJob> LstCustomerJobs = new List<CustomerJob>();
                LstCustomerJobs = objDal.GetCustomerJob(servicesetting.ConnString);
                objDal.InsertCustomerjobLog(0, "Call records fetched " + LstCustomerJobs, servicesetting.ConnString);
                var currentTime = DateTime.UtcNow;
                string StartTime = currentTime.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
                string EndTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                var CustomerJob = LstCustomerJobs.FirstOrDefault();
                DateTime CroneJobFinishdatetime = objDal.getjobstartdate(servicesetting.ConnString);
                bool Insert = true;
                Insert = false;
                log.Info("CroneJobFinishdatetime value is " + CroneJobFinishdatetime + " Company " + servicesetting.CompanyName);
                log.Info(CroneJobFinishdatetime.AddMinutes(-60).ToString("yyyy-MM-dd HH:mm:ss"));

                StartTime = CroneJobFinishdatetime.AddMinutes(-60).ToString("yyyy-MM-dd HH:mm:ss");
                EndTime = currentTime.ToString("yyyy-MM-dd HH:mm:ss");
                int jobStatus = 1;
                int RecordFetched = 0;
                //while (CroneJobFinishdatetime.AddHours(-1) < DateTime.UtcNow)
                //{
                //if (CustomerJob != null)
                //{
                //    Insert = false;
                //    StartTime = CroneJobFinishdatetime.AddMinutes(-80).ToString("yyyy-MM-dd HH:mm:ss");
                //    EndTime = CroneJobFinishdatetime.AddMinutes(60).ToString("yyyy-MM-dd HH:mm:ss");
                //}
                using (var client = new HttpClient())
                {
                    PrepareClient(client);
                    jobStatus = 1;
                    RecordFetched = 0;
                    log.Info("Total Customers " + SysCustomers.Count.ToString());
                    string URL = "getAllAccountsCDRS/" + servicesetting.LicenseKey + "/" + StartTime + "/" + EndTime + "/0/0/100000/0?format=json";
                    //string URL = "getAllAccountsCDRS/BD98ED49-F96D-465F-8160-94983510AFD1/2021-03-10 00:00:00/2021-03-10 23:59:59/0/0/100000/0";
                    var EncUrl = URL.Replace(" ", "%20");
                    // HTTP GET
                    HttpResponseMessage response = await client.GetAsync(EncUrl);
                    DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Hourly CDR Started URL:  " + EncUrl, servicesetting.LicenseKey);
                    jobStatus = 1;
                    if (response.IsSuccessStatusCode)
                    {
                        CDR = await response.Content.ReadAsAsync<AllAccountCdrs>();
                        var cdrlist = CDR.lstAccountCDRS.ToList();
                        var distinctcustomer = cdrlist.Select(x => x.i_Customer).Distinct();
                        if (CDR.CDRCallsTotal > 0)
                        {
                            RecordFetched = CDR.CDRCallsTotal;
                            string Msg = objDal.InsertCDR(cdrlist, ref jobStatus, servicesetting.ConnString);
                            if (jobStatus == 0) { log.Error(Msg); }
                            else
                            {
                                log.Info(Msg + " " + CDR.CDRCallsTotal);
                                DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Hourly CDR Inserted records:  " + CDR.CDRCallsTotal, servicesetting.LicenseKey);
                                foreach (var cust in distinctcustomer)
                                {
                                    var existcustomer = LstCustomerJobs.Where(x => x.ICustomer == cust).FirstOrDefault();
                                    if (existcustomer != null)
                                    {
                                        objCDRUtil.ProcessCalls(cust, servicesetting.ConnString, StartTime);
                                    }
                                }
                            }
                        }
                        try
                        {
                            // Convert.ToDateTime(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"));
                            objDal.UpdatejobLastRunTime(Convert.ToDateTime(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")), servicesetting.ConnString);
                            DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Updated cronjob_comand last job time to :  " + DateTime.UtcNow, servicesetting.LicenseKey);
                        }
                        catch (Exception ex)
                        {
                            log.Error("Failed to Update Custoemr Job Log" + ex.Message);
                        }
                    }
                    else
                    {
                        log.Error("Failed to get data from API from Get ALL CDR status code " + response.StatusCode);
                        DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Failed to get data from API from Get ALL CDR status code  " + response.StatusCode, servicesetting.LicenseKey);
                    }

                    //}
                }
                CroneJobFinishdatetime = CroneJobFinishdatetime.AddMinutes(60);
                //CustomerJob.LastRunTime = CustomerJob.LastRunTime.AddDays(21);
                //}
                log.Info("Get CDR End");
                DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get CDR End  ", servicesetting.LicenseKey);
            }
            catch (Exception ex)
            {
                DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get CDR Error  " + ex.Message, servicesetting.LicenseKey);
                log.Error("Get CDR " + ex.Message);
            }
        }
        /// <summary>
        /// This is Old service cdrfetching code
        /// 
        /// </summary>
        /// <param name="ICustomer"></param>
        /// <param name="servicesetting"></param>
        /// <returns></returns>
        public async Task GetHourlyCDRS_Old(int? ICustomer, ServiceSetting servicesetting)
        {
            try
            {
                var currentTime = DateTime.UtcNow;
                string StartTime = currentTime.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
                string EndTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
                DateTime CroneJobFinishdatetime = objDal.getjobstartdate(servicesetting.ConnString);


                StartTime = CroneJobFinishdatetime.AddMinutes(-60).ToString("yyyy-MM-dd HH:mm:ss");
                EndTime = currentTime.ToString("yyyy-MM-dd HH:mm:ss");
                log.Info("Get CDR Started");
                List<CustomersShortInfo> SysCustomers = new List<CustomersShortInfo>();
                SysCustomers.Add(new CustomersShortInfo { ICustomer = ICustomer ?? 0 });
                if (ICustomer == null) { SysCustomers = objDal.GetCustomer(servicesetting.ConnString); }


                using (var client = new HttpClient())
                {
                    PrepareClient(client);

                    List<CustomerJob> LstCustomerJobs = new List<CustomerJob>();
                    LstCustomerJobs = objDal.GetCustomerJob(servicesetting.ConnString);
                    objDal.InsertCustomerjobLog(0, "Call records fetched " + LstCustomerJobs, servicesetting.ConnString);

                    int jobStatus = 1;
                    bool Insert = true;
                    int RecordFetched = 0;


                    log.Info("Total Customers " + SysCustomers.Count.ToString());
                    int loopCount = 0;
                    foreach (var customer in SysCustomers)
                    {
                        // var currentTime = DateTime.UtcNow;
                        //string StartTime = currentTime.AddDays(-2).ToString("yyyy-MM-dd HH:mm:ss");
                        //string EndTime = currentTime.ToString("yyyy-MM-dd HH:mm:ss");
                        var CustomerJob = LstCustomerJobs.Where(a => a.ICustomer == customer.ICustomer).FirstOrDefault();

                        //if (CustomerJob != null)
                        //{
                        //    Insert = false;
                        //    StartTime = CustomerJob.LastRunTime.AddMinutes(-60).ToString("yyyy-MM-dd HH:mm:ss");
                        //    EndTime = currentTime.ToString("yyyy-MM-dd HH:mm:ss");
                        //}

                        string URL = "getCustomerAccountCDRS/" + servicesetting.LicenseKey + "/" + customer.ICustomer + "/" + StartTime + "/" + EndTime + "/true";
                        //string URL = "https://api.activesoftswitch.com/getCustomerAccountCDRS/BD98ED49-F96D-465F-8160-94983510AFD1/411064/2021-03-10 00:00:00/2021-03-10 23:59:59/true";

                        var EncUrl = URL.Replace(" ", "%20");

                        // HTTP GET
                        HttpResponseMessage response = await client.GetAsync(EncUrl);
                        log.Info(EncUrl);
                        jobStatus = 1;
                        if (response.IsSuccessStatusCode)
                        {
                            CDR CDR = await response.Content.ReadAsAsync<CDR>();
                            if (CDR.DictPortaCustomerAccountCDRS.Voice_Calls != null)
                            {

                                RecordFetched = CDR.DictPortaCustomerAccountCDRS.Voice_Calls.Count;
                                string Msg = objDal.InsertCDR_Old(CDR.DictPortaCustomerAccountCDRS.Voice_Calls, customer.ICustomer, ref jobStatus, servicesetting.ConnString);
                                if (jobStatus == 0) { log.Error(Msg); }
                                else
                                {
                                    log.Info(Msg + " " + CDR.DictPortaCustomerAccountCDRS.Voice_Calls.Count());
                                    objCDRUtil.ProcessCalls_Old(customer.ICustomer, servicesetting.ConnString);
                                }

                                //try
                                //{
                                //    objDal.UpdateCustomerjob(customer.ICustomer, jobStatus, RecordFetched, Insert, (jobStatus == 0 ? Convert.ToDateTime(StartTime) : Convert.ToDateTime(EndTime)), Convert.ToDateTime(EndTime).AddMinutes(10), servicesetting.ConnString);

                                //}
                                //catch (Exception ex)
                                //{
                                //    log.Error("Failed to Update Custoemr Job Log" + ex.Message);
                                //}
                            }


                        }
                        else
                        {
                            log.Error("Failed to get data from API");

                        }
                        if (loopCount != 0 && loopCount % 5 == 0)
                        {
                            System.Threading.Thread.Sleep(1000);
                        }
                        loopCount++;

                    }

                    try
                    {
                        objDal.UpdatejobLastRunTime(Convert.ToDateTime(EndTime), servicesetting.ConnString);
                    }
                    catch (Exception ex)
                    {

                    }
                }

                log.Info("Get CDR End");
            }


            catch (Exception ex)
            {
                log.Error("Get CDR " + ex.Message);
            }

        }


        public async Task GetHourlyCDRS_Old_From_Live(int? ICustomer, ServiceSetting servicesetting)
        {
            Exception exception;
            int num;
            DateTime dateTime;
            try
            {

                DataAccessLayer.InserCDRServiceLogs(LogConnString, "Info", "Get CDR Started", servicesetting.LicenseKey);
                List<CustomersShortInfo> customersShortInfos = new List<CustomersShortInfo>();
                List<CustomersShortInfo> customersShortInfos1 = customersShortInfos;
                CustomersShortInfo customersShortInfo = new CustomersShortInfo();
                CustomersShortInfo customersShortInfo1 = customersShortInfo;
                int? customer = ICustomer;
                num = (customer.HasValue ? customer.GetValueOrDefault() : 0);
                customersShortInfo1.ICustomer = num;
                customersShortInfos1.Add(customersShortInfo);
                if (!ICustomer.HasValue)
                {
                    customersShortInfos = this.objDal.GetCustomer(servicesetting.ConnString);
                }
                HttpClient httpClient = new HttpClient();
                try
                {
                    this.PrepareClient(httpClient);
                    List<CustomerJob> customerJobs = new List<CustomerJob>();
                    customerJobs = this.objDal.GetCustomerJob(servicesetting.ConnString);
                    this.objDal.InsertCustomerjobLog(0, string.Concat("Call records fetched ", customerJobs), servicesetting.ConnString);
                    int num1 = 1;
                    bool flag = true;
                    int count = 0;
                    //ILog log = ApiUtility.log;
                    int count1 = customersShortInfos.Count;
                    log.Info(string.Concat("Total Customers ", count1.ToString()));
                    int loopCount = 0;
                    foreach (CustomersShortInfo customersShortInfo2 in customersShortInfos)
                    {
                        DateTime utcNow = DateTime.UtcNow;
                        DateTime dateTime1 = utcNow.AddDays(-1);
                        string str = dateTime1.ToString("yyyy-MM-dd HH:mm:ss");
                        string str1 = utcNow.ToString("yyyy-MM-dd HH:mm:ss");
                        List<CustomerJob> customerJobs1 = customerJobs;
                        (
                            from a in customerJobs1
                            where a.ICustomer == customersShortInfo2.ICustomer
                            select a).FirstOrDefault<CustomerJob>();
                        object[] licenseKey = new object[] { "getCustomerAccountCDRS/", servicesetting.LicenseKey, "/", customersShortInfo2.ICustomer, "/", str, "/", str1, "/true" };
                        string str2 = string.Concat(licenseKey);
                        string str3 = str2.Replace(" ", "%20");
                        HttpResponseMessage async = await httpClient.GetAsync(str3);
                        DataAccessLayer.InserCDRServiceLogs(LogConnString, "Info", "Get CDR Service URL " + str3, servicesetting.LicenseKey);
                        num1 = 1;
                        if (!async.IsSuccessStatusCode)
                        {
                            ApiUtility.log.Error("Failed to get data from API");
                            DataAccessLayer.InserCDRServiceLogs(LogConnString, "Error", "Get CDR Service Error In API " + async.StatusCode, servicesetting.LicenseKey);
                        }
                        else
                        {
                            CDR cDR = await HttpContentExtensions.ReadAsAsync<CDR>(async.Content);
                            if (cDR.DictPortaCustomerAccountCDRS.Voice_Calls != null)
                            {
                                count = cDR.DictPortaCustomerAccountCDRS.Voice_Calls.Count;
                                string str4 = this.objDal.InsertCDR_Old(cDR.DictPortaCustomerAccountCDRS.Voice_Calls, customersShortInfo2.ICustomer, ref num1, servicesetting.ConnString);
                                if (num1 != 0)
                                {
                                    DataAccessLayer.InserCDRServiceLogs(LogConnString, "Info", "Successfully inserted records for customer  " + customersShortInfo2.ICustomer, servicesetting.LicenseKey);
                                    ApiUtility.log.Info(string.Concat(str4, " ", cDR.DictPortaCustomerAccountCDRS.Voice_Calls.Count<hourlycdr>()));
                                    this.objCDRUtil.ProcessCalls_Old(customersShortInfo2.ICustomer, servicesetting.ConnString);
                                    DataAccessLayer.InserCDRServiceLogs(LogConnString, "Info", "Successfully processed calls for customer  " + customersShortInfo2.ICustomer, servicesetting.LicenseKey);
                                }
                                else
                                {
                                    ApiUtility.log.Error(str4);
                                }
                                try
                                {
                                    DataAccessLayer dataAccessLayer = this.objDal;
                                    int customer1 = customersShortInfo2.ICustomer;
                                    int num2 = num1;
                                    int num3 = count;
                                    bool flag1 = flag;
                                    dateTime = (num1 == 0 ? Convert.ToDateTime(str) : Convert.ToDateTime(str1));
                                    dateTime1 = Convert.ToDateTime(str1);
                                    dataAccessLayer.UpdateCustomerjob(customer1, num2, num3, flag1, dateTime, dateTime1.AddMinutes(10), servicesetting.ConnString);
                                }
                                catch (Exception exception1)
                                {
                                    exception = exception1;
                                    ApiUtility.log.Error(string.Concat("Failed to Update Custoemr Job Log", exception.Message));
                                }
                            }
                        }
                        if (loopCount != 0 && loopCount % 5 == 0)
                        {
                            System.Threading.Thread.Sleep(5000);
                        }
                        loopCount++;
                    }
                }
                finally
                {
                    if (httpClient != null)
                    {
                        ((IDisposable)httpClient).Dispose();
                    }
                }
                ApiUtility.log.Info("Get CDR End");
            }
            catch (Exception exception2)
            {
                DataAccessLayer.InserCDRServiceLogs(LogConnString, "Info", "Get CDR ServiceException " + exception2.Message, servicesetting.LicenseKey);
                Console.WriteLine(exception2.Message);
                ApiUtility.log.Error(string.Concat("Get CDR ", exception2.Message));
                //throw exception2;
            }
        }






        public async Task GetAccountSync(int? ICustomer, ServiceSetting servicesetting)
        {
            log.Info("Get Account Started");
            DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Account Started  ", servicesetting.LicenseKey);
            try
            {

                List<CustomersShortInfo> SysCustomers = new List<CustomersShortInfo>();
                SysCustomers.Add(new CustomersShortInfo { ICustomer = ICustomer ?? 0 });
                if (ICustomer == null) { SysCustomers = objDal.GetCustomer(servicesetting.ConnString); }


                using (var client = new HttpClient())
                {
                    PrepareClient(client);

                    string JobStatusMsg = "";
                    int jobStatus = 1;
                    int loopCount = 0;
                    foreach (var customer in SysCustomers)
                    {
                        string url_Account = "GetAccountByICustomer/" + servicesetting.LicenseKey + "/" + customer.ICustomer + "?format=json";

                        HttpResponseMessage AccountResponse = await client.GetAsync(url_Account);
                        try
                        {
                            if (AccountResponse.IsSuccessStatusCode)
                            {
                                CustomerAccounts lstAcct = await AccountResponse.Content.ReadAsAsync<CustomerAccounts>();
                                JobStatusMsg = objDal.InsertAccounts(lstAcct.Accounts, customer.ICustomer, ref jobStatus, servicesetting.ConnString);
                            }
                            else
                            {
                                DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get Account API FAILED  " + AccountResponse.StatusCode, servicesetting.LicenseKey);
                            }
                        }


                        catch (Exception ex)
                        {
                            jobStatus = 0;
                        }
                        if (loopCount != 0 && loopCount % 5 == 0)
                        {
                            System.Threading.Thread.Sleep(5000);
                        }
                        loopCount++;

                    }
                }

            }
            catch (Exception ex)
            {
                log.Error("Get Account " + ex.Message);
                DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get Account Error  " + ex.Message, servicesetting.LicenseKey);
            }
            DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Account Completed  ", servicesetting.LicenseKey);
            log.Info("Get Account Completed");
        }


        public async Task GetProductSync(int? ICustomer, ServiceSetting servicesetting)
        {
            log.Info("Get Product Started");
            DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Product started  ", servicesetting.LicenseKey);
            try
            {
                List<CustomersShortInfo> SysCustomers = new List<CustomersShortInfo>();
                SysCustomers.Add(new CustomersShortInfo { ICustomer = ICustomer ?? 0 });

                if (ICustomer == null)
                {
                    SysCustomers = objDal.GetCustomer(servicesetting.ConnString);
                }

                using (var client = new HttpClient())
                {
                    PrepareClient(client);

                    string JobStatusMsg = "";
                    int jobStatus = 1;
                    int loopCount = 0;
                    foreach (var customer in SysCustomers)
                    {
                        string url_Account = "GetCustomerProducts/" + servicesetting.LicenseKey + "/" + customer.ICustomer + "?format=json";

                        HttpResponseMessage ProductResponse = await client.GetAsync(url_Account);
                        objDal.InsertCustomerjobLog(0, "Processing: " + url_Account, servicesetting.ConnString);
                        jobStatus = 0;
                        if (ProductResponse.IsSuccessStatusCode)
                        {
                            Products lstProducts = await ProductResponse.Content.ReadAsAsync<Products>();
                            JobStatusMsg = objDal.InsertProducts(lstProducts.PortaProducts, customer.ICustomer, ref jobStatus, servicesetting.ConnString);
                            objDal.InsertCustomerjobLog(0, "Finished: " + url_Account, servicesetting.ConnString);
                        }
                        else
                        {
                            DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get Product API Failed   " + ProductResponse.StatusCode, servicesetting.LicenseKey);
                        }
                        if (loopCount != 0 && loopCount % 5 == 0)
                        {
                            System.Threading.Thread.Sleep(5000);
                        }
                        loopCount++;

                    }
                }

            }
            catch (Exception ex)
            {
                DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get Product Error  " + ex.Message, servicesetting.LicenseKey);
                log.Error("Get Product " + ex.Message);
            }
            //DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Product Compelted  ", servicesetting.LicenseKey);
            log.Info("Get Product Completed");
        }

        public async Task GetExtensionSync(int? ICustomer, ServiceSetting servicesetting)
        {
            log.Info("Get Extension Started");
            DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Extension started  ", servicesetting.LicenseKey);
            List<ExtensionLst> result = new List<ExtensionLst>();
            try
            {
                List<CustomersShortInfo> SysCustomers = new List<CustomersShortInfo>();
                SysCustomers.Add(new CustomersShortInfo { ICustomer = ICustomer ?? 0 });

                if (ICustomer == null)
                {
                    SysCustomers = objDal.GetCustomer(servicesetting.ConnString);
                }

                using (var client = new HttpClient())
                {
                    PrepareClient(client);
                    int loopCount = 0;
                    foreach (var customer in SysCustomers)
                    {
                        string url_Account = "GetCustomerExtensions/" + servicesetting.LicenseKey + "/" + customer.ICustomer + "?format=json";

                        HttpResponseMessage AccountResponse = await client.GetAsync(url_Account);
                        Extension extension = null;
                        if (AccountResponse.IsSuccessStatusCode)
                        {
                            extension = await AccountResponse.Content.ReadAsAsync<Extension>();
                        }
                        else
                        {
                            DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get Extension API FAILED  " + AccountResponse.StatusCode, servicesetting.LicenseKey);
                        }
                        if (extension != null && extension.ExtensionLst != null)
                        {
                            result = extension.ExtensionLst.ToList();

                            int jobstatus = 0;
                            objDal.InsertExtension(result, customer.ICustomer, ref jobstatus, servicesetting.ConnString);

                        }
                        if (loopCount != 0 && loopCount % 5 == 0)
                        {
                            System.Threading.Thread.Sleep(5000);
                        }
                        loopCount++;

                    }

                }
            }
            catch (Exception ex)
            {
                DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get Extension Error  " + ex.Message, servicesetting.LicenseKey);
                log.Error("Get Extension " + ex.Message);
            }
            DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Extension Completed  ", servicesetting.LicenseKey);
            log.Info("Get Extension Completed");

        }

        public async Task GetHuntGrupSync(int? ICustomer, ServiceSetting servicesetting)
        {
            log.Info("Get HuntGroup Started");
            DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Hunt Group Started  ", servicesetting.LicenseKey);
            List<HGInfoLst> result = new List<HGInfoLst>();
            try
            {
                List<CustomersShortInfo> SysCustomers = new List<CustomersShortInfo>();
                SysCustomers.Add(new CustomersShortInfo { ICustomer = ICustomer ?? 0 });

                if (ICustomer == null)
                {
                    SysCustomers = objDal.GetCustomer(servicesetting.ConnString);
                }
                
                using (var client = new HttpClient())
                {
                    PrepareClient(client);
                    int loopCount = 0;
                    foreach (var customer in SysCustomers)
                    {
                        string url_Account = "GetCustomerHuntGroups/" + servicesetting.LicenseKey + "/" + customer.ICustomer + "?format=json";

                        HttpResponseMessage AccountResponse = await client.GetAsync(url_Account);
                        HuntGroup extension = null;
                        if (AccountResponse.IsSuccessStatusCode)
                        {
                            extension = await AccountResponse.Content.ReadAsAsync<HuntGroup>();
                        }
                        else
                        {
                            DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get Hunt Group API FAILED  " + AccountResponse.StatusCode, servicesetting.LicenseKey);
                        }

                        if (extension != null && extension.HGInfoLst != null)
                        {
                            result = extension.HGInfoLst;

                            int jobstatus = 0;
                            objDal.InsertHuntGroup(result, customer.ICustomer, ref jobstatus, servicesetting.ConnString);

                        }
                        if (loopCount != 0 && loopCount % 5 == 0)
                        {
                            System.Threading.Thread.Sleep(5000);
                        }
                        loopCount++;
                    }

                }
            }
            catch (Exception ex)
            {
                DataAccessLayer.InserServiceLogs(LogConnString, "Error", "Get Hunt Group Error  " + ex.Message, servicesetting.LicenseKey);
                log.Error("Get Extension " + ex.Message);
            }
            DataAccessLayer.InserServiceLogs(LogConnString, "Info", "Get Hunt Group Completed  ", servicesetting.LicenseKey);
            log.Info("Get Hunt Group Completed");
        }

        public List<TimerSettingModel> GetTimerSettings(string sqlConn)
        {
            List<TimerSettingModel> lst = new List<TimerSettingModel>();
            try
            {
                lst = objDal.GetTimerSettings(sqlConn);
            }
            catch (Exception ex)
            {
                log.Error("Get Timer Settings " + ex.Message);
            }
            return lst;
        }

        public TimerSettingModel GetTimerSetting(int id, ServiceSetting servicesetting)
        {
            TimerSettingModel timersetting = new TimerSettingModel();
            try
            {
                timersetting = objDal.GetTimerSetting(id, servicesetting.ConnString);
            }
            catch (Exception ex)
            {
                log.Error("Get Timer Setting " + ex.Message);
            }
            return timersetting;
        }


        private void PrepareClient(HttpClient client)
        {
            client.Timeout = new TimeSpan(0, 30, 0);
            client.BaseAddress = new Uri(BaseURL);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }


    }
}
