﻿using DAL;
using log4net;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace PortaUtility
{
    public class CDRUtilities_Old
    {
        private readonly static ILog log;

        private DataAccessLayer objDal = new DataAccessLayer();

        private string GetPeriod(DateTime connect_time)
        {
            string str = "00:00 - 01:00";
            switch (connect_time.Hour)
            {
                case 0:
                    {
                        str = "00:00 - 00:59";
                        break;
                    }
                case 1:
                    {
                        str = "01:00 - 01:59";
                        break;
                    }
                case 2:
                    {
                        str = "02:00 - 02:59";
                        break;
                    }
                case 3:
                    {
                        str = "03:00 - 03:59";
                        break;
                    }
                case 4:
                    {
                        str = "04:00 - 04:59";
                        break;
                    }
                case 5:
                    {
                        str = "05:00 - 05:59";
                        break;
                    }
                case 6:
                    {
                        str = "06:00 - 06:59";
                        break;
                    }
                case 7:
                    {
                        str = "07:00 - 07:59";
                        break;
                    }
                case 8:
                    {
                        str = "08:00 - 08:59";
                        break;
                    }
                case 9:
                    {
                        str = "09:00 - 09:59";
                        break;
                    }
                case 10:
                    {
                        str = "10:00 - 10:59";
                        break;
                    }
                case 11:
                    {
                        str = "11:00 - 11:59";
                        break;
                    }
                case 12:
                    {
                        str = "12:00 - 12:59";
                        break;
                    }
                case 13:
                    {
                        str = "13:00 - 13:59";
                        break;
                    }
                case 14:
                    {
                        str = "14:00 - 14:59";
                        break;
                    }
                case 15:
                    {
                        str = "15:00 - 15:59";
                        break;
                    }
                case 16:
                    {
                        str = "16:00 - 16:59";
                        break;
                    }
                case 17:
                    {
                        str = "17:00 - 17:59";
                        break;
                    }
                case 18:
                    {
                        str = "18:00 - 18:59";
                        break;
                    }
                case 19:
                    {
                        str = "19:00 - 19:59";
                        break;
                    }
                case 20:
                    {
                        str = "20:00 - 20:59";
                        break;
                    }
                case 21:
                    {
                        str = "21:00 - 21:59";
                        break;
                    }
                case 22:
                    {
                        str = "22:00 - 22:59";
                        break;
                    }
                case 23:
                    {
                        str = "23:00 - 23:59";
                        break;
                    }
            }
            return str;
        }

        private bool IsAllDigits(string s)
        {
            bool flag;
            string str = s;
            int num = 0;
            while (true)
            {
                if (num >= str.Length)
                {
                    flag = true;
                    break;
                }
                else if (char.IsDigit(str[num]))
                {
                    num++;
                }
                else
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }

        public List<LstActiveCall> ProcessActiveCalls(int ICustomer, List<LstActiveCall> lstCDR, string ConnString)
        {
            ExtensionLst extensionLst;
            List<LstActiveCall> lstActiveCalls = new List<LstActiveCall>();
            List<ExtensionLst> extensions = this.objDal.GetExtensions(ICustomer, ConnString);
            this.objDal.GetProductByType(ICustomer, 1, ConnString);
            if ((lstCDR == null ? false : lstCDR.Count > 0))
            {
                foreach (LstActiveCall lstActiveCall in lstCDR)
                {
                    LstActiveCall durationMins = new LstActiveCall();
                    if (lstActiveCall.CallType == "OUTGOING")
                    {
                        extensionLst = (
                            from i in extensions
                            where i.i_account == lstActiveCall.IAccount
                            select i).FirstOrDefault<ExtensionLst>();
                        if (extensionLst != null)
                        {
                            durationMins.CallType = "OUTGOING";
                            durationMins.CLI = extensionLst.name;
                            durationMins.Name = extensionLst.name;
                            durationMins.DurationMins = lstActiveCall.DurationMins;
                            durationMins.CLD = lstActiveCall.CLD;
                            lstActiveCalls.Add(durationMins);
                        }
                    }
                    else if (lstActiveCall.CallType == "INCOMING")
                    {
                        extensionLst = (
                            from i in extensions
                            where i.i_account == lstActiveCall.IAccount
                            select i).FirstOrDefault<ExtensionLst>();
                        if (extensionLst != null)
                        {
                            durationMins.CallType = "INCOMING";
                            durationMins.CLI = lstActiveCall.CLI;
                            durationMins.Name = extensionLst.name;
                            durationMins.DurationMins = lstActiveCall.DurationMins;
                            durationMins.CLD = extensionLst.name;
                            lstActiveCalls.Add(durationMins);
                        }
                    }
                }
            }
            return lstActiveCalls;
        }

        public void ProcessCalls(int ICustomer, string ConnString)
        {
            List<hourlycdr> cDR = this.objDal.GetCDR_Old_Live(ICustomer, ConnString);
            List<Account> account = this.objDal.GetAccount(ICustomer, ConnString);
            List<ExtensionLst> extensions = this.objDal.GetExtensions(ICustomer, ConnString);
            List<Product> product = this.objDal.GetProduct(ICustomer, ConnString);
            List<DefinedProduct> productByType = this.objDal.GetProductByType(ICustomer, 1, ConnString);
            this.objDal.GetProductByType(ICustomer, 2, ConnString);
            SystemSetting systemSetting = this.objDal.GetSystemSetting(ICustomer, ConnString);
            if ((cDR == null ? false : cDR.Count > 0))
            {
                List<IGrouping<string, hourlycdr>> list = (
                    from u in cDR
                    group u by u.CallId into grp
                    select grp).ToList<IGrouping<string, hourlycdr>>();
                if (list != null)
                {
                    foreach (IGrouping<string, hourlycdr> strs in list)
                    {
                        Call call = new Call()
                        {
                            CallId = strs.Key
                        };
                        
                        List<hourlycdr> hourlycdrs = (
                            from c in cDR
                            where c.CallId == strs.Key
                            select c).ToList<hourlycdr>();
                        int count = hourlycdrs.Count;
                        bool flag = false;
                        call.hourly_cdr_id = string.Join(",", (
                            from x in hourlycdrs
                            select x.hourly_cdr_id.ToString()).ToArray<string>());
                        call.TalkTime = hourlycdrs.Max<hourlycdr>((hourlycdr a) => a.Charged_Quantity);
                        call.ConnectTime = hourlycdrs.Min<hourlycdr, DateTime>((hourlycdr a) => a.Connect_time);
                        call.DisconnectTime = hourlycdrs.Max<hourlycdr, DateTime>((hourlycdr a) => a.Disconnect_time);
                        call.Charged_Amount = hourlycdrs.FirstOrDefault<hourlycdr>().Charged_Amount;
                        call.ICustomer = ICustomer;
                        call.PeriodId = call.ConnectTime.Hour;
                        call.Legs = count;
                        var collection = (
                            from c in hourlycdrs
                            join e in extensions on c.IAccount equals e.i_account
                            select new { CallType = c.CallType, ExtensionID = e.ExtensionID, id = e.id }).Distinct().ToList();
                        var list1 = (
                            from c in hourlycdrs
                            join a in account on c.IAccount equals a.IAccount
                            select new { Id = a.Id, Name = a.Name, IAccount = a.IAccount, IProduct = a.IProduct, CallType = c.CallType }).Distinct().ToList();
                        var collection1 = (
                            from a in list1
                            join p in productByType on a.IProduct equals p.IProduct
                            select new { Id = a.Id, Name = a.Name, CallType = a.CallType, IProduct = a.IProduct, IAccount = a.IAccount }).Distinct().ToList();
                        hourlycdr _hourlycdr = hourlycdrs.FirstOrDefault<hourlycdr>();
                        if(_hourlycdr.BitFlags == 8 || _hourlycdr.BitFlags == 12 || _hourlycdr.BitFlags == 40 || _hourlycdr.BitFlags == 72 || _hourlycdr.BitFlags == 104)
                        {
                            Console.Write("Incoming call " + _hourlycdr.CallId);
                        }
                        
                        if (!(count != collection.Count() ? true : this.IsAllDigits(_hourlycdr.CallType.Trim())))
                        {
                            flag = true;
                            call.CallTypeId = 0;
                            this.ProcessLegs(hourlycdrs, account, product, extensions, call);
                        }
                        else if (!(count != 1 ? true : !this.IsAllDigits(_hourlycdr.CallType.Trim())))
                        {
                            flag = true;
                            call.CallTypeId = 2;
                            call.ExtensionID = (collection.FirstOrDefault() == null ? 0 : collection.FirstOrDefault().ExtensionID);
                            call.CLI = _hourlycdr.CLI;
                            call.DID = _hourlycdr.CLD;
                            call.IAccount = _hourlycdr.IAccount;
                            this.ProcessLegs(hourlycdrs, account, product, extensions, call);
                        }
                        else if (!(collection.Count() <= 0 || collection1.Count <= 0 ? true : (
                            from i in collection
                            where i.CallType == "UMRECORD"
                            select i).Count() != 0))
                        {
                            flag = true;
                            call.DID = collection1.FirstOrDefault().Id;
                            call.CLI = _hourlycdr.CLI;
                            call.IAccount = collection1.FirstOrDefault().IAccount;
                            call.ExtensionID = collection.FirstOrDefault().ExtensionID;
                            call.RingTime = hourlycdrs.Max<hourlycdr>((hourlycdr a) => a.Charged_Quantity) - hourlycdrs.Min<hourlycdr>((hourlycdr a) => a.Charged_Quantity);
                            call.TalkTime = hourlycdrs.Min<hourlycdr>((hourlycdr a) => a.Charged_Quantity);
                            call.AnswerInTarget = 1;
                            if (call.RingTime >= systemSetting.AnswerTime)
                            {
                                call.AnswerInTarget = 0;
                            }
                            call.CallTypeId = 1;
                            this.ProcessLegs(hourlycdrs, account, product, extensions, call);
                        }
                        else if (!(collection.Count() <= 0 || collection1.Count <= 0 ? true : (
                            from i in collection
                            where i.CallType == "UMRECORD"
                            select i).Count() <= 0))
                        {
                            flag = true;
                            call.CallTypeId = 1;
                            call.MissedCall = true;
                            call.DID = collection1.FirstOrDefault().Id;
                            call.CLI = _hourlycdr.CLI;
                            call.IAccount = collection1.FirstOrDefault().IAccount;
                            call.ExtensionID = collection.FirstOrDefault().ExtensionID;
                            call.RingTime = hourlycdrs.Max<hourlycdr>((hourlycdr a) => a.Charged_Quantity) - hourlycdrs.Min<hourlycdr>((hourlycdr a) => a.Charged_Quantity);
                            call.TalkTime = hourlycdrs.Min<hourlycdr>((hourlycdr a) => a.Charged_Quantity);
                            this.ProcessLegs(hourlycdrs, account, product, extensions, call);
                        }
                        else if (!(collection.Count() != 0 || collection1.Count <= 0 ? true : (
                            from i in collection1
                            where i.CallType == "UMRECORD"
                            select i).Count() <= 0))
                        {
                            flag = true;
                            call.CallTypeId = 1;
                            call.MissedCall = true;
                            call.DID = collection1.FirstOrDefault().Id;
                            call.CLI = _hourlycdr.CLI;
                            call.IAccount = collection1.FirstOrDefault().IAccount;
                            call.RingTime = hourlycdrs.Max<hourlycdr>((hourlycdr a) => a.Charged_Quantity) - hourlycdrs.Min<hourlycdr>((hourlycdr a) => a.Charged_Quantity);
                            call.TalkTime = hourlycdrs.Min<hourlycdr>((hourlycdr a) => a.Charged_Quantity);
                        }
                        else if ((count != 1 || collection1.Count <= 0 ? true : !(_hourlycdr.CallType == "UMRECORD")))
                        {
                        }
                        if (flag)
                        {
                            this.objDal.InsertCall(call, ConnString);
                        }
                    }
                }
            }
        }

        public void ProcessLegs(List<hourlycdr> complete_call, List<Account> lstAccount, List<Product> lstProduct, List<ExtensionLst> lstExtension, Call objCall)
        {
            objCall.CallDetails = new List<CallDetail>();
            foreach (hourlycdr completeCall in complete_call)
            {
                ExtensionLst extensionLst = new ExtensionLst();
                if ((
                    from i in lstExtension
                    where i.i_account == completeCall.IAccount
                    select i).FirstOrDefault<ExtensionLst>() != null)
                {
                    CallDetail callDetail = new CallDetail()
                    {
                        TalkTime = completeCall.Charged_Quantity,
                        PeriodId = completeCall.Connect_time.Hour,
                        ConnectTime = completeCall.Connect_time,
                        DisconnectTime = completeCall.Disconnect_time,
                        IAccount = completeCall.IAccount,
                        RingTime = 0
                    };
                    objCall.CallDetails.Add(callDetail);
                }
            }
        }
    }
}


