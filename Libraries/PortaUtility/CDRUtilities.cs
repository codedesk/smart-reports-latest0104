﻿using DAL;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortaUtility
{
    public class CDRUtilities
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private DataAccessLayer objDal = new DataAccessLayer();
        public void ProcessCalls(int ICustomer, String ConnString, string datetime)
        {
            //DateTime Start = Convert.ToDateTime("2016-03-15 14:18:31.000");
            //DateTime End = Convert.ToDateTime("2016-03-15 17:12:56.000");

            var lstCDR = objDal.GetCDR(ICustomer, ConnString, datetime);

            var lstAccount = objDal.GetAccount(ICustomer, ConnString);

            var lstExtension = objDal.GetExtensions(ICustomer, ConnString);

            var lstProduct = objDal.GetProduct(ICustomer, ConnString);

            var DIDProducts = objDal.GetProductByType(ICustomer, 1, ConnString);

            var ExtProducts = objDal.GetProductByType(ICustomer, 2, ConnString);

            var systemSettings = objDal.GetSystemSetting(ICustomer, ConnString);


            if (lstCDR != null && lstCDR.Count > 0)
            {

                var Calls = lstCDR
                     .GroupBy(u => u.CallId)
                     .Select(grp => grp)
                     .ToList();

                if (Calls != null)
                {
                    foreach (var call in Calls)
                    {
                        Call objCall = new Call();

                        objCall.CallId = call.Key;


                        var complete_call = lstCDR.Where(c => c.CallId == call.Key).ToList();

                        var total_Legs = complete_call.Count;
                        bool IsRecognized = false;

                        objCall.hourly_cdr_id = String.Join(",", complete_call.Select(x => x.hourly_cdr_id.ToString()).ToArray());

                        objCall.TalkTime = complete_call.Max(a => a.Charged_Quantity);

                        objCall.ConnectTime = complete_call.Min(a => a.Connect_time);

                        objCall.DisconnectTime = complete_call.Max(a => a.Disconnect_time);

                        objCall.ICustomer = ICustomer;
                        objCall.PeriodId = objCall.ConnectTime.Hour;
                        objCall.Legs = total_Legs;



                        var ExtInLegs = (from c in complete_call
                                         join e in lstExtension
                                         on c.IAccount equals e.i_account
                                         select new { c.CallType, e.ExtensionID, e.id }).Distinct().ToList();

                        var AccountInLegs = (from c in complete_call
                                             join a in lstAccount
                                             on c.IAccount equals a.IAccount
                                             select new { a.Id, a.Name, a.IAccount, a.IProduct, c.CallType }).Distinct().ToList();

                        var DIDInLegs = (from a in AccountInLegs
                                         join p in DIDProducts on a.IProduct equals p.IProduct
                                         select new { a.Id, a.Name, a.CallType, a.IProduct, a.IAccount }).Distinct().ToList();

                        var FirstLeg = complete_call.FirstOrDefault();

                        if(string.IsNullOrEmpty(FirstLeg.CallType))
                        {
                            FirstLeg.CallType = "";
                        }
                        if (total_Legs == ExtInLegs.Count() && !IsAllDigits(FirstLeg.CallType.Trim()))
                        {
                            //Internal
                            IsRecognized = true;
                            objCall.CallTypeId = 0;
                            ProcessLegs(complete_call, lstAccount, lstProduct, lstExtension, objCall);
                        }
                        else if (total_Legs == 1 && IsAllDigits(FirstLeg.CallType.Trim()))
                        {
                            //OUT
                            IsRecognized = true;
                            objCall.CallTypeId = 2;
                            objCall.ExtensionID = ExtInLegs.FirstOrDefault() == null ? 0 : ExtInLegs.FirstOrDefault().ExtensionID;
                            objCall.CLI = FirstLeg.CLI;
                            objCall.DID = FirstLeg.CLD;
                            objCall.IAccount = FirstLeg.IAccount;

                            ProcessLegs(complete_call, lstAccount, lstProduct, lstExtension, objCall);
                        }
                        else if (ExtInLegs.Count() > 0 && DIDInLegs.Count > 0 && ExtInLegs.Where(i => i.CallType == "UMRECORD").Count() == 0)
                        {
                            //IN
                            IsRecognized = true;
                            objCall.DID = DIDInLegs.FirstOrDefault().Id;
                            objCall.CLI = FirstLeg.CLI;
                            objCall.IAccount = DIDInLegs.FirstOrDefault().IAccount;
                            objCall.ExtensionID = ExtInLegs.FirstOrDefault().ExtensionID;

                            objCall.RingTime = complete_call.Max(a => a.Charged_Quantity) - complete_call.Min(a => a.Charged_Quantity);
                            objCall.TalkTime = complete_call.Min(a => a.Charged_Quantity);

                            objCall.AnswerInTarget = 1;
                            if (objCall.RingTime >= systemSettings.AnswerTime)
                            {
                                objCall.AnswerInTarget = 0;
                            }

                            objCall.CallTypeId = 1;
                            ProcessLegs(complete_call, lstAccount, lstProduct, lstExtension, objCall);
                        }
                        else if (ExtInLegs.Count() > 0 && DIDInLegs.Count > 0 && ExtInLegs.Where(i => i.CallType == "UMRECORD").Count() > 0)
                        {
                            //Missed at Extension
                            IsRecognized = true;
                            objCall.CallTypeId = 1;
                            objCall.MissedCall = true;

                            objCall.DID = DIDInLegs.FirstOrDefault().Id;
                            objCall.CLI = FirstLeg.CLI;
                            objCall.IAccount = DIDInLegs.FirstOrDefault().IAccount;
                            objCall.ExtensionID = ExtInLegs.FirstOrDefault().ExtensionID;

                            objCall.RingTime = complete_call.Max(a => a.Charged_Quantity) - complete_call.Min(a => a.Charged_Quantity);
                            objCall.TalkTime = complete_call.Min(a => a.Charged_Quantity);

                            ProcessLegs(complete_call, lstAccount, lstProduct, lstExtension, objCall);
                        }
                        else if (ExtInLegs.Count() == 0 && DIDInLegs.Count > 0 && DIDInLegs.Where(i => i.CallType == "UMRECORD").Count() > 0)
                        {
                            //Missed at DID
                            IsRecognized = true;
                            objCall.CallTypeId = 1;
                            objCall.MissedCall = true;

                            objCall.DID = DIDInLegs.FirstOrDefault().Id;
                            objCall.CLI = FirstLeg.CLI;
                            objCall.IAccount = DIDInLegs.FirstOrDefault().IAccount;

                            objCall.RingTime = complete_call.Max(a => a.Charged_Quantity) - complete_call.Min(a => a.Charged_Quantity);
                            objCall.TalkTime = complete_call.Min(a => a.Charged_Quantity);


                            //ProcessLegs(complete_call, lstAccount, lstProduct, lstExtension, objCall);
                        }
                        else if (total_Legs == 1 && DIDInLegs.Count > 0 && FirstLeg.CallType == "UMRECORD")
                        {
                            //Abandant
                            //log.Info("Call Abundant " + FirstLeg.CallId);
                            //Console.WriteLine("Abundant " + FirstLeg.CallId);
                        }
                        else
                        {
                            //log.Info("Call not recognized " + FirstLeg.CallId);
                            //Console.WriteLine("Call not recognized" + FirstLeg.CallId);
                        }

                        if (IsRecognized)
                        {
                            objDal.InsertCall(objCall, ConnString);
                        }


                    }
                }

            }

        }
        public void ProcessCalls_Old(int ICustomer, String ConnString)
        {
            //DateTime Start = Convert.ToDateTime("2016-03-15 14:18:31.000");
            //DateTime End = Convert.ToDateTime("2016-03-15 17:12:56.000");

            var lstCDR = objDal.GetCDR_Old(ICustomer, ConnString);

            var lstAccount = objDal.GetAccount(ICustomer, ConnString);

            var lstExtension = objDal.GetExtensions(ICustomer, ConnString);

            var lstProduct = objDal.GetProduct(ICustomer, ConnString);

            var DIDProducts = objDal.GetProductByType(ICustomer, 1, ConnString);

            var ExtProducts = objDal.GetProductByType(ICustomer, 2, ConnString);

            var systemSettings = objDal.GetSystemSetting(ICustomer, ConnString);


            if (lstCDR != null && lstCDR.Count > 0)
            {

                var Calls = lstCDR
                     .GroupBy(u => u.CallId)
                     .Select(grp => grp)
                     .ToList();

                if (Calls != null)
                {
                    foreach (var call in Calls)
                    {
                        Call objCall = new Call();

                        objCall.CallId = call.Key;


                        var complete_call = lstCDR.Where(c => c.CallId == call.Key).ToList();

                        var total_Legs = complete_call.Count;
                        bool IsRecognized = false;

                        objCall.hourly_cdr_id = String.Join(",", complete_call.Select(x => x.hourly_cdr_id.ToString()).ToArray());

                        objCall.TalkTime = complete_call.Max(a => a.Charged_Quantity);

                        objCall.ConnectTime = complete_call.Min(a => a.Connect_time);

                        objCall.DisconnectTime = complete_call.Max(a => a.Disconnect_time);

                        objCall.ICustomer = ICustomer;
                        objCall.PeriodId = objCall.ConnectTime.Hour;
                        objCall.Legs = total_Legs;



                        var ExtInLegs = (from c in complete_call
                                         join e in lstExtension
                                         on c.IAccount equals e.i_account
                                         select new { c.CallType, e.ExtensionID, e.id }).Distinct().ToList();

                        var AccountInLegs = (from c in complete_call
                                             join a in lstAccount
                                             on c.IAccount equals a.IAccount
                                             select new { a.Id, a.Name, a.IAccount, a.IProduct, c.CallType }).Distinct().ToList();

                        var DIDInLegs = (from a in AccountInLegs
                                         join p in DIDProducts on a.IProduct equals p.IProduct
                                         select new { a.Id, a.Name, a.CallType, a.IProduct, a.IAccount }).Distinct().ToList();

                        var FirstLeg = complete_call.FirstOrDefault();
                        if (FirstLeg.BitFlags == 8 || FirstLeg.BitFlags == 12 || FirstLeg.BitFlags == 40 || FirstLeg.BitFlags == 72 || FirstLeg.BitFlags == 104)
                        {
                            Console.Write("Incoming call " + FirstLeg.CallId);
                        }

                        if (total_Legs == ExtInLegs.Count() && !IsAllDigits(FirstLeg.CallType.Trim()))
                        {
                            //Internal
                            IsRecognized = true;
                            objCall.CallTypeId = 0;
                            ProcessLegs(complete_call, lstAccount, lstProduct, lstExtension, objCall);
                        }
                        else if (total_Legs == 1 && IsAllDigits(FirstLeg.CallType.Trim()))
                        {
                            //OUT
                            IsRecognized = true;
                            objCall.CallTypeId = 2;
                            objCall.ExtensionID = ExtInLegs.FirstOrDefault() == null ? 0 : ExtInLegs.FirstOrDefault().ExtensionID;
                            objCall.CLI = FirstLeg.CLI;
                            objCall.DID = FirstLeg.CLD;
                            objCall.IAccount = FirstLeg.IAccount;

                            ProcessLegs(complete_call, lstAccount, lstProduct, lstExtension, objCall);
                        }
                        else if (ExtInLegs.Count() > 0 && DIDInLegs.Count > 0 && ExtInLegs.Where(i => i.CallType == "UMRECORD").Count() == 0)
                        {
                            //IN
                            IsRecognized = true;
                            objCall.DID = DIDInLegs.FirstOrDefault().Id;
                            objCall.CLI = FirstLeg.CLI;
                            objCall.IAccount = DIDInLegs.FirstOrDefault().IAccount;
                            objCall.ExtensionID = ExtInLegs.FirstOrDefault().ExtensionID;

                            objCall.RingTime = complete_call.Max(a => a.Charged_Quantity) - complete_call.Min(a => a.Charged_Quantity);
                            objCall.TalkTime = complete_call.Min(a => a.Charged_Quantity);

                            objCall.AnswerInTarget = 1;
                            if (objCall.RingTime >= systemSettings.AnswerTime)
                            {
                                objCall.AnswerInTarget = 0;
                            }

                            objCall.CallTypeId = 1;
                            ProcessLegs(complete_call, lstAccount, lstProduct, lstExtension, objCall);
                        }
                        else if (ExtInLegs.Count() > 0 && DIDInLegs.Count > 0 && ExtInLegs.Where(i => i.CallType == "UMRECORD").Count() > 0)
                        {
                            //Missed at Extension
                            IsRecognized = true;
                            objCall.CallTypeId = 1;
                            objCall.MissedCall = true;

                            objCall.DID = DIDInLegs.FirstOrDefault().Id;
                            objCall.CLI = FirstLeg.CLI;
                            objCall.IAccount = DIDInLegs.FirstOrDefault().IAccount;
                            objCall.ExtensionID = ExtInLegs.FirstOrDefault().ExtensionID;

                            objCall.RingTime = complete_call.Max(a => a.Charged_Quantity) - complete_call.Min(a => a.Charged_Quantity);
                            objCall.TalkTime = complete_call.Min(a => a.Charged_Quantity);

                            ProcessLegs(complete_call, lstAccount, lstProduct, lstExtension, objCall);
                        }
                        else if (ExtInLegs.Count() == 0 && DIDInLegs.Count > 0 && DIDInLegs.Where(i => i.CallType == "UMRECORD").Count() > 0)
                        {
                            //Missed at DID
                            IsRecognized = true;
                            objCall.CallTypeId = 1;
                            objCall.MissedCall = true;

                            objCall.DID = DIDInLegs.FirstOrDefault().Id;
                            objCall.CLI = FirstLeg.CLI;
                            objCall.IAccount = DIDInLegs.FirstOrDefault().IAccount;

                            objCall.RingTime = complete_call.Max(a => a.Charged_Quantity) - complete_call.Min(a => a.Charged_Quantity);
                            objCall.TalkTime = complete_call.Min(a => a.Charged_Quantity);


                            //ProcessLegs(complete_call, lstAccount, lstProduct, lstExtension, objCall);
                        }
                        else if (total_Legs == 1 && DIDInLegs.Count > 0 && FirstLeg.CallType == "UMRECORD")
                        {
                            //Abandant
                            //log.Info("Call Abundant " + FirstLeg.CallId);
                            //Console.WriteLine("Abundant " + FirstLeg.CallId);
                        }
                        else
                        {
                            //log.Info("Call not recognized " + FirstLeg.CallId);
                            //Console.WriteLine("Call not recognized" + FirstLeg.CallId);
                        }

                        if (IsRecognized)
                        {
                            objDal.InsertCall(objCall, ConnString);
                        }


                    }
                }

            }

        }
        public void ProcessLegs(List<hourlycdr> complete_call, List<Account> lstAccount, List<Product> lstProduct, List<ExtensionLst> lstExtension, Call objCall)
        {
            objCall.CallDetails = new List<CallDetail>();
            foreach (var callleg in complete_call)
            {
                ExtensionLst extension = new ExtensionLst();
                extension = lstExtension.Where(i => i.i_account == callleg.IAccount).FirstOrDefault();

                if (extension != null)
                {
                    CallDetail objcall = new CallDetail();



                    objcall.TalkTime = callleg.Charged_Quantity;
                    objcall.PeriodId = callleg.Connect_time.Hour;
                    objcall.ConnectTime = callleg.Connect_time;
                    objcall.DisconnectTime = callleg.Disconnect_time;
                    objcall.IAccount = callleg.IAccount;
                    objcall.RingTime = 0;

                    objCall.CallDetails.Add(objcall);
                }

            }
        }


      public List<LstActiveCall> GetActiveCalls(int Icustomer, string connString)
        {
            return objDal.GetActiveCallsDb(Icustomer, connString);
        }

        public List<LstActiveCall> ProcessActiveCalls(int ICustomer, List<LstActiveCall> lstCDR, string ConnString)
        {
            List<LstActiveCall> Returncalls = new List<LstActiveCall>();



            var lstExtension = objDal.GetExtensions(ICustomer, ConnString);

            //var lstProduct = objDal.GetProduct(ICustomer);

            var DIDProducts = objDal.GetProductByType(ICustomer, 1, ConnString);

           //var ExtProducts = objDal.GetProductByType(ICustomer, 2);

            //var systemSettings = objDal.GetSystemSetting(ICustomer);


            if (lstCDR != null && lstCDR.Count > 0)
            {

                    foreach (var leg in lstCDR)
                    {
                        LstActiveCall objCall = new LstActiveCall();

                        if (leg.CallType == "OUTGOING")
                        {
                            //OUT

                            var ext = lstExtension.Where(i => i.i_account == leg.IAccount).FirstOrDefault();

                            if(ext != null)
                            {
                                objCall.CallType = "OUTGOING";
                                objCall.CLI = ext.name;
                                objCall.Name = ext.name;
                                objCall.DurationMins = leg.DurationMins;
                                objCall.CLD = leg.CLD;
                                Returncalls.Add(objCall);
                            }
                           
                        }
                        else if (leg.CallType == "INCOMING")
                        {
                            //OUT
                            var ext = lstExtension.Where(i => i.i_account == leg.IAccount).FirstOrDefault();
                              //OUT
                            if (ext != null)
                            {
                                objCall.CallType = "INCOMING";
                                objCall.CLI = leg.CLI;
                                objCall.Name = ext.name;
                                objCall.DurationMins = leg.DurationMins;
                                objCall.CLD = ext.name;
                                Returncalls.Add(objCall);
                            }
                        }

                    }

            }

            return Returncalls;
        }

        bool IsAllDigits(string s)
        {
            foreach (char c in s)
            {
                if (!char.IsDigit(c))
                    return false;
            }
            return true;
        }

        string GetPeriod(DateTime connect_time)
        {
            string period = "00:00 - 01:00";

            switch (connect_time.Hour)
            {
                case 0:
                    period = "00:00 - 00:59";
                    break;
                case 1:
                    period = "01:00 - 01:59";
                    break;
                case 2:
                    period = "02:00 - 02:59";
                    break;
                case 3:
                    period = "03:00 - 03:59";
                    break;
                case 4:
                    period = "04:00 - 04:59";
                    break;
                case 5:
                    period = "05:00 - 05:59";
                    break;
                case 6:
                    period = "06:00 - 06:59";
                    break;
                case 7:
                    period = "07:00 - 07:59";
                    break;
                case 8:
                    period = "08:00 - 08:59";
                    break;
                case 9:
                    period = "09:00 - 09:59";
                    break;
                case 10:
                    period = "10:00 - 10:59";
                    break;
                case 11:
                    period = "11:00 - 11:59";
                    break;
                case 12:
                    period = "12:00 - 12:59";
                    break;
                case 13:
                    period = "13:00 - 13:59";
                    break;
                case 14:
                    period = "14:00 - 14:59";
                    break;
                case 15:
                    period = "15:00 - 15:59";
                    break;
                case 16:
                    period = "16:00 - 16:59";
                    break;
                case 17:
                    period = "17:00 - 17:59";
                    break;
                case 18:
                    period = "18:00 - 18:59";
                    break;
                case 19:
                    period = "19:00 - 19:59";
                    break;
                case 20:
                    period = "20:00 - 20:59";
                    break;
                case 21:
                    period = "21:00 - 21:59";
                    break;
                case 22:
                    period = "22:00 - 22:59";
                    break;
                case 23:
                    period = "23:00 - 23:59";
                    break;

            }

            return period;
        }

    }
}
