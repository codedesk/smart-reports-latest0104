using DataAccess.BLL;
using DataAccess.Repository;

namespace DataAccess.RepositoryContracts
{
    public interface ISystemSettingRepository : IRepository<SystemSetting>
    {
    }
}
