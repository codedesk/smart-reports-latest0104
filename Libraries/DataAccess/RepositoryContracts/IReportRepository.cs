using DataAccess.BLL;
using DataAccess.Repository;

namespace DataAccess.RepositoryContracts
{
    public interface IReportRepository : IRepository<Report>
    {
    }
}
