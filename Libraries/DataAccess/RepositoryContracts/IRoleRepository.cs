using DataAccess.BLL;
using DataAccess.Repository;

namespace DataAccess.RepositoryContracts
{
    public interface IRoleRepository : IRepository<Role>
    {
    }
}
