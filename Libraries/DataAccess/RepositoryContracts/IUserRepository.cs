using DataAccess.BLL;
using DataAccess.Repository;

namespace DataAccess.RepositoryContracts
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
