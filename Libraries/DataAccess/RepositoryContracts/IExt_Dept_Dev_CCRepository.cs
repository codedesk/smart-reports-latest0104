using DataAccess.BLL;
using DataAccess.Repository;

namespace DataAccess.RepositoryContracts
{
    public interface IExt_Dept_Dev_CCRepository : IRepository<Ext_Dept_Dev_CC>
    {
    }
}
