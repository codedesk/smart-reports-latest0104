using DataAccess.BLL;
using DataAccess.Repository;
using System.Xml.Schema;

namespace DataAccess.RepositoryContracts
{
    public interface IExtensionRepository : IRepository<extension>
    {
    }
}
