using DataAccess.BLL;
using DataAccess.Repository;

namespace DataAccess.RepositoryContracts
{
    public interface ICountryRepository : IRepository<Country>
    {
    }
}
