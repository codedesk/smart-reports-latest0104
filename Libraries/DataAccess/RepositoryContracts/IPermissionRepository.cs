using DataAccess.BLL;
using DataAccess.Repository;

namespace DataAccess.RepositoryContracts
{
    public interface IPermissionRepository : IRepository<Permission>
    {
    }
}
