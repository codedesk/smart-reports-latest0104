using DataAccess.BLL;
using DataAccess.DAL;
using DataAccess.RepositoryContracts;

namespace DataAccess.Repository
{
    public class Ext_Dept_Dev_CCRepository : BaseRepository<Ext_Dept_Dev_CC>, IExt_Dept_Dev_CCRepository
    {
        public Ext_Dept_Dev_CCRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
}
