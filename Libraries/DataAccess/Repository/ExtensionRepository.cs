using DataAccess.BLL;
using DataAccess.DAL;
using DataAccess.RepositoryContracts;

namespace DataAccess.Repository
{
    public class ExtensionRepository : BaseRepository<extension>, IExtensionRepository
    {
        public ExtensionRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
}
