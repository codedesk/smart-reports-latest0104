using DataAccess.BLL;
using DataAccess.DAL;
using DataAccess.RepositoryContracts;

namespace DataAccess.Repository
{
    public class CostCentreRepository : BaseRepository<CostCentre>, ICostCentreRepository
    {
        public CostCentreRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
}
