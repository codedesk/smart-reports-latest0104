using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using DataAccess.DAL;
using DataAccess.BLL;
using System.Collections;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Data.Common;
using System.Dynamic;
using DataAccess.RepositoryContracts;
using System.Linq.Expressions;


namespace DataAccess.Repository
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public Entities _context;
        public RoleRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public override IEnumerable<Role> GetMany(Func<Role, bool> where)
        {
            _context = new Entities();
            _context.Configuration.LazyLoadingEnabled = false;
            return _context.Role.Where(where);
        }
    }
}
