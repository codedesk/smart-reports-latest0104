using DataAccess.BLL;
using DataAccess.DAL;
using DataAccess.RepositoryContracts;

namespace DataAccess.Repository
{
    public class DivisionRepository : BaseRepository<Division>, IDivisionRepository
    {
        public DivisionRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
}
