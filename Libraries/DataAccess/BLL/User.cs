//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess.BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public User()
        {
            this.UserLogin = new HashSet<UserLogin>();
        }
    
        public int Id { get; set; }
        public Nullable<int> ICustomer { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Nullable<int> RoleId { get; set; }
        public byte UserType { get; set; }
        public Nullable<int> ClientId { get; set; }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }
        public bool IsSuspended { get; set; }
        public int InvalidPasswordAttempt { get; set; }
        public int Code { get; set; }
        public bool IsCodeUsed { get; set; }
        public System.DateTime CodeExpiry { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    
        public virtual ICollection<UserLogin> UserLogin { get; set; }
        public virtual Role Role { get; set; }
        public virtual Company Company { get; set; }
    }
}
