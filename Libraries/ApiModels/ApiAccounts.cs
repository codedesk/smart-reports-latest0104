﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class ApiAccounts
    {
        public List<ApiAccount> Accounts { get; set; }
    }

    public class ApiAccount
    {
        public string Id { get; set; }
        public string IAccount { get; set; }
        public string Balance { get; set; }
    }
}