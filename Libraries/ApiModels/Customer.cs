﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Customer
    {
        public CustomerRes CustomerRes { get; set; }
        public List<CustomersShortInfo> CustomersShortInfo { get; set; }
    }
    public class CustomerRes
    {
        public int Balance { get; set; }
        public int CreditLimit { get; set; }
        public int availableBalance { get; set; }
    }

    public class CustomersShortInfo
    {
        public int ICustomer { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public string email { get; set; }
    }

   
}
