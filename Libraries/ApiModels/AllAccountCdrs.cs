﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{

    public class AllAccountCdrs
    {
        public int CDRCallsTotal { get; set; }
        public Lstaccountcdr[] lstAccountCDRS { get; set; }
    }

    public class Lstaccountcdr
    {
        public int reseller_ID { get; set; }
        public int customer_ID { get; set; }
        public string customer { get; set; }
        public int Account_ID { get; set; }
        public string account { get; set; }
        public int id { get; set; }
        public int i_env { get; set; }
        public string h_323_conf_id { get; set; }
        public string CLI { get; set; }
        public string CLD { get; set; }
        public int setup_time { get; set; }
        public DateTime Connect_time { get; set; }
        public DateTime Disconnect_time { get; set; }
        public int disconnect_cause { get; set; }
        public int voice_quality { get; set; }
        public int IDest { get; set; }
        public int charged_time { get; set; }
        public string history { get; set; }
        public int i_vendor { get; set; }
        public int i_cost { get; set; }
        public int i_Customer { get; set; }
        public string call_id { get; set; }
        public int i_account { get; set; }
        public int i_tariff { get; set; }
        public int i_rate { get; set; }
        public int i_service { get; set; }
        public DateTime Bill_time { get; set; }
        public int Charged_Quantity { get; set; }
        public int used_quantity { get; set; }
        public int BitFlags { get; set; }
        public string rating_pattern { get; set; }
        public int split_order { get; set; }
        public int billing_model { get; set; }
        public int peak_level { get; set; }
        public string subscriber_ip { get; set; }
        public int i_dest_group { get; set; }
        public int i_accessibility { get; set; }
        public int i_invoice { get; set; }
        public float Charged_Amount { get; set; }
        public int Reseller_ICustomer { get; set; }
        public string Product { get; set; }
        public int i_product { get; set; }
        public string Account_Label { get; set; }
        public float CallCost { get; set; }
        public string LineIdentity { get; set; }
        public string h_323_incoming_conf_id { get; set; }
        public string reseller { get; set; }
    }

}
