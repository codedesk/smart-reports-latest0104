﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class HotelAccount
    {
        public class CustomerAddress
        {
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string Address3 { get; set; }
            public string Address4 { get; set; }
            public string City { get; set; }
            public string Region { get; set; }
            public string Postcode { get; set; }
            public string Country { get; set; }
            public string Town { get; set; }
        }

        public class CustomerContact
        {
            public string Phone1 { get; set; }
            public string Phone2 { get; set; }
            public string Contact1 { get; set; }
            public string Contact2 { get; set; }
            public string Fax { get; set; }
        }

        public class CustomerStatus
        {
            public int OpeningBalance { get; set; }
            public double Balance { get; set; }
            public int CreditLimit { get; set; }
            public int DiscountRate { get; set; }
            public List<object> ServiceFeatures { get; set; }
            public string BillStatus { get; set; }
            public string ISO4217 { get; set; }
        }

        public class Preferences
        {
            public int MaxAbbreviatedLength { get; set; }
            public int IMoh { get; set; }
            public int ITimeZone { get; set; }
            public int CustomerType { get; set; }
            public int IVDPlan { get; set; }
        }

        public class PersonalInformation
        {
            public string FirstName { get; set; }
            public string MiddleInitials { get; set; }
            public string LastName { get; set; }
            public string Salutation { get; set; }
            public string Password { get; set; }
            public string Name { get; set; }
        }

        public class Customer
        {
            public CustomerAddress CustomerAddress { get; set; }
            public CustomerContact CustomerContact { get; set; }
            public CustomerStatus CustomerStatus { get; set; }
            public Preferences Preferences { get; set; }
            public PersonalInformation PersonalInformation { get; set; }
            public int ICustomer { get; set; }
            public int IParent { get; set; }
        }

        public class Hotel
        {
            public Customer Customer { get; set; }
        }
    }
}
