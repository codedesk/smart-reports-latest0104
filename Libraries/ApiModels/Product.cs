﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{

    public class Products
    {
        public List<Product> PortaProducts { get; set; }
    }

    public class Product
    {
        public int IProduct { get; set; }
        public string Name { get; set; }
        public string MaintanenceFee { get; set; }
        public int? ITariff { get; set; }
    }

   
}
