﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{

    public class AssignedExtension
    {
        public int i_cg_ext { get; set; }
        public bool i_cg_extSpecified { get; set; }
        public int i_c_ext { get; set; }
        public bool i_c_extSpecified { get; set; }
        public string type { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }

    public class HGInfoLst
    {
        public int i_c_group { get; set; }
        public int i_customer { get; set; }
        public bool i_customerSpecified { get; set; }
        public string name { get; set; }
        public string hunt_sequence { get; set; }
        public string hunt_keep_original_cli { get; set; }
        public int i_c_ext { get; set; }
        public string id { get; set; }
        public List<AssignedExtension> assigned_extensions { get; set; }
    }

    public class HuntGroup
    {
        public bool HGInfoDeleted { get; set; }
        public int HuntGroupId { get; set; }
        public List<HGInfoLst> HGInfoLst { get; set; }
    }

}
