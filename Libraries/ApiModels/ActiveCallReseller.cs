﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    //public class LstActiveCall
    //{
    //    public int IEnv { get; set; }
    //    public string CLI { get; set; }
    //    public string CLD { get; set; }
    //    public string AccountID { get; set; }
    //    public DateTime Connect_time { get; set; }
    //    public DateTime Disconnect_time { get; set; }
    //    public int ICustomer { get; set; }
    //    public int IAccount { get; set; }
    //    public int noofcalls { get; set; }
    //    public string DurationMins { get; set; }
    //    public string CallType { get; set; }
    //    public int VendorId { get; set; }
    //    public int IConnection { get; set; }
    //    public int IDialRuleCld { get; set; }
    //}

    public class ActiveCallReseller
    {
        public List<LstActiveCall> lstActiveCalls { get; set; }
        public List<object> lstAccountActiveCalls { get; set; }
        public string License { get; set; }
    }
  
}
