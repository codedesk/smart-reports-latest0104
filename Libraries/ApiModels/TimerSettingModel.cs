﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class TimerSettingModel
    {
        public int Id { get; set; }

        public string TimerName { get; set; }

        public int DueTime { get; set; }

        public int Period { get; set; }

        public bool IsActive { get; set; }
    }
}
