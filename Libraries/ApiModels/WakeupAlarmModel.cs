﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public class WakeupAlarmModel
    {
        public int Id { get; set; }
        public System.DateTime Date { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public int account_id { get; set; }
        public string  Extension { get; set; }
        public string ICustomer { get; set; }
        public string IAccount { get; set; }

    }
}
