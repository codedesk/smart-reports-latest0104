﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public class SystemSetting
    {
        public int AnswerTime { get; set; }
        public int LocalCode { get; set; }
        public int MaxCallDuration { get; set; }
    }
}
