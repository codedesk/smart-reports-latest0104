﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Call
    {
        public string hourly_cdr_id { get; set; }
        public int CallTypeId { get; set; }
        public int TalkTime { get; set; }
        public int RingTime { get; set; }
        public DateTime ConnectTime { get; set; }
        public DateTime DisconnectTime { get; set; }
        public bool MissedCall { get; set; }
        public string DID { get; set; }
        public string CLI { get; set; }
        public int IAccount { get; set; }
        public int ICustomer { get; set; }
        public int ExtensionID { get; set; }
        public int PeriodId { get; set; }
        public string CallId { get; set; }
        public int Legs { get; set; }
        public int Location { get; set; }
        public int Bounced { get; set; }
        public int InComingTransfered { get; set; }
        public string OutGoingTransfered { get; set; }
        public int AnswerInTarget { get; set; }

        public string DurationMins { get; set; }
        public double Charged_Amount { get; set; }
        public List<CallDetail> CallDetails { get; set; }
    }

    public class CallDetail
    {
        public int FKCallId { get; set; }
        public DateTime ConnectTime { get; set; }
        public DateTime DisconnectTime { get; set; }
        public int IAccount { get; set; }
        public int TalkTime { get; set; }
        public int RingTime { get; set; }
        public int PeriodId { get; set; }
    }
}
