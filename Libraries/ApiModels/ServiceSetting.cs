﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ServiceSetting
    {
        public string CompanyName { get; set; }
        public string LicenseKey { get; set; }
        public string ConnString { get; set; }
    }
}
