﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{

    public class CDR
    {
        public int TotalCalls { get; set; }
        public int NextOffset { get; set; }
        public DictPortaCustomerAccountCDRS DictPortaCustomerAccountCDRS { get; set; }
    }

    public class DictPortaCustomerAccountCDRS
    {
         [JsonProperty("Voice Calls")]
        public List<hourlycdr> Voice_Calls { get; set; }
    }

    public class hourlycdr
    {
        public int hourly_cdr_id { get; set; }
        public int ID { get; set; }
        public int IEnv { get; set; }
        public string CLI { get; set; }
        public string CLD { get; set; }
        public string AccountID { get; set; }
        public DateTime Connect_time { get; set; }
        public DateTime Disconnect_time { get; set; }
        public DateTime Bill_time { get; set; }
        public int ICustomer { get; set; }
        public int IDest { get; set; }
        public int Charged_Quantity { get; set; }
        public double Charged_Amount { get; set; }
        public string CallId { get; set; }
        public int IAccount { get; set; }
        public string ServiceName { get; set; }
        public int BitFlags { get; set; }
        public int DisconnectCause { get; set; }
        public string ServiceFlags { get; set; }
        public string History { get; set; }
        public int IService { get; set; }
        public string CallType { get; set; }
        public string RatingPattern { get; set; }
        public int Used_Quantity { get; set; }
    }
}
