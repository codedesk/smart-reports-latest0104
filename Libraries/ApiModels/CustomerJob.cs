﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public class CustomerJob
    {
        public int Id { get; set; }
        public int ICustomer { get; set; }
        public DateTime LastRunTime { get; set; }
        public DateTime NextRunTime { get; set; }
        public int RecordsFetched { get; set; }
        public int Status { get; set; }
        public string ReportTo { get; set; }
    }
}
