﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public class AppSetting
    {
        public int Id { get; set; }
        public string Logo { get; set; }
        public string LicenseKey { get; set; }
        public string CustomStyles { get; set; }
    }
}
