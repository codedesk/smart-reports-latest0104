﻿using DataAccess.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ApiAccountModel
    {

        public List<account> Accounts { get; set; }
        public string License { get; set; }
        public string ContentId { get; set; }
        public string FeatureName { get; set; }


        public ApiAccountModel()
        {
            Accounts = new List<account>();
        }
        
       

        //public string Id = "440032821791";
        //public int IAccount = 2635397;
        //public int ICustomer = 27994;
        //public string BillingModel = "Credit";
        //public string ISO4217 = "GBP";
        //public double Balance = 132.1217;
        //public string Blockedfield = "N";
        //public bool Blocked = true;
        //public string UMEnabledfield ="Y";
        //public bool UMEnabled = true;
        //public string UILogin = "440032821791";
        //public string UIPassword = "h8agu4uz";
        //public string VoipPassword = "6o3wwdez";
        //public int IProduct = 2113;
        //public string BatchNamePrefix { get; set; }
        //public int IBatch  {get; set;}
        //public int IRoutingPlan = -1;
        //public int IEnv = 106;
        //public string Email { get; set; }
        //public string Phone1 { get; set; }
        //public string CompanyName { get; set; }
        //public string BillStatus = "0";
        //public string FollowMeEnabled = "N";
        //public string ServiceFlags = "NY^^^NNN73YN~N~NY^^NY~~NN~/^N/ NYN";
        //public string Note = "Code Desk 1 - 207";
        //public string Name { get; set; }
        //public string FirstName { get; set; }
        //public string MiddleInitials { get; set; }
        //public string LastName { get; set; }
        //public string Address1 { get; set; }
        //public string Address2 { get; set; }
        //public string Address3 { get; set; }
        //public string Address4 { get; set; }
        //public string Town { get; set; }
        //public string City { get; set; }
        //public string Postcode { get; set; }
        //public string Country { get; set; }
        //public string Phone2 { get; set; }
        //public string PhoneNo { get; set; }
        //public string Salutation { get; set; }
        //public string Fax { get; set; }
    }
}
