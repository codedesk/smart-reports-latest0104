﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public class CallByExtension
    {
        public string Period { get; set; }
        public string Extension { get; set; }
        public string Title { get; set; }
        public int Out { get; set; }
        public int In { get; set; }
        public int Internal { get; set; }
        public int Missed { get; set; }
        public int TalkTime { get; set; }
    }
}
