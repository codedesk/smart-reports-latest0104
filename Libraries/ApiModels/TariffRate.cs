﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{


    public class TariffRates
    {

        public List<TariffRate> CountrysRates { get; set; }


        public TariffRates()
        {

            CountrysRates = new List<TariffRate>();
        }

    }


    public class TariffRate
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int Prefix { get; set; }


        public float SecondIntervalPrice { get; set; }


        public string Hidden { get; set; }

        public string Discontinued { get; set; }

        public string Forbidden { get; set; }



    }
}
